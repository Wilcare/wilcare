wilcare.config(["$routeProvider", function($routeProvider) {
	$routeProvider
	.when("/", {
		resolve: {
			'check': function($location, $localStorage) {
				if($localStorage != undefined) {
					if($localStorage.user == "doctor")
						$location.path("/doctor")
				}
			}
		},
		templateUrl: "../adminScreen/adminAdmins.html"})
	.when("/admin", {
		templateUrl: "../adminScreen/adminAdmins.html"})
	.when("/addAdmin", {
		templateUrl: "../adminScreen/adminAddAdmin.html"})
	.when("/records", {
		templateUrl: "../adminScreen/adminRecords.html"})
	.when("/addRecord", {
		templateUrl: "../adminScreen/adminAddRecord.html"})
	.when("/medicines", {
		templateUrl: "../adminScreen/adminMedicines.html"})
	.when("/addMedicine", {
		templateUrl: "../adminScreen/adminAddMedicine.html"})
	.when("/diseases", {
		templateUrl: "../adminScreen/adminDiseases.html"})
	.when("/addDisease", {
		templateUrl: "../adminScreen/adminAddDisease.html"})
	.when("/symptoms", {
		templateUrl: "../adminScreen/adminSymptoms.html"})
	.when("/addSymptom", {
		templateUrl: "../adminScreen/adminAddSymptom.html"})
	.when("/logout", {
		templateUrl: "../mainScreen/login.html"})
}]);

wilcare.controller('adminController', function($scope, $http, $httpParamSerializer) {
		console.log("wilcare.adminController start");
		$scope.adminMode  = "Add";
		$scope.doctorMode  = "Add";
		$scope.medicineMode  = "Add";
		$scope.symptomMode  = "Add";		
		$scope.diseaseMode  = "Add";	
		
		$scope.admin = {
		    	id: "",
		    	username:"",
		    	password:"",
		    	firstName: "", 
		    	middleName: "",
		    	lastName: "",
		   		type:"admin",
		    	status:true  		
		}
		
		$scope.editAdmin = {
				id: "",
				username:"",
		    	password:"",
				firstName: "", 
				middleName: "",
				lastName: "",
				status: true,
				totalBill: 0.0    		
		}
		
		$scope.doctor = {
				id: "",
				username:"",
				password:"",
				firstName: "", 
				middleName: "",
				lastName: "",
				type:"doctor",	
				price:0.0,
				status:true  		
		}
		
		$scope.editDoctor = {
				id: "",
				patientID: [],
				username:"",
				password:"",
				firstName: "", 
				middleName: "",
				lastName: "",
				type:"doctor",
				price:0.0,
				status:true  		
		}
		
		$scope.medicine = {
				id: "",
				medicineName:"",
				medicineDescription:"",
				price:0.0
		}
		
		$scope.editMedicine = {
				id: "",
				medicineName:"",
				medicineDescription:"",
				price:0.0   		
		}
		
		$scope.symptom = {
				id: "",
				symptomName:"",
				symptomDescription:""	
		}
		
		$scope.editSymptom = {
				id: "",
				symptomName:"",
				symptomDescription:""	
		}
		
		$scope.disease = {
				id: "",
				symptomID:[],
				medicineID:[],	
				diseaseName:"",	
				diseaseDescription:""
		}
		
		$scope.editDisease = {
				id: "",
				symptomID:[],
				medicineID:[],	
				diseaseName:"",	
				diseaseDescription:""
		}
		
		$scope.adminList = [];
		$scope.doctorList = [];
		$scope.medicineList = [];
		$scope.symptomList = [];
		$scope.diseaseList = [];
		
		$scope.Doctor = function(id,username,password, firstName, middleName, lastName, type, price, status ) {
			this.id = id;
			this.username = username;
			this.password = password;
		    this.firstName = firstName;
		    this.middleName = middleName;
		    this.lastName = lastName;
		    this.type = type;
		    this.price = price;
		    this.status = status;
		}
		
		$scope.EditDoctor = function(id,patientID,username,password, firstName, middleName, lastName, type, price, status ) {
			this.id = id;
			this.patientID = patientID;
			this.username = username;
			this.password = password;
			this.firstName = firstName;
			this.middleName = middleName;
			this.lastName = lastName;
			this.type = type;
			this.price = price;
			this.status = status;  
		}
		
		$scope.Admin = function( id, username, 	password, firstName,  middleName, lastName,  type, status ){
			this.id = id;
			this.username = username;
			this.password = password;
			this.firstName = firstName;
			this.middleName = middleName;
			this.lastName = lastName
			this.type = type;
			this.status = status;	
		}	
		
		$scope.Medicine = function( id, medicineName, medicineDescription, price ){
			this.id = id;
			this.medicineName = medicineName;
			this.medicineDescription = medicineDescription;
			this.price = price;
		}
		
		$scope.Symptom = function(	id, symptomName, symptomDescription){
			this.id = id;
			this.symptomName = symptomName;
			this.symptomDescription = symptomDescription;
		}
		
		$scope.Disease = function(	id, symptomID, medicineID, diseaseName, diseaseDescription){
			this.id = id;
			this.symptomID = symptomID;
			this.medicineID = medicineID;
			this.diseaseName = diseaseName;
			this.diseaseDescription = diseaseDescription;
		}
		
		$scope.submitDoctor = function() {	
			if($scope.doctorMode === "Add") {
				$scope.addDoctor();
				$scope.clearDoctor();
			}
			else {
			  $scope.doctorMode = "Update"
			  $scope.updateDoctor();
			}
		}	
		
		$scope.submitAdmin = function() {	
			if($scope.adminMode === "Add") {
				$scope.addAdmin();
				$scope.clearAdmin();
			}
			else {
			  $scope.adminMode = "Update"
			  $scope.updateAdmin();
			}
		}	
		
		$scope.submitMedicine = function() {	
			if($scope.medicineMode === "Add") {
				$scope.addMedicine();
				$scope.clearMedicine();
			}
			else {
			  $scope.medicineMode = "Update"
			  $scope.updateMedicine();
			}
		}	
		
		$scope.submitSymptom = function() {	
			if($scope.symptomMode === "Add") {
				$scope.addSymptom();
				$scope.clearSymptom();
			}
			else {
			  $scope.symptomMode = "Update"
			  $scope.updateSymptom();
			}
		}	
		
		$scope.submitDisease = function() {	
			if($scope.diseaseMode === "Add") {
				$scope.addDisease();
				$scope.clearDisease();
			}
			else {
			  $scope.diseaseMode = "Update"
			  $scope.updateDisease();
			}
		}	
		
		
/////CRUD Doctor
		$scope.addDoctor = function() {
			const url = '/DoctorCreate';
			
			if(window.confirm("Are you sure you want to add doctor?")) {
				var doctor = new $scope.Doctor(
						$scope.doctor.id, 
						$scope.doctor.username, 
						$scope.doctor.password,
						$scope.doctor.firstName, 
					    $scope.doctor.middleName,
					    $scope.doctor.lastName,
					    $scope.doctor.type,
					    $scope.doctor.price,
					    $scope.doctor.status );	
				var headers = {
						 headers: {'Content-Type':'application/x-www-form-urlencoded'}  
			    };
				
				 $http.post(url, $httpParamSerializer(doctor), headers)
					.then(
							response => {
								$scope.getDoctorList();
								$scope.clearDoctor();
							},
							error => {
								console.log('something went wrong in adding data');
							}
					)
			}
			console.log(doctor);
			
		}
		$scope.updateDoctor = function() {
			const url = '/DoctorUpdate';
			
			var doctor = new $scope.EditDoctor(
					$scope.doctor.id,
					$scope.doctor.patientID,
					$scope.doctor.username,
					$scope.doctor.password,
					$scope.doctor.firstName,
					$scope.doctor.middleName,
					$scope.doctor.lastName,
					$scope.doctor.type,
					$scope.doctor.price,
					$scope.doctor.status );
			
			var headers = {
					headers: {'Content-Type':'application/json'}
		    };
			if(window.confirm("Are you sure you want to update this?")){
			 $http.post(url,doctor, headers)
				.then(
						response => {
							$scope.getDoctorList();
							$scope.clearEditDoctor();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
			console.log(doctor);
			
		}
		$scope.deleteDoctor = function() {
			var index = $scope.doctorList.indexOf($scope.editDoctor);
			var url = '/DoctorDelete';
			if(window.confirm("Are you sure you want to delete this?")){
				var headers = {
						headers: {'Content-Type':'application/json'}
			    };
				 $http.post(url, $scope.editDoctor, headers)
			  	.then(
			  			response => {
			  				console.log('delete successful');
			  				$scope.doctorList.splice(index, 1);
			  			},
			  			error => {
			  				console.log('something went wrong in deleting data');
			  			}
			  	) 
			}
		}
		
	//CRUD Admin	
		$scope.addAdmin = function() {
			const url = '/AdminCreate';
			
			if(window.confirm("Are you sure you want to add admin?")) {
				var admin = new $scope.Admin($scope.admin.id, $scope.admin.username, $scope.admin.password, $scope.admin.firstName, 
											$scope.admin.middleName, $scope.admin.lastName, $scope.admin.type,  $scope.doctor.status 
											 );	
				
				var headers = {
					 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
			    };
				
				  $http.post(url, $httpParamSerializer(admin), headers)
					.then(
							response => {
								$scope.getAdminList();
								$scope.clearAdmin();
							},
							error => {
								console.log('something went wrong in adding data');
							}
					)
			}
			console.log(admin);
		}
		
		$scope.updateAdmin = function() {
			const url = '/AdminUpdate';
			
			var admin = new $scope.Admin($scope.admin.id, $scope.admin.username, $scope.admin.password, $scope.admin.firstName, 
										$scope.admin.middleName, $scope.admin.lastName, $scope.admin.type,  $scope.doctor.status 
										 );	
			
			var headers = {
				 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
		    };
			if(window.confirm("ARe you sure you want to update this?")){
			  $http.post(url, $httpParamSerializer(admin), headers)
				.then(
						response => {
							$scope.getAdminList();
							$scope.clearAdmin();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
			console.log(admin);
		}
		
		
		$scope.deleteAdmin = function() {
			
			var index = $scope.adminList.indexOf($scope.editAdmin);
			var url = '/AdminDelete';
			
			if(window.confirm("Are you sure you want to delete this?")){
				var headers = {
						headers: {'Content-Type':'application/json'}
			    };
				 $http.post(url, $scope.editAdmin, headers)
			  	.then(
			  			response => {
			  				console.log('delete successful');
			  				$scope.adminList.splice(index, 1);
			  			},
			  			error => {
			  				console.log('something went wrong in deleting data');
			  			}
			  	) 
			}
		}
  //CRUD Medicine	
		$scope.addMedicine = function() {
			const url = '/MedicineCreate';
			
			if(window.confirm("Are you sure you want to add medicine?")) {
			var	medicine = new $scope.Medicine($scope.medicine.id, $scope.medicine.medicineName,$scope.medicine.medicineDescription, 
												$scope.medicine.price);	
			
			var headers = {
				 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
		    };
			
			  $http.post(url, $httpParamSerializer(medicine), headers)
				.then(
						response => {
							$scope.getMedicineList();
							$scope.clearMedicine();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
			console.log(medicine);
		}
		
		$scope.updateMedicine = function() {
			const url = '/MedicineUpdate';
			
			var	medicine = new $scope.Medicine($scope.medicine.id, $scope.medicine.medicineName,$scope.medicine.medicineDescription, 
												$scope.medicine.price);	
			
			var headers = {
				 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
		    };
			if(window.confirm("ARe you sure you want to update this?")){
			  $http.post(url, $httpParamSerializer(medicine), headers)
				.then(
						response => {
							$scope.getMedicineList();
							$scope.clearMedicine();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
			console.log(medicine);
			  
		}
		
				
		$scope.deleteMedicine = function() {
			var index = $scope.medicineList.indexOf($scope.editMedicine);
			var url = '/MedicineDelete';
			if(window.confirm("Are you sure you want to delete this?")){
				var headers = {
						headers: {'Content-Type':'application/json'}
			    };
				 $http.post(url, $scope.editMedicine, headers)
			  	.then(
			  			response => {
			  				console.log('delete successful');
			  				$scope.medicineList.splice(index, 1);
			  			},
			  			error => {
			  				console.log('something went wrong in deleting data');
			  			}
			  	) 
			}
		}
		
 //CRUD Symptom		
		$scope.addSymptom = function() {
			const url = '/SymptomCreate';
			
			if(window.confirm("Are you sure you want to add symptom?")) {
				var	symptom = new $scope.Symptom($scope.symptom.id, $scope.symptom.symptomName, $scope.symptom.symptomDescription);									
				
				var headers = {
					 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
			    };
				
				  $http.post(url, $httpParamSerializer(symptom), headers)
					.then(
							response => {
								$scope.getSymptomList();
								$scope.clearSymptom();
							},
							error => {
								console.log('something went wrong in adding data');
							}
					)
				}
			console.log(symptom);
		}
		
		$scope.updateSymptom = function() {
			const url = '/SymptomUpdate';
			
			var	symptom = new $scope.Symptom($scope.symptom.id, $scope.symptom.symptomName, $scope.symptom.symptomDescription);									
			
			var headers = {
				 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
		    };
			if(window.confirm("ARe you sure you want to update this?")){
			  $http.post(url, $httpParamSerializer(symptom), headers)
				.then(
						response => {
							$scope.getSymptomList();
							$scope.clearSymptom();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
				console.log(symptom);
		}
		
	   $scope.deleteSymptom = function() {			
			var index = $scope.symptomList.indexOf($scope.editSymptom);
			var url = '/SymptomDelete';
			if(window.confirm("Are you sure you want to delete this?")){
				var headers = {
						headers: {'Content-Type':'application/x-www-form-urlencoded'}
			    };
				 $http.post(url, $scope.editSymptom, headers)
			  	.then(
			  			response => {
			  				console.log('delete successful');
			  				$scope.symptomList.splice(index, 1);
			  			},
			  			error => {
			  				console.log('something went wrong in deleting data');
			  			}
			  	) 
			}
		}
		
//Crud Disease
	   
	   $scope.addDisease = function() {
			const url = '/DiseaseCreate';
			
			if(window.confirm("Are you sure you want to add disease?")) {
			var	disease = new $scope.Disease($scope.disease.id, $scope.disease.symptomID, $scope.disease.medicineID,
					                        $scope.disease.diseaseName, $scope.disease.diseaseDescription);									
			
			var headers = {
					headers: {'Content-Type':'application/json'}     
		    };
			
			 $http.post(url,disease, headers)
				.then(
						response => {
							$scope.getDiseaseList();
							$scope.clearDisease();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
			console.log(disease);
		}	   	    	
	   	   	 
	   
		$scope.updateDisease = function() {
			const url = '/DiseaseUpdate';
			
			var	disease = new $scope.Disease($scope.disease.id, $scope.disease.symptomID, $scope.disease.medicineID,
					                        $scope.disease.diseaseName,$scope.disease.diseaseDescription);																	
			
			var headers = {
					headers: {'Content-Type':'application/json'}     
		    };
			if(window.confirm("ARe you sure you want to update this?")){
			 $http.post(url,disease, headers)
				.then(
						response => {
							$scope.getDiseaseList();
							$scope.clearDisease();
						},
						error => {
							console.log('something went wrong in adding data');
						}
				)
			}
				console.log(disease);
		}

//		  $scope.deleteSymptom = function() {			
//				var index = $scope.symptomList.indexOf($scope.symptom);
//				var url = '/SymptomDelete';
//				var headers = {
//						headers: {'Content-Type':'application/x-www-form-urlencoded'}
//			    };
//				 $http.post(url, $httpParamSerializer($scope.symptom), headers)
//			  	.then(
//			  			response => {
//			  				console.log('delete successful');
//			  				$scope.symptomList.splice(index, 1);
//			  			},
//			  			error => {
//			  				console.log('something went wrong in deleting data');
//			  			}
//			  	) 
//			}
		
	   
	   
		$scope.getDoctorList = function() {
			const url = '/DoctorList';
			$http.get(url)
				.then(
						response => {
							$scope.doctorList = response.data.doctorList;
							console.log('success' + $scope.doctorList);
						},
						error => {
							console.log('something went wrong in retrieving data');
						}
				)
		}
		
		$scope.getAdminList = function() {
			const url = '/AdminList';
			$http.get(url)
				.then(
						response => {
							$scope.adminList = response.data.adminList;
							console.log('success' + $scope.adminList);
						},
						error => {
							console.log('something went wrong in retrieving data');
						}
				)
		}
		
		$scope.getMedicineList = function() {
			const url = '/MedicineList';
			$http.get(url)
				.then(
						response => {
							$scope.medicineList = response.data.medicineList;
							console.log('success' + $scope.medicineList);
						},
						error => {
							console.log('something went wrong in retrieving data');
						}
				)
		}
		
    	$scope.getSymptomList = function() {
				const url = '/SymptomList';
				$http.get(url)
					.then(
							response => {
								$scope.symptomList = response.data.symptomList;
								console.log('success' + $scope.symptomList);
							},
							error => {
								console.log('something went wrong in retrieving data');
							}
					)
			}
			
    	
    	$scope.getDiseaseList = function() {
			const url = '/DiseaseList';
			$http.get(url)
				.then(
						response => {
							$scope.diseaseList = response.data.diseaseList;
							console.log('success' + $scope.diseaseList);
						},
						error => {
							console.log('something went wrong in retrieving data');
						}
				)
		}
    	
    	
     	$scope.clearDoctor = function() {
	    	for(p in $scope.doctor) {
	    		$scope.doctor[p] = "";
	    	}
	    	$scope.doctor.type="doctor";
	    	$scope.doctor.status = true;
	    }
     	
    	$scope.clearAdmin = function() {
	    	for(p in $scope.admin) {
	    		$scope.admin[p] = "";
	    	}	  
	    	$scope.admin.type="admin";
	    	$scope.admin.status = true;
	    }
    	
    	$scope.clearEditDoctor = function() {
	    	for(p in $scope.editDoctor) {
	    		$scope.editDoctor[p] = "";
	    	}		    			    						
	    }
  
    	$scope.clearMedicine = function() {
	    	for(p in $scope.medicine) {
	    		$scope.medicine[p] = "";
	    	}		    			    		    			
	    }
	
    	$scope.clearSymptom = function() {
	    	for(p in $scope.symptom) {
	    		$scope.symptom[p] = "";
	    	}		    			    		    			
	    }
    	$scope.clearDisease = function() {
	    	for(p in $scope.disease) {
	    		$scope.disease[p] = "";
	    	}		    			    		    			
	    }
    	
    	$scope.selectAdmin = function(admin){
    		$scope.editAdmin = admin;
    		$scope.displayAdminRecord(admin);
    	}
    	
    	$scope.displayAdminRecord = function(admin) {
    		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
    		var modalContainer = angular.element(document.querySelector('#modalContainer'));
    		var close = angular.element(document.querySelector('#close'));
    		$scope.admin = admin;
    		modalWrapper.addClass("showModalWrapper");
    		modalContainer.addClass("showModalContainer");
    		close.on('click', function() {
    			modalContainer.removeClass("showModalContainer");
    			modalWrapper.removeClass("showModalWrapper");
    		})
    	}
    	
    	$scope.selectDoctor = function(doctor){
    		$scope.editDoctor = doctor;
    		$scope.displayDoctorRecord(doctor);
    	}
    	
    	$scope.displayDoctorRecord = function(doctor) {
    		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
    		var modalContainer = angular.element(document.querySelector('#modalContainer'));
    		var close = angular.element(document.querySelector('#close'));
    		$scope.doctor = doctor;
    		modalWrapper.addClass("showModalWrapper");
    		modalContainer.addClass("showModalContainer");
    		close.on('click', function() {
    			modalContainer.removeClass("showModalContainer");
    			modalWrapper.removeClass("showModalWrapper");
    		})
    	}
    	
    	$scope.selectMedicine = function(medicine){
    		$scope.editMedicine = medicine;
    		$scope.displayMedicineRecord(medicine);
    	}
    	
    	$scope.displayMedicineRecord = function(medicine) {
    		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
    		var modalContainer = angular.element(document.querySelector('#modalContainer'));
    		var close = angular.element(document.querySelector('#close'));
    		$scope.medicine = medicine;
    		modalWrapper.addClass("showModalWrapper");
    		modalContainer.addClass("showModalContainer");
    		close.on('click', function() {
    			modalContainer.removeClass("showModalContainer");
    			modalWrapper.removeClass("showModalWrapper");
    		})
    	}
    	
    	$scope.selectDisease = function(disease){
    		$scope.editDisease = disease;
    		$scope.displayDiseaseRecord(disease);
    	}
    	
    	$scope.displayDiseaseRecord = function(disease) {
    		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
    		var modalContainer = angular.element(document.querySelector('#modalContainer'));
    		var close = angular.element(document.querySelector('#close'));
    		$scope.disease = disease;
    		modalWrapper.addClass("showModalWrapper");
    		modalContainer.addClass("showModalContainer");
    		close.on('click', function() {
    			modalContainer.removeClass("showModalContainer");
    			modalWrapper.removeClass("showModalWrapper");
    		})
    	}
    	
    	$scope.selectSymptom = function(symptom){
    		$scope.editSymptom = symptom;
    		$scope.displaySymptomRecord(symptom);
    	}
    	
    	$scope.displaySymptomRecord = function(symptom) {
    		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
    		var modalContainer = angular.element(document.querySelector('#modalContainer'));
    		var close = angular.element(document.querySelector('#close'));
    		$scope.symptom = symptom;
    		modalWrapper.addClass("showModalWrapper");
    		modalContainer.addClass("showModalContainer");
    		close.on('click', function() {
    			modalContainer.removeClass("showModalContainer");
    			modalWrapper.removeClass("showModalWrapper");
    		})
    	}
    	
    	$scope.getAdminList();
    	$scope.getDoctorList();
    	$scope.getMedicineList();
    	$scope.getDiseaseList();
    	$scope.getSymptomList();
});   