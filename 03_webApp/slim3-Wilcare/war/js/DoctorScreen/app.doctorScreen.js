wilcare.config(["$routeProvider", function($routeProvider) {
	$routeProvider
	.when("/", {
		resolve:	 {
			'check': function($location, $localStorage) {
				if($localStorage != undefined) {
					if($localStorage.user == "doctor")
						$location.path("/doctor")
				}
			}
		},
		templateUrl: "../doctorScreen/doctorRecords.html"})
	.when("/doctor", {
		templateUrl: "../doctorScreen/doctor.html"})
	.when("/records", {
		templateUrl: "../doctorScreen/doctorRecords.html"})
	.when("/medicines", {
		templateUrl: "../doctorScreen/doctorMedicines.html"})
	.when("/diseases", {
		templateUrl: "../doctorScreen/doctorDiseases.html"})
	.when("/addPatient", {
		templateUrl: "../doctorScreen/doctorAddPatient.html"})
	.when("/logout", {
		templateUrl: "../Index.html"})
}]);

wilcare.controller('doctorController', function($scope, $http, $httpParamSerializer, $timeout) {
	console.log("wilcareDoctor.wilcareDoctorController " + "start");
	
	$scope.monthArray = [
		{optionValue: 0 , numValue: "00", displayValue: "Month"},
		{optionValue: 1 , numValue: "01", displayValue: "January"},
		{optionValue: 2 , numValue: "02", displayValue: "February"},
		{optionValue: 3 , numValue: "03", displayValue: "March"},
		{optionValue: 4 , numValue: "04", displayValue: "April"},
		{optionValue: 5 , numValue: "05", displayValue: "May"},
		{optionValue: 6 , numValue: "06", displayValue: "June"},
		{optionValue: 7 , numValue: "07", displayValue: "July"},
		{optionValue: 8 , numValue: "08", displayValue: "August"},
		{optionValue: 9 , numValue: "09", displayValue: "September"},
		{optionValue: 10, numValue: "10", displayValue: "October"},
		{optionValue: 11, numValue: "11", displayValue: "November"},
		{optionValue: 12, numValue: "12", displayValue: "December"}
	];

	$scope.sexArray = [
		{optionValue: 0, displayValue: "Male"},
		{optionValue: 1, displayValue: "Female"},
	];

	$scope.patient = {
    		id: "",
    		firstName: "", 
    		middleName: "",
    		lastName: "",
    		sex: "",
    		birthday: "",
    		admissionDate: "",
    		dischargeDate: "not set",
    		bloodType: "",
    		condition: "",
    		status: true,
    		totalBill: 0.0   		
    }
    
    $scope.editPatient = {
		id: "",
		diseaseID: [],
		symptomID: [],
		doctorID: [],
		medicineID: [],   	
		firstName: "", 
		middleName: "",
		lastName: "",
		birthday: "",
		sex: "",
		admissionDate: "",
		dischargeDate: "not set",
		bloodType: "",
		condition: "",
		status: true,
		totalBill: 0.0    		
    }
	
	$scope.Patient = function(id, firstName, middleName, lastName, birthday, sex, admissionDate, dischargeDate, bloodType, condition, status, totalBill ) {
    	this.id = id;
    	this.firstName = firstName;
    	this.middleName = middleName;
    	this.lastName = lastName;
    	this.birthday = birthday;
    	this.sex = sex;
    	this.admissionDate  = admissionDate;
    	this.dischargeDate = dischargeDate;
    	this.bloodType = bloodType;
    	this.condition = condition;
    	this.status = status;
    	this.totalBill = totalBill;
    }
	
	$scope.EditPatient = function(id,symptomID,medicineID,doctorID,diseaseID, firstName, middleName, lastName, birthday, sex, admissionDate, dischargeDate, bloodType, condition, status, totalBill ) {
    	this.id = id;
    	this.symptomID = symptomID;
    	this.doctorID = doctorID;
    	this.diseaseID = diseaseID;
    	this.medicineID = medicineID;
    	this.firstName = firstName;
    	this.middleName = middleName;
    	this.lastName = lastName;
    	this.birthday = birthday;
    	this.sex = sex;
    	this.admissionDate  = admissionDate;
    	this.dischargeDate = dischargeDate;
    	this.bloodType = bloodType;
    	this.condition = condition;
    	this.status = status;
    	this.totalBill = totalBill;
    }
	
	$scope.extraInfo = {
		birthMonth: "",
		birthDay: "",
		birthYear: "",
		dischargeMonth: "",
		dischargeDay: "",
		dischargeYear: ""
	}
	
	$scope.patients = [];
	$scope.medicines = [];
	$scope.diseases = [];
    $scope.mode = "Add Patient";

	$scope.initAddPatient = function(){
		$scope.extraInfo.birthMonth = $scope.dischargeMonth = $scope.monthArray[0];
		$scope.patient.sex = $scope.sexArray[0];
	}

	$scope.submit = function() {
    	//insert
    	if($scope.mode === "Add Patient") {
    		$scope.addPatient();
    		$scope.clear();
    	}
    	//update
    	else {
    		$scope.mode = "Add Patient";
    	    $scope.updatePatient();
    	}
    }
	
	$scope.addPatient = function() {
    	const url = '/PatientCreate';
    	$scope.setAdmissionDate();
    	$scope.patient.birthday = $scope.extraInfo.birthMonth.displayValue + " " + $scope.extraInfo.birthDay + ", " + $scope.extraInfo.birthYear;
    	$scope.patient.sex = $scope.patient.sex.displayValue;
        var patient = new $scope.Patient(
        		$scope.patient.id,  
        		$scope.patient.firstName, 
        		$scope.patient.middleName, 
        		$scope.patient.lastName,
        		$scope.patient.birthday, 
        		$scope.patient.sex, 
        		$scope.patient.admissionDate, 
        		$scope.patient.dischargeDate,
        		$scope.patient.bloodType,
        		$scope.patient.condition,
        		true, 
        		$scope.patient.totalBill
        );
        var headers = {
        		headers: {'Content-Type':'application/x-www-form-urlencoded'}           
        };
        $http.post(url, $httpParamSerializer(patient), headers)
        	.then(
        			response => {
        				$scope.getPatients();
        				$scope.clear();
        			},
        			error => {
        				console.log('something went wrong in adding data');
        			}
        	)
    }
	
	$scope.updatePatient = function(){
		const url = '/PatientUpdate';
		$scope.editPatient.birthday = $scope.birthMonth + " " + $scope.birthDay + ", " + $scope.birthYear;
		$scope.editPatient.dischargeDate = $scope.dischargeMonth + " " + $scope.dischargeDay + ", " + $scope.dischargeYear;
		var patient = new $scope.EditPatient(
				$scope.editPatient.id,
				$scope.editPatient.symptomID,
				$scope.editPatient.medicineID,
				$scope.editPatient.doctorID,
				$scope.editPatient.diseaseID,
				$scope.editPatient.firstName,
				$scope.editPatient.middleName,
				$scope.editPatient.lastName,
				$scope.editPatient.birthday,
				$scope.editPatient.sex,
				$scope.editPatient.admissionDate,
				$scope.editPatient.dischargeDate,
				$scope.editPatient.bloodType,
				$scope.editPatient.condition,
				$scope.editPatient.status,
				$scope.editPatient.totalBill
		);
		console.log(patient);
		var headers = {
		     headers: {'Content-Type':'application/json'}  
		};
		$http.post(url, patient, headers)
			.then(
				response => {
					$scope.getPatients();
					$scope.clear();
					console.log(patient);
				},
				error => {
					console.log('something went wrong in adding data');
				}
			)
	}

	$scope.setAdmissionDate = function() {
		var today = new Date();
		$scope.admissionDay = today.getDate();
		var month = today.getMonth() + 1; //January is 0!
		$scope.admissionYear = today.getFullYear();
		switch(month) {
		case 1: $scope.admissionMonth = "January";
			break;
		case 2: $scope.admissionMonth = "February";
		break;
		case 3: $scope.admissionMonth = "March";
		break;
		case 4: $scope.admissionMonth = "April";
		break;
		case 5: $scope.admissionMonth = "May";
		break;
		case 6: $scope.admissionMonth = "June";
		break;
		case 7: $scope.admissionMonth = "July";
		break;
		case 8: $scope.admissionMonth = "August";
		break;
		case 9: $scope.admissionMonth = "September";
		break;
		case 10: $scope.admissionMonth = "October";
		break;
		case 11: $scope.admissionMonth = "November";
		break;
		default: $scope.admissionMonth = "December";
		break;	
		}
		$scope.patient.admissionDate = $scope.admissionMonth + " " + $scope.admissionDay + ", " + $scope.admissionYear;
	}

	$scope.getPatients = function() {
    	const url = '/PatientList';
    	$http.get(url)
    		.then(
				response => {
					$scope.patients = response.data.patientList;
					console.log($scope.patients);
				},
				error => {
					console.log('something went wrong in retrieving data');
				}
    		)
    }
	
	$scope.filterMedicine = function(){
		
	}
	
	$scope.deletePatient = function(){
		var index = $scope.patients.indexOf($scope.editPatient);
		var url = '/PatientDelete';
		var headers = {
				headers: {'Content-Type':'application/json'}
	    };
    	 $http.post(url, $scope.editPatient, headers)
      	.then(
      			response => {
      				console.log('delete successful');
      				$scope.patients.splice(index, 1);
      			},
      			error => {
      				console.log('something went wrong in deleting data');
      			}
      	)
	}
	
	$scope.clear = function() {
    	for(p in $scope.patient) {
    		$scope.patient[p] = "";
    	}
    	$scope.initAddPatient();
    	$scope.extraInfo.birthMonth = $scope.monthArray[0];
    	$scope.extraInfo.birthDay = "";
    	$scope.extraInfo.birthYear = "";
    	$scope.dischargeDate = "not set";
    	$scope.bloodType= "";
    	$scope.condition= "";
    	$scope.status = true;
    	$scope.totalBill = 0.0;
    }
	
	$scope.selectRecord = function(patient){
		$scope.editPatient = patient;
		$scope.displayPatientRecord(patient);
	}
	
	$scope.displayPatientRecord = function(patient) {
		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
		var modalContainer = angular.element(document.querySelector('#modalContainer'));
		var close = angular.element(document.querySelector('#close'));
		$scope.patient = patient;
		modalWrapper.addClass("showModalWrapper");
		modalContainer.addClass("showModalContainer");
		close.on('click', function() {
			modalContainer.removeClass("showModalContainer");
			modalWrapper.removeClass("showModalWrapper");
		})
	}
	
	$scope.getMedicines = function() {
		const url = '/MedicineList';
		$http.get(url)
			.then(
					response => {
						$scope.medicines = response.data.medicineList;
						console.log('success' + $scope.medicines);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
	$scope.getDiseases = function() {
		const url = '/DiseaseList';
		$http.get(url)
			.then(
					response => {
						$scope.diseases = response.data.diseaseList;
						console.log('success' + $scope.diseases);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
	$scope.initAddPatient();
	$scope.getPatients();
	$scope.getMedicines();
	$scope.getDiseases();
});
