wilcare.config(["$routeProvider", function($routeProvider) {
	$routeProvider
	.when("/", {
		resolve: {
			'check': function($location, $localStorage) {
				if($localStorage != undefined) {
					if($localStorage.user == "doctor")
						$location.path("/doctor")
				}
			}
		},
		templateUrl: "../html/mainScreen/login.html",
		controller: "loginController"})
}])
