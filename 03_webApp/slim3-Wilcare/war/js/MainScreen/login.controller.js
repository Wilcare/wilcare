wilcare.controller("loginController", function($scope, $http, $localStorage, $location){
	console.log("login.controller.js " + "start");
	
	$scope.doctors = [];
	$scope.admins = [];
	
	$scope.login = function(){
		console.log("wilcareLogin.login " + "start");
		
		var id = $scope.setID($scope.username);
		var type = $scope.setType($scope.username);
		
		if(id != -1) {
			if($scope.isPasswordCorrect(id, $scope.password)) {
				if(type == "doctor") {				
					window.location = "../html/DoctorScreen/doctor.html";
				}
				else {
					window.location = "../html/AdminScreen/admin.html";				
				}
			}
			else {
				alert("Password is incorrect!");
			}
		}
		
		else {
			alert("User does not exist!");
		}
		console.log("wilcareLogin.login " + "end");
	}

	$scope.isPasswordCorrect = function(id, password) {
		var valid = false;
		if(password == $scope.doctors[id].password)
			valid = true;
		else if(password == $scope.admins[id].password)
			valid = true;
		return valid;
	}

	$scope.setID = function(username) {
		var id = -1;
		for(var i = 0; i < $scope.doctors.length; i++) {
			if(username == $scope.doctors[i].username) {
				id = i;	
				break;
			}
		}
		if(id == -1) {
			for(var i = 0; i < $scope.admins.length; i++) {
				if(username == $scope.admins[i].username) {
					id = i;	
					break;
				}
			}
		}
		return id;
	}
	
	$scope.setType = function(username) {
		var type = "";
		for(var i = 0; i < $scope.doctors.length; i++) {
			if(username == $scope.doctors[i].username) {
				type = $scope.doctors[i].type;
				break;
			}
		}
		if(type == "") {
			for(var i = 0; i < $scope.admins.length; i++) {
				if(username == $scope.admins[i].username) {
					type = $scope.admins[i].type;	
					break;
				}
			}
		}
		return type;
	}
	
	$scope.getDoctors = function() {
    	const url = '/DoctorList';
    	$http.get(url)
    		.then(
				response => {
					$scope.doctors = response.data.doctorList;
					console.log('success' + $scope.doctors);
				},
				error => {
					console.log('something went wrong in retrieving data');
				}
    		)
    }
	
	$scope.getAdmins = function() {
    	const url = '/AdminList';
    	$http.get(url)
    		.then(
				response => {
					$scope.admins = response.data.adminList;
					console.log('success' + $scope.admins);
				},
				error => {
					console.log('something went wrong in retrieving data');
				}
    		)
    }
	
	$scope.getDoctors();
	$scope.getAdmins();
})