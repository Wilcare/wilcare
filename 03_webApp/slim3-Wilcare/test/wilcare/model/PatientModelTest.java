package wilcare.model;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PatientModelTest extends AppEngineTestCase {

    private PatientModel model = new PatientModel();

    @Test
    public void test() throws Exception {
        assertThat(model, is(notNullValue()));
    }
}
