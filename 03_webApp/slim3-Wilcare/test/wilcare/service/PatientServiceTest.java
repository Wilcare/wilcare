package wilcare.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class PatientServiceTest extends AppEngineTestCase {

    private PatientService service = new PatientService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
