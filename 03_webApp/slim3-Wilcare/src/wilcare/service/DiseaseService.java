    package wilcare.service;

import java.util.List;
import wilcare.dao.DiseaseDao;
import wilcare.dto.DiseaseDto;
import wilcare.dto.DiseaseListDto;
import wilcare.model.DiseaseModel;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/04/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DiseaseService {
    DiseaseDao objectDao =  new DiseaseDao();
    
    public DiseaseDto insertDisease(DiseaseDto inputDto) {
        DiseaseModel objectModel = new DiseaseModel(); 
        
        System.out.println("DiseaseService.insertDisease " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseName(inputDto.getDiseaseName()); 
        objectModel.setDiseaseDescription(inputDto.getDiseaseDescription());
        objectModel.setMedicineID(inputDto.getMedicineID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        
        try {
            DiseaseModel resultModel = this.objectDao.getDiseaseByName(objectModel);
            
            if (resultModel!=null) {
                // Item must not be inserted
                System.out.println(" Disease Name already exist");
            } else {
                // add the item to the datastore
                try {
                    this.objectDao.insertDisease(objectModel);
                    System.out.println("-------Adding Succesful-------");
                } catch (Exception e) {
                    System.out.println("Exception in adding item: " + e.toString());
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Adding item: " + e.toString());
        }        
        System.out.println("DiseaseService.insertDisease " + "end");
        return inputDto;
    }
    
    public DiseaseDto updateDisease(DiseaseDto inputDto) {
        
        DiseaseModel objectModel = new DiseaseModel();  
        
        System.out.println("DiseaseService.updateDisease " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseName(inputDto.getDiseaseName());      
        objectModel.setMedicineID(inputDto.getMedicineID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        objectModel.setDiseaseDescription(inputDto.getDiseaseDescription());
        try {
            // checking if there is already the same item that exists in the datastore.
            DiseaseModel objectModel2 = this.objectDao.getDiseaseById(objectModel);
            
            if (objectModel2 != null) {
                // setting the key in order to properly update the item
                objectModel.setKey(objectModel2.getKey());
                // update the entity to the datastore.
                this.objectDao.updateDisease(objectModel);
                System.out.println("Updated Disease Information");
            } else {
                // updating was canceled.
                System.out.println("There is no item with the same id.");
            }
        } catch (Exception e) {
            System.out.println("Exception in Updating item: " + e.toString());
        }
        System.out.println("DiseaseService.updateDisease " + "end");
       
        return inputDto;
        
    }
    public DiseaseDto deleteDisease(DiseaseDto inputDto) {
        DiseaseModel objectModel = new DiseaseModel(); 
        System.out.println("DiseaseService.updateDisease " + "start");
       
        objectModel.setId(inputDto.getId());
//        objectModel.setDiseaseName(inputDto.getDiseaseName());      
//        objectModel.setMedicineID(inputDto.getMedicineID());
//        objectModel.setSymptomID(inputDto.getSymptomID());
     
        try{
            DiseaseModel resultModel = this.objectDao.getDiseaseById(objectModel);
            
            if(resultModel !=null){
                objectModel.setKey(resultModel.getKey());
                this.objectDao.deleteDisease(resultModel);
                System.out.println("Deleted Disease");               
            }else {
               System.out.println("No Disease was Found!");
        }       
         }catch(Exception e){           
             System.out.println("Exception in deleting item: " + e.toString());
        }         
        System.out.println("DiseaseService.updateDisease " + "end");
        
        return inputDto;
    }
    
    public DiseaseModel storeDtoToModel(DiseaseDto inputObject) {
        System.out.println("DiseaseService.storeDtoToModel " + "start");
        
        DiseaseModel objectModel = new DiseaseModel(); 
        objectModel.setId(inputObject.getId());
        objectModel.setDiseaseName(inputObject.getDiseaseName());
        objectModel.setMedicineID(inputObject.getMedicineID());
        objectModel.setSymptomID(inputObject.getSymptomID());
        objectModel.setDiseaseDescription(inputObject.getDiseaseDescription());
        System.out.println("DiseaseService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public DiseaseListDto getDiseaseList() {
      //  System.out.println("DiseaseService.getDiseaseList " + "start");
        DiseaseListDto listDto = new DiseaseListDto();
        List<DiseaseModel> modelList = objectDao.getDisease();
        
        for(DiseaseModel s : modelList) {
            DiseaseDto objectDto = new DiseaseDto();
            objectDto.setId(s.getId());
            objectDto.setDiseaseName(s.getDiseaseName());
            objectDto.setDiseaseDescription(s.getDiseaseDescription());
            objectDto.setMedicineID(s.getMedicineID());
            objectDto.setSymptomID(s.getSymptomID());       
            listDto.addDiseaseDto(objectDto);
         //   System.out.println(s.getDiseaseName());
        }        
     //   System.out.println("DiseaseService.getDiseaseList " + "end");
        return listDto; 
       
    }
}

