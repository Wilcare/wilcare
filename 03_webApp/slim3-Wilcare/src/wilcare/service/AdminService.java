package wilcare.service;

import java.util.List;

import wilcare.dao.AdminDao;
import wilcare.dto.AdminDto;
import wilcare.dto.AdminListDto;
import wilcare.model.AdminModel;

public class AdminService {
    AdminDao objectDao =  new AdminDao();
    
    public AdminDto insertAdmin (AdminDto inputDto) {
        AdminModel objectModel = new AdminModel(); 
         
         System.out.println("AdminService.insertAdmin " + "start");
         objectModel.setId(inputDto.getId());
         objectModel.setFirstName(inputDto.getFirstName());
         objectModel.setMiddleName(inputDto.getMiddleName());
         objectModel.setLastName(inputDto.getLastName());
         objectModel.setUsername(inputDto.getUsername());
         objectModel.setPassword(inputDto.getPassword());    
         objectModel.setType(inputDto.getType());
         objectModel.setStatus(inputDto.isStatus());
                  
         try {
             AdminModel resultModel = this.objectDao.getAdminById(objectModel);             
             if (resultModel!=null) {              
                 System.out.println(" Doctor ID already exist");
             } else {               
                 try {
                     this.objectDao.insertAdmin(objectModel);
                     System.out.println("-------Adding Succesful-------");
                 } catch (Exception e) {
                     System.out.println("Exception in adding Admin: " + e.toString());
                 }
             }
         } catch (Exception e) {
             System.out.println("Exception in Adding Admin: " + e.toString());
         }        
         System.out.println("AdminService.insertAdmin " + "end");
         return inputDto;
     }
     
     public AdminDto updateAdmin (AdminDto inputDto) {
         
         AdminModel objectModel = new AdminModel();  
         
         System.out.println("AdminService.updatAdmin " + "start");
         objectModel.setId(inputDto.getId());
         objectModel.setFirstName(inputDto.getFirstName());
         objectModel.setMiddleName(inputDto.getMiddleName());
         objectModel.setLastName(inputDto.getLastName());
         objectModel.setUsername(inputDto.getUsername());
         objectModel.setPassword(inputDto.getPassword());  
         objectModel.setType(inputDto.getType());
         objectModel.setStatus(inputDto.isStatus());
       
         try {
           
             AdminModel objectModel2 = this.objectDao.getAdminById(objectModel);
             
             if (objectModel2 != null) {               
                 objectModel.setKey(objectModel2.getKey());               
                 this.objectDao.updateAdmin(objectModel);
                 System.out.println("Updated Admin Information");
             } else {                 
                 System.out.println("There is no Admin with the same id.");
             }
         } catch (Exception e) {
             System.out.println("Exception in Updating Admin: " + e.toString());
         }
         System.out.println("AdminService.updateAdmin " + "end");
        
         return inputDto;
         
     }
     
     public AdminDto deleteAdmin(AdminDto inputDto) {
         AdminModel objectModel = new AdminModel(); 
         System.out.println("AdminService.deleteDoctor " + "start");
        
         objectModel.setId(inputDto.getId());
         objectModel.setFirstName(inputDto.getFirstName());
         objectModel.setMiddleName(inputDto.getMiddleName());
         objectModel.setLastName(inputDto.getLastName());
         objectModel.setUsername(inputDto.getUsername());
         objectModel.setPassword(inputDto.getPassword()); 
         objectModel.setType(inputDto.getType());
         objectModel.setStatus(inputDto.isStatus());
         
         try{
             AdminModel resultModel = this.objectDao.getAdminById(objectModel);
             
             if(resultModel !=null){
                 objectModel.setKey(resultModel.getKey());
                 this.objectDao.deleteAdmin(resultModel);
                 System.out.println("Deleted Admin");               
             }else {
                System.out.println("No Admin was Found!");
         }       
          }catch(Exception e){           
              System.out.println("Exception in deleting Admin: " + e.toString());
         }         
         System.out.println("AdminService.deleteAdmin " + "end");
         
         return inputDto;
     }
     
     
     public AdminModel storeDtoToModel(AdminDto inputDto) {
         System.out.println("AdminService.storeDtoToModel " + "start");         
         AdminModel objectModel = new AdminModel(); 
         objectModel.setId(inputDto.getId());
         objectModel.setFirstName(inputDto.getFirstName());
         objectModel.setMiddleName(inputDto.getMiddleName());
         objectModel.setLastName(inputDto.getLastName());
         objectModel.setUsername(inputDto.getUsername());
         objectModel.setPassword(inputDto.getPassword());    
         objectModel.setType(inputDto.getType());
         objectModel.setStatus(inputDto.isStatus());
         System.out.println("AdminService.storeDtoToModel " + "end");
         return objectModel;
     }
     
     public AdminListDto getAdminList() {
         AdminListDto listDto = new AdminListDto();
         List<AdminModel> modelList = objectDao.getAdmin();
         
         for(AdminModel s : modelList) {
             AdminDto objectDto = new AdminDto();
             objectDto.setId(s.getId());
             objectDto.setFirstName(s.getFirstName());
             objectDto.setMiddleName(s.getMiddleName());
             objectDto.setLastName(s.getLastName());
             objectDto.setUsername(s.getUsername());
             objectDto.setPassword(s.getPassword()); 
             objectDto.setType(s.getType());
             objectDto.setStatus(s.isStatus());
             
             listDto.addAdminDto(objectDto);
         }
         
         return listDto; 
     }
}
