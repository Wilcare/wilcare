package wilcare.service;

import java.util.List;

import wilcare.dao.DoctorDao;
import wilcare.dto.DoctorDto;
import wilcare.dto.DoctorListDto;
import wilcare.model.DoctorModel;



public class DoctorService {
  DoctorDao objectDao =  new DoctorDao();
    
    public DoctorDto insertDoctor(DoctorDto inputDto) {
       DoctorModel objectModel = new DoctorModel(); 
        
        System.out.println("DiseaseService.insertDisease " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword());    
        objectModel.setPatientID(inputDto.getPatientID()); 
        objectModel.setType(inputDto.getType());
        objectModel.setPrice(inputDto.getPrice());
    
        try {
            DoctorModel resultModel = this.objectDao.getDoctorById(objectModel);
            
            if (resultModel!=null) {
                // Item must not be inserted
                System.out.println(" Doctor ID already exist");
            } else {
                // add the item to the datastore
                try {
                    this.objectDao.insertDoctor(objectModel);
                    System.out.println("-------Adding Succesful-------");
                } catch (Exception e) {
                    System.out.println("Exception in adding item: " + e.toString());
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in Adding item: " + e.toString());
        }        
        System.out.println("DoctorService.insertDoctor " + "end");
        return inputDto;
    }
    
    public DoctorDto updateDoctor(DoctorDto inputDto) {
        
        DoctorModel objectModel = new DoctorModel();  
        
        System.out.println("DoctorService.updateDoctor " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword());     
        objectModel.setPatientID(inputDto.getPatientID()); 
        objectModel.setType(inputDto.getType());
        objectModel.setPrice(inputDto.getPrice());
        try {
            // checking if there is already the same item that exists in the datastore.
            DoctorModel objectModel2 = this.objectDao.getDoctorById(objectModel);
            
            if (objectModel2 != null) {
                // setting the key in order to properly update the item
                objectModel.setKey(objectModel2.getKey());
                // update the entity to the datastore.
                this.objectDao.updateDoctor(objectModel);
                System.out.println("Updated Doctor Information");
            } else {
                // updating was canceled.
                System.out.println("There is no item with the same id.");
            }
        } catch (Exception e) {
            System.out.println("Exception in Updating item: " + e.toString());
        }
        System.out.println("DoctorService.updateDoctor " + "end");
       
        return inputDto;
        
    }
    public DoctorDto deleteDoctor(DoctorDto inputDto) {
        DoctorModel objectModel = new DoctorModel(); 
        System.out.println("DoctorService.deleteDoctor " + "start");
       
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword()); 
        objectModel.setPatientID(inputDto.getPatientID()); 
        objectModel.setType(inputDto.getType());
        objectModel.setPrice(inputDto.getPrice());
        
        try{
            DoctorModel resultModel = this.objectDao.getDoctorById(objectModel);
            
            if(resultModel !=null){
                objectModel.setKey(resultModel.getKey());
                this.objectDao.deleteDoctor(resultModel);
                System.out.println("Deleted Doctor");               
            }else {
               System.out.println("No Doctor was Found!");
        }       
         }catch(Exception e){           
             System.out.println("Exception in deleting item: " + e.toString());
        }         
        System.out.println("DoctorService.deleteDoctor " + "end");
        
        return inputDto;
    }
    
    public DoctorModel storeDtoToModel(DoctorDto inputDto) {
        System.out.println("MedicineService.storeDtoToModel " + "start");
        
        DoctorModel objectModel = new DoctorModel(); 
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword());    
        objectModel.setPatientID(inputDto.getPatientID());
        objectModel.setType(inputDto.getType());
        objectModel.setPrice(inputDto.getPrice());
        System.out.println("MedicineService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public DoctorListDto getDoctorList() {
        DoctorListDto listDto = new DoctorListDto();
        List<DoctorModel> modelList = objectDao.getDoctor();
        
        for(DoctorModel s : modelList) {
            DoctorDto objectDto = new DoctorDto();
            objectDto.setId(s.getId());
            objectDto.setFirstName(s.getFirstName());
            objectDto.setMiddleName(s.getMiddleName());
            objectDto.setLastName(s.getLastName());
            objectDto.setUsername(s.getUsername());
            objectDto.setPassword(s.getPassword());         
            objectDto.setPatientID(s.getPatientID()); 
            objectDto.setType(s.getType());
            objectDto.setPrice(s.getPrice());
            listDto.addDoctorDto(objectDto);
        }
        
        return listDto; 
    }
  
}
