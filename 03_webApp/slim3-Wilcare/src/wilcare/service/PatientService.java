package wilcare.service;

import java.util.List;

import wilcare.dao.PatientDao;
import wilcare.dto.PatientDto;
import wilcare.dto.PatientListDto;
import wilcare.model.PatientModel;


/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/04/2016] 1.00 Wilson Justine G. Sison - initial codes
 */

public class PatientService {
    PatientDao objectDao =  new PatientDao();
    
    public PatientDto insertPatient(PatientDto inputDto) {
        PatientModel objectModel = new PatientModel(); 
        System.out.println("PatientService.insertSymptom " + "start");
        
        
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseID(inputDto.getDiseaseID());
        objectModel.setDoctorID(inputDto.getDoctorID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setSex(inputDto.getSex());
        objectModel.setBirthday(inputDto.getBirthday());
        objectModel.setAdmissionDate(inputDto.getAdmissionDate());
        objectModel.setBloodType(inputDto.getBloodType());
        objectModel.setCondition(inputDto.getCondition());
        objectModel.setStatus(inputDto.isStatus());
      
        
        try{
            PatientModel resultModel = this.objectDao.getPatientByName(objectModel);
            if(resultModel!=null){
                System.out.println(" Patient already exist");                
            }else{
                try{
                    this.objectDao.insertPatient(objectModel);
                    System.out.println("-------Adding Succesful-------");
                }catch(Exception e){
                    System.out.println("Exception in adding item: " + e.toString());
                }                
            }
            
        }catch(Exception e){
            System.out.println("Exception in adding item: " + e.toString());            
        }
        System.out.println("PatientService.insertSymptom " + "end");
        return inputDto;
    }
    
    public PatientDto updatePatient(PatientDto inputDto) {
        PatientModel objectModel = new PatientModel(); 
        System.out.println("PatientService.insertSymptom " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseID(inputDto.getDiseaseID());
        objectModel.setDoctorID(inputDto.getDoctorID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setSex(inputDto.getSex());
        objectModel.setBirthday(inputDto.getBirthday());
        objectModel.setAdmissionDate(inputDto.getAdmissionDate());
        objectModel.setDischargeDate(inputDto.getDischargeDate());
        objectModel.setBloodType(inputDto.getBloodType());
        objectModel.setCondition(inputDto.getCondition());
        objectModel.setStatus(inputDto.isStatus());
        objectModel.setTotalBill(inputDto.getTotalBill());
        try{
            PatientModel objectModel2 = this.objectDao.getPatientById(objectModel);
            if(objectModel2!=null){
                objectModel.setKey(objectModel2.getKey());
                this.objectDao.updatePatient(objectModel);
                System.out.println("Updated Patient Information");
            } else {               
                System.out.println("There is no item with the same id.");
            }
            
        }catch(Exception e){
            System.out.println("Exception in adding item: " + e.toString());            
        }
        System.out.println("PatientService.insertSymptom " + "end");
        return inputDto;
    }
    
    public PatientDto deletePatient(PatientDto inputDto) {
        PatientModel objectModel = new PatientModel(); 
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseID(inputDto.getDiseaseID());
        objectModel.setDoctorID(inputDto.getDoctorID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setSex(inputDto.getSex());
        objectModel.setBirthday(inputDto.getBirthday());
        objectModel.setAdmissionDate(inputDto.getAdmissionDate());
        objectModel.setDischargeDate(inputDto.getDischargeDate());
        objectModel.setBloodType(inputDto.getBloodType());
        objectModel.setCondition(inputDto.getCondition());        
        objectModel.setStatus(inputDto.isStatus());
        objectModel.setTotalBill(inputDto.getTotalBill());
        System.out.println("PatientService.insertSymptom " + "start");
        try{
            
            PatientModel resultModel = this.objectDao.getPatientById(objectModel);
    
            if(resultModel != null){
                this.objectDao.deletePatient(resultModel);       
                System.out.println("Deleted Patient");               
            }else {
               System.out.println("No Patient was Found!");
              }       
            
        }catch(Exception e){
            System.out.println("Exception in deleting item: " + e.toString());            
        }
        System.out.println("PatientService.insertSymptom " + "end");
        return inputDto;
    }
    
    public PatientModel storeDtoToModel(PatientDto inputDto) {
        System.out.println("PatientService.storeDtoToModel " + "start");
        
        PatientModel objectModel = new PatientModel(); 
        objectModel.setId(inputDto.getId());
        objectModel.setDiseaseID(inputDto.getDiseaseID());
        objectModel.setDoctorID(inputDto.getDoctorID());
        objectModel.setSymptomID(inputDto.getSymptomID());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setSex(inputDto.getSex());
        objectModel.setBirthday(inputDto.getBirthday());
        objectModel.setAdmissionDate(inputDto.getAdmissionDate());
        objectModel.setDischargeDate(inputDto.getDischargeDate());
        objectModel.setBloodType(inputDto.getBloodType());
        objectModel.setCondition(inputDto.getCondition());
        objectModel.setStatus(inputDto.isStatus());
        objectModel.setTotalBill(inputDto.getTotalBill());
        
        
        System.out.println("PatientService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public PatientListDto  getPatientList() {
        PatientListDto listDto = new PatientListDto();
        List<PatientModel> modelList = objectDao.getPatient();
        
        for(PatientModel s : modelList) {
            PatientDto objectDto = new PatientDto(); 
            objectDto.setId(s.getId());
            objectDto.setDiseaseID(s.getDiseaseID());
            objectDto.setDoctorID(s.getDoctorID());
            objectDto.setSymptomID(s.getSymptomID());
            objectDto.setMedicineID(s.getMedicineID());
            objectDto.setFirstName(s.getFirstName());
            objectDto.setMiddleName(s.getMiddleName());
            objectDto.setLastName(s.getLastName());
            objectDto.setSex(s.getSex());
            objectDto.setBirthday(s.getBirthday());
            objectDto.setAdmissionDate(s.getAdmissionDate());
            objectDto.setDischargeDate(s.getDischargeDate());
            objectDto.setBloodType(s.getBloodType());
            objectDto.setCondition(s.getCondition());
            objectDto.setStatus(s.isStatus());
            objectDto.setTotalBill(s.getTotalBill());         
            listDto.addPatientDto(objectDto);
        }
        
        return listDto; 
    }
    
}
