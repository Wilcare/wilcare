package wilcare.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-21 18:43:04")
/** */
public final class DiseaseModelMeta extends org.slim3.datastore.ModelMeta<wilcare.model.DiseaseModel> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.DiseaseModel> diseaseDescription = new org.slim3.datastore.StringAttributeMeta<wilcare.model.DiseaseModel>(this, "diseaseDescription", "diseaseDescription");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.DiseaseModel> diseaseName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.DiseaseModel>(this, "diseaseName", "diseaseName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.DiseaseModel, java.util.List<java.lang.Long>, java.lang.Long> medicineID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.DiseaseModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "medicineID", "medicineID", java.util.List.class);

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.DiseaseModel, java.util.List<java.lang.Long>, java.lang.Long> symptomID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.DiseaseModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "symptomID", "symptomID", java.util.List.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.DiseaseModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final DiseaseModelMeta slim3_singleton = new DiseaseModelMeta();

    /**
     * @return the singleton
     */
    public static DiseaseModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public DiseaseModelMeta() {
        super("DiseaseModel", wilcare.model.DiseaseModel.class);
    }

    @Override
    public wilcare.model.DiseaseModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        wilcare.model.DiseaseModel model = new wilcare.model.DiseaseModel();
        model.setDiseaseDescription((java.lang.String) entity.getProperty("diseaseDescription"));
        model.setDiseaseName((java.lang.String) entity.getProperty("diseaseName"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setMedicineID(toList(java.lang.Long.class, entity.getProperty("medicineID")));
        model.setSymptomID(toList(java.lang.Long.class, entity.getProperty("symptomID")));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("diseaseDescription", m.getDiseaseDescription());
        entity.setProperty("diseaseName", m.getDiseaseName());
        entity.setProperty("id", m.getId());
        entity.setProperty("medicineID", m.getMedicineID());
        entity.setProperty("symptomID", m.getSymptomID());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        wilcare.model.DiseaseModel m = (wilcare.model.DiseaseModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getDiseaseDescription() != null){
            writer.setNextPropertyName("diseaseDescription");
            encoder0.encode(writer, m.getDiseaseDescription());
        }
        if(m.getDiseaseName() != null){
            writer.setNextPropertyName("diseaseName");
            encoder0.encode(writer, m.getDiseaseName());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMedicineID() != null){
            writer.setNextPropertyName("medicineID");
            writer.beginArray();
            for(java.lang.Long v : m.getMedicineID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getSymptomID() != null){
            writer.setNextPropertyName("symptomID");
            writer.beginArray();
            for(java.lang.Long v : m.getSymptomID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected wilcare.model.DiseaseModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        wilcare.model.DiseaseModel m = new wilcare.model.DiseaseModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("diseaseDescription");
        m.setDiseaseDescription(decoder0.decode(reader, m.getDiseaseDescription()));
        reader = rootReader.newObjectReader("diseaseName");
        m.setDiseaseName(decoder0.decode(reader, m.getDiseaseName()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("medicineID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("medicineID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setMedicineID(elements);
            }
        }
        reader = rootReader.newObjectReader("symptomID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("symptomID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setSymptomID(elements);
            }
        }
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}