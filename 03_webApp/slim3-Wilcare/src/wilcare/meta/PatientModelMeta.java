package wilcare.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-21 18:43:05")
/** */
public final class PatientModelMeta extends org.slim3.datastore.ModelMeta<wilcare.model.PatientModel> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> admissionDate = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "admissionDate", "admissionDate");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> birthday = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "birthday", "birthday");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> bloodType = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "bloodType", "bloodType");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> condition = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "condition", "condition");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> dischargeDate = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "dischargeDate", "dischargeDate");

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long> diseaseID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "diseaseID", "diseaseID", java.util.List.class);

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long> doctorID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "doctorID", "doctorID", java.util.List.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> firstName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "firstName", "firstName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> lastName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "lastName", "lastName");

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long> medicineID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "medicineID", "medicineID", java.util.List.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> middleName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "middleName", "middleName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel> sex = new org.slim3.datastore.StringAttributeMeta<wilcare.model.PatientModel>(this, "sex", "sex");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Boolean> status = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Boolean>(this, "status", "status", boolean.class);

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long> symptomID = new org.slim3.datastore.CollectionAttributeMeta<wilcare.model.PatientModel, java.util.List<java.lang.Long>, java.lang.Long>(this, "symptomID", "symptomID", java.util.List.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Double> totalBill = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Double>(this, "totalBill", "totalBill", double.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.PatientModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final PatientModelMeta slim3_singleton = new PatientModelMeta();

    /**
     * @return the singleton
     */
    public static PatientModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public PatientModelMeta() {
        super("PatientModel", wilcare.model.PatientModel.class);
    }

    @Override
    public wilcare.model.PatientModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        wilcare.model.PatientModel model = new wilcare.model.PatientModel();
        model.setAdmissionDate((java.lang.String) entity.getProperty("admissionDate"));
        model.setBirthday((java.lang.String) entity.getProperty("birthday"));
        model.setBloodType((java.lang.String) entity.getProperty("bloodType"));
        model.setCondition((java.lang.String) entity.getProperty("condition"));
        model.setDischargeDate((java.lang.String) entity.getProperty("dischargeDate"));
        model.setDiseaseID(toList(java.lang.Long.class, entity.getProperty("diseaseID")));
        model.setDoctorID(toList(java.lang.Long.class, entity.getProperty("doctorID")));
        model.setFirstName((java.lang.String) entity.getProperty("firstName"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setLastName((java.lang.String) entity.getProperty("lastName"));
        model.setMedicineID(toList(java.lang.Long.class, entity.getProperty("medicineID")));
        model.setMiddleName((java.lang.String) entity.getProperty("middleName"));
        model.setSex((java.lang.String) entity.getProperty("sex"));
        model.setStatus(booleanToPrimitiveBoolean((java.lang.Boolean) entity.getProperty("status")));
        model.setSymptomID(toList(java.lang.Long.class, entity.getProperty("symptomID")));
        model.setTotalBill(doubleToPrimitiveDouble((java.lang.Double) entity.getProperty("totalBill")));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("admissionDate", m.getAdmissionDate());
        entity.setProperty("birthday", m.getBirthday());
        entity.setProperty("bloodType", m.getBloodType());
        entity.setProperty("condition", m.getCondition());
        entity.setProperty("dischargeDate", m.getDischargeDate());
        entity.setProperty("diseaseID", m.getDiseaseID());
        entity.setProperty("doctorID", m.getDoctorID());
        entity.setProperty("firstName", m.getFirstName());
        entity.setProperty("id", m.getId());
        entity.setProperty("lastName", m.getLastName());
        entity.setProperty("medicineID", m.getMedicineID());
        entity.setProperty("middleName", m.getMiddleName());
        entity.setProperty("sex", m.getSex());
        entity.setProperty("status", m.isStatus());
        entity.setProperty("symptomID", m.getSymptomID());
        entity.setProperty("totalBill", m.getTotalBill());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        wilcare.model.PatientModel m = (wilcare.model.PatientModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAdmissionDate() != null){
            writer.setNextPropertyName("admissionDate");
            encoder0.encode(writer, m.getAdmissionDate());
        }
        if(m.getBirthday() != null){
            writer.setNextPropertyName("birthday");
            encoder0.encode(writer, m.getBirthday());
        }
        if(m.getBloodType() != null){
            writer.setNextPropertyName("bloodType");
            encoder0.encode(writer, m.getBloodType());
        }
        if(m.getCondition() != null){
            writer.setNextPropertyName("condition");
            encoder0.encode(writer, m.getCondition());
        }
        if(m.getDischargeDate() != null){
            writer.setNextPropertyName("dischargeDate");
            encoder0.encode(writer, m.getDischargeDate());
        }
        if(m.getDiseaseID() != null){
            writer.setNextPropertyName("diseaseID");
            writer.beginArray();
            for(java.lang.Long v : m.getDiseaseID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getDoctorID() != null){
            writer.setNextPropertyName("doctorID");
            writer.beginArray();
            for(java.lang.Long v : m.getDoctorID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getFirstName() != null){
            writer.setNextPropertyName("firstName");
            encoder0.encode(writer, m.getFirstName());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getLastName() != null){
            writer.setNextPropertyName("lastName");
            encoder0.encode(writer, m.getLastName());
        }
        if(m.getMedicineID() != null){
            writer.setNextPropertyName("medicineID");
            writer.beginArray();
            for(java.lang.Long v : m.getMedicineID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getMiddleName() != null){
            writer.setNextPropertyName("middleName");
            encoder0.encode(writer, m.getMiddleName());
        }
        if(m.getSex() != null){
            writer.setNextPropertyName("sex");
            encoder0.encode(writer, m.getSex());
        }
        writer.setNextPropertyName("status");
        encoder0.encode(writer, m.isStatus());
        if(m.getSymptomID() != null){
            writer.setNextPropertyName("symptomID");
            writer.beginArray();
            for(java.lang.Long v : m.getSymptomID()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        writer.setNextPropertyName("totalBill");
        encoder0.encode(writer, m.getTotalBill());
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected wilcare.model.PatientModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        wilcare.model.PatientModel m = new wilcare.model.PatientModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("admissionDate");
        m.setAdmissionDate(decoder0.decode(reader, m.getAdmissionDate()));
        reader = rootReader.newObjectReader("birthday");
        m.setBirthday(decoder0.decode(reader, m.getBirthday()));
        reader = rootReader.newObjectReader("bloodType");
        m.setBloodType(decoder0.decode(reader, m.getBloodType()));
        reader = rootReader.newObjectReader("condition");
        m.setCondition(decoder0.decode(reader, m.getCondition()));
        reader = rootReader.newObjectReader("dischargeDate");
        m.setDischargeDate(decoder0.decode(reader, m.getDischargeDate()));
        reader = rootReader.newObjectReader("diseaseID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("diseaseID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setDiseaseID(elements);
            }
        }
        reader = rootReader.newObjectReader("doctorID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("doctorID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setDoctorID(elements);
            }
        }
        reader = rootReader.newObjectReader("firstName");
        m.setFirstName(decoder0.decode(reader, m.getFirstName()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("lastName");
        m.setLastName(decoder0.decode(reader, m.getLastName()));
        reader = rootReader.newObjectReader("medicineID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("medicineID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setMedicineID(elements);
            }
        }
        reader = rootReader.newObjectReader("middleName");
        m.setMiddleName(decoder0.decode(reader, m.getMiddleName()));
        reader = rootReader.newObjectReader("sex");
        m.setSex(decoder0.decode(reader, m.getSex()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.isStatus()));
        reader = rootReader.newObjectReader("symptomID");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("symptomID");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setSymptomID(elements);
            }
        }
        reader = rootReader.newObjectReader("totalBill");
        m.setTotalBill(decoder0.decode(reader, m.getTotalBill()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}