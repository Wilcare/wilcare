package wilcare.controller;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.common.GlobalConstants;
import wilcare.dto.DiseaseDto;
import wilcare.service.DiseaseService;
import wilcare.utils.JSONValidators;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DiseaseCreateController extends Controller {
    DiseaseService objectService = new DiseaseService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        DiseaseDto objectDto = new DiseaseDto();
              
        System.out.println("DiseaseCreateController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());
            JSONValidators validator = new JSONValidators(this.request); 
            System.out.println(jsonObject);
            
            //validator.add("symptomID", validator.required());
          //  validator.add("medicineID", validator.required());
         //   validator.add("diseaseName", validator.required());
         //   validator.add("diseaseDescription", validator.required());
           
            if(validator.validate()){                
         //    jsonObject = new JSONObject(this.request.getReader().readLine());       
             
                      
             JSONArray  symptomID = jsonObject.getJSONArray("symptomID");
             JSONArray  medicineID = jsonObject.getJSONArray("medicineID");
             
             String diseaseName = jsonObject.getString("diseaseName");
             String diseaseDescription =  jsonObject.getString("diseaseDescription");
             List<Long> symptomIDList = new ArrayList<Long>();
             List<Long> medicineIDList = new ArrayList<Long>();     
            
             for (int i = 0; i < symptomID.length(); i++){
                 symptomIDList.add(symptomID.getLong(i));
             }
             for (int i = 0; i < medicineID.length(); i++){
                 medicineIDList.add(medicineID.getLong(i));
             }             
             
            // setting the values of DTO from the request
             objectDto.setDiseaseName(diseaseName);
             objectDto.setDiseaseDescription(diseaseDescription);
             objectDto.setMedicineID(medicineIDList);
             objectDto.setSymptomID(symptomIDList);
             
            this.objectService.insertDisease(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
            
        }catch(Exception e){
            System.out.println("DiseaseCreateController.run.exception " + e.toString());
            objectDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }    
        
        jsonObject.put("errorList", objectDto.getErrorList());
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        response.getWriter().write(jsonObject.toString());
        System.out.println("DiseaseCreateController.run " + "end");
        return null;
    }
}


