package wilcare.controller;



import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.DiseaseDto;
import wilcare.service.DiseaseService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DiseaseDeleteController extends Controller {
    DiseaseService objectService = new DiseaseService();
    @Override
    public Navigation run() throws Exception {
        DiseaseDto objectDto = new DiseaseDto();
        JSONObject jsonObject;
        System.out.println("DiseaseDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());           
            Long id = jsonObject.getLong("id");           
            objectDto.setId(id); 
             this.objectService.deleteDisease(objectDto);            
        }catch(Exception e){
            System.out.println("DiseaseDeleteController.run.exception " + e.toString());
        }
        System.out.println("DiseaseDeleteController.run " + "end");
     
        return null;
    }
}
//Slim3
//String id = request.getParameter("id");                   
//objectDto.setId(Long.parseLong(id)); 