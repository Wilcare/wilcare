package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.common.GlobalConstants;
import wilcare.dto.MedicineDto;
import wilcare.service.MedicineService;

public class MedicineUpdateController extends Controller {

    MedicineService objectService = new MedicineService();
    JSONObject jsonObject = null ;
    @Override
    public Navigation run() throws Exception {
        MedicineDto objectDto = new MedicineDto();
        System.out.println("MedicinceUpdateController.run " + "start");
        try{
            
            Validators validator = new Validators(this.request); 
            validator.add("id", validator.required());
            validator.add("medicineName", validator.required());
            validator.add("medicineDescription", validator.required());
            validator.add("price", validator.required());
            
            if(validator.validate()){
            jsonObject = new JSONObject(new RequestMap(this.request));
            Long id = jsonObject.getLong("id");
            String medicineName = jsonObject.getString("medicineName");
            String medicineDescription = jsonObject.getString("medicineDescription");
            Double price = jsonObject.getDouble("price");
            
            objectDto.setId(id);
            objectDto.setMedicineName(medicineName);
            objectDto.setMedicineDescription(medicineDescription);
            objectDto.setPrice(price);
            
            this.objectService.updateMedicine(objectDto);
            }
        }catch(Exception e){
            System.out.println("MedicinceUpdateController.run.exception " + e.toString());
            objectDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        response.getWriter().write(jsonObject.toString());
        System.out.println("MedicinceUpdateController.run " + "end");
        return null;
    }
}
