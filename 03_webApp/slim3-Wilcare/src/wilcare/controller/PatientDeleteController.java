package wilcare.controller;


import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.PatientDto;
import wilcare.service.PatientService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class PatientDeleteController extends Controller {
    PatientService objectService = new PatientService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        PatientDto objectDto = new PatientDto();     
        System.out.println("PatientDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());
            
           Long id = jsonObject.getLong("id");           
           objectDto.setId(id);             
            this.objectService.deletePatient(objectDto);
            
        }catch(Exception e){
            System.out.println("PatientDeleteController.run.exception " + e.toString());
        }
        System.out.println("PatientDeleteController.run " + "end");
        return null;
    }
}
//Slim3
//String id = request.getParameter("id");                   
//objectDto.setId(Long.parseLong(id)); 
