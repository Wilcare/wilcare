package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.MedicineDtoList;
import wilcare.service.MedicineService;



public class MedicineListController extends Controller {
    MedicineService objectService = new  MedicineService();
    JSONObject jsonObject = new JSONObject();
    
    @Override
    public Navigation run() throws Exception {
        MedicineDtoList ListDto = objectService.getMedicineList();
        jsonObject.put("medicineList",ListDto.getMedicineList());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
