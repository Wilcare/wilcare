package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.DiseaseListDto;
import wilcare.service.DiseaseService;

public class DiseaseListController extends Controller {

    DiseaseService objectService = new  DiseaseService();
    JSONObject jsonObject = new JSONObject();
    
    @Override
    public Navigation run() throws Exception {
        DiseaseListDto ListDto = objectService.getDiseaseList();
        jsonObject.put("diseaseList",ListDto.getDiseaseList());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
