package wilcare.controller;


import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.common.GlobalConstants;
import wilcare.dto.PatientDto;
import wilcare.service.PatientService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class PatientCreateController extends Controller {
    PatientService objectService = new PatientService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        PatientDto objectDto = new PatientDto();    
        System.out.println("PatientCreateController.run " + "start");
        try{            
            
            Validators validator = new Validators(this.request); 
            validator.add("firstName", validator.required());
            validator.add("middleName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("birthday", validator.required());
            validator.add("sex", validator.required());
            validator.add("admissionDate", validator.required());
            validator.add("bloodType", validator.required());
            validator.add("condition", validator.required());
            validator.add("status", validator.required());
        
            
           
            if(validator.validate()){
            jsonObject = new JSONObject(new RequestMap(this.request));
            String firstName = jsonObject.getString("firstName");
            String middleName = jsonObject.getString("middleName");
            String lastName = jsonObject.getString("lastName");
            String birthday = jsonObject.getString("birthday");
            String sex = jsonObject.getString("sex");
            String admissionDate = jsonObject.getString("admissionDate");
            String bloodType = jsonObject.getString("bloodType");
            String condition = jsonObject.getString("condition");
            Boolean status = jsonObject.getBoolean("status");
                    
  
            objectDto.setFirstName(firstName);
            objectDto.setMiddleName(middleName);
            objectDto.setLastName(lastName);
            objectDto.setBirthday(birthday);
            objectDto.setSex(sex);
            objectDto.setAdmissionDate(admissionDate);
            objectDto.setBloodType(bloodType);
            objectDto.setCondition(condition);
            objectDto.setStatus(status);
          
            
            this.objectService.insertPatient(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("PatientCreateController.run.exception " + e.toString());
            objectDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        response.getWriter().write(jsonObject.toString());
        System.out.println("PatientCreateController.run " + "end");
     
        return null;
    }
}
