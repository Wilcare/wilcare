package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.SymptomListDto;
import wilcare.service.SymptomService;

public class SymptomListController extends Controller {
    SymptomService service = new SymptomService();
    JSONObject json = new JSONObject();
    
    @Override
    public Navigation run() throws Exception {
        SymptomListDto symptomListDto = service.getSymptomList();
        json.put("symptomList",symptomListDto.getSymptomList());
        
        response.setContentType("application/json");
        response.getWriter().write(json.toString());
        
        return null;
    }
}
