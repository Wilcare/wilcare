package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


import wilcare.model.DoctorModel;
import wilcare.meta.DoctorModelMeta;

public class DoctorDao extends DaoBase<DoctorModel>{
    
    
 
    public DoctorModel getDoctorById(DoctorModel inputObject){
        DoctorModel objectModel = new DoctorModel();
        
        System.out.println("DoctorDao.getDoctorById " + "start");
        
        DoctorModelMeta meta = DoctorModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId())).asSingle();
                             
        
        System.out.println("DoctorDao.getDoctorById " + "end");
        return objectModel;
        
    }
       
    
    public void insertDoctor(DoctorModel inputOjbect) {
        System.out.println("DoctorDao.insertDoctor " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
        Key parentKey = KeyFactory.createKey("DoctorModel", inputOjbect.getLastName());
        Key key = Datastore.allocateId(parentKey, "DoctorModel");
        
        //passing the key in DoctorModel
        inputOjbect.setKey(key);
        inputOjbect.setId(key.getId());     
        Datastore.put(inputOjbect);
        transaction.commit();
        
        System.out.println("DoctorDao.insertDoctor " + "end");
        
    }   
      
    public void updateDoctor(DoctorModel inputOjbect) {
        System.out.println("DoctorDao.updateDoctor " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("DoctorDao.updateDoctor " + "end");
    }   
    
    public void deleteDoctor(DoctorModel inputOjbect) {
        System.out.println("DoctorDao.deleteDoctor " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        System.out.println("DoctorDao.deleteDoctor " + "end");
    }   
    public List<DoctorModel> getDoctor() {        
        return Datastore.query(DoctorModel.class).asList();
    }
}
