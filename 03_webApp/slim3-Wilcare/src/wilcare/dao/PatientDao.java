package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


import wilcare.meta.PatientModelMeta;
import wilcare.model.PatientModel;

/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class PatientDao extends DaoBase<PatientModel>{
    
    public PatientModel getPatientByName(PatientModel inputObject){
        PatientModel objectModel = new PatientModel();
        
        System.out.println("PatientDao.getPatientByName " + "start");
        
        PatientModelMeta meta = PatientModelMeta.get();
        objectModel = Datastore.query(meta)
                .filter(meta.lastName.equal(inputObject.getLastName()),
                        meta.firstName.equal(inputObject.getFirstName()),
                        meta.middleName.equal(inputObject.getMiddleName())
                       ).asSingle();
        
        
        System.out.println("PatientDao.getPatientByName " + "end");
        return objectModel;
        
    }
    
    public PatientModel getPatientByNameId(PatientModel inputObject){
        PatientModel objectModel = new PatientModel();
        
        System.out.println("PatientDao.getPatientByNameId " + "start");
        
        PatientModelMeta meta = PatientModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.lastName.equal(inputObject.getLastName())
                               ,meta.id.equal(inputObject.getId())
                                ).asSingle();
        
        System.out.println("PatientDao.getPatientByNameId " + "end");
        return objectModel;
        
    }
       
    public PatientModel getPatientById(PatientModel inputObject){
        
        PatientModel objectModel = new PatientModel();
        
        System.out.println("PatientDao.getPatientByNameId " + "start");
        
        PatientModelMeta meta = PatientModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId()))
                        .asSingle();                            
        
        System.out.println("PatientDao.getPatientByNameId " + "end");
        return objectModel;
        
    }
       
    
    public void insertPatient(PatientModel inputObject) {
        System.out.println("PatientDao.insertPatient " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
        Key parentKey = KeyFactory.createKey("PatientModel", inputObject.getLastName());
        Key key = Datastore.allocateId(parentKey, "PatientModel");
        
        //passing the key in Model
        inputObject.setKey(key);
        inputObject.setId(key.getId());     
        Datastore.put(inputObject);
        transaction.commit();
        
        System.out.println("PatientDao.insertPatient " + "end");
    }   
    
    public void updatePatient(PatientModel inputOjbect) {
        System.out.println("PatientDao.updatePatient " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("PatientDao.updatePatient " + "end");
    }   
    
    public void deletePatient(PatientModel inputOjbect) {
        System.out.println("PatientDao.deletPatient " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        System.out.println("PatientDao.deletPatient " + "end");
    }   

    public List<PatientModel> getPatient() {
        
        return Datastore.query(PatientModel.class).asList();
    }
}
