package wilcare.dto;

import java.util.List;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DiseaseDto extends BaseDto {
    private Long id;
    private String diseaseName;
    private List<Long> symptomID; 
    private List<Long> medicineID; 
    private String diseaseDescription;
    public String getDiseaseDescription() {
        return diseaseDescription;
    }
    public void setDiseaseDescription(String diseaseDescription) {
        this.diseaseDescription = diseaseDescription;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getDiseaseName() {
        return diseaseName;
    }
    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }
    public List<Long> getSymptomID() {
        return symptomID;
    }
    public void setSymptomID(List<Long> symptomID) {
        this.symptomID = symptomID;
    }
    public List<Long> getMedicineID() {
        return medicineID;
    }
    public void setMedicineID(List<Long> medicineID) {
        this.medicineID = medicineID;
    }
   
    
}
