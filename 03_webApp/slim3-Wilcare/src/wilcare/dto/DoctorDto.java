package wilcare.dto;

import java.util.List;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/06/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DoctorDto extends BaseDto {
    
   private Long  id; 
   private List<Long> patientID;   
   private double price;
   
   private String username;
   private String password;
   private String firstName;
   private String middleName;
   private String lastName;
   private String type;
   public String getType() {
    return type;
}
public void setType(String type) {
    this.type = type;
}
private boolean status;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
  
    public List<Long> getPatientID() {
        return patientID;
    }
    public void setPatientID(List<Long> patientID) {
        this.patientID = patientID;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
       
   

}
