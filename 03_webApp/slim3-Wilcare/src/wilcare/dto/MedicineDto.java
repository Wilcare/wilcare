package wilcare.dto;

public class MedicineDto extends BaseDto {

    private Long id;
    private String medicineName;
    private String medicineDescription; 
    private double price;
    
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getMedicineName() {
        return medicineName;
    }
    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }
    public String getMedicineDescription() {
        return medicineDescription;
    }
    public void setMedicineDescription(String medicineDescription) {
        this.medicineDescription = medicineDescription;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
    
    
}
