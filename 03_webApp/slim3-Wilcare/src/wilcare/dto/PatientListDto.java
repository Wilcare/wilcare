package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class PatientListDto {
  private List<PatientDto> patientListDto;
    
    public PatientListDto() {
        patientListDto = new LinkedList<PatientDto>();
    }
    
    public void addPatientDto(PatientDto patientDto) {
        patientListDto.add(patientDto);
    }
    
    public List<PatientDto> getPatientList() {
        return patientListDto;
    }
    
    public void setPatientList(List<PatientDto> patientDto) {
        patientListDto = patientDto;
    }
}
