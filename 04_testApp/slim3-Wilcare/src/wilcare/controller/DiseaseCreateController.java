package wilcare.controller;

import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.DiseaseDto;
import wilcare.service.DiseaseService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DiseaseCreateController extends Controller {
    DiseaseService objectService = new DiseaseService();
    @Override
    public Navigation run() throws Exception {
        DiseaseDto objectDto = new DiseaseDto();
        JSONObject jsonObject = null;
        
        System.out.println("DiseaseCreateController.run " + "start");
        try{
            Validators validator = new Validators(this.request); 
            validator.add("diseaseName", validator.required());
            validator.add("symptomID", validator.required());
            validator.add("medicineID", validator.required());
           
            if(validator.validate()){
            jsonObject = new JSONObject(this.request.getReader().readLine());       
             String diseaseName = jsonObject.getString("diseaseName");
             JSONArray  symptomID = jsonObject.getJSONArray("symptomID");
             JSONArray  medicineID = jsonObject.getJSONArray("medicineID");
             List<Long> symptomIDList = new ArrayList<Long>();
             List<Long> medicineIDList = new ArrayList<Long>();     
             
             for (int i = 0; i < symptomID.length(); i++){
                 symptomIDList.add(symptomID.getLong(i));
             }
             for (int i = 0; i < medicineID.length(); i++){
                 medicineIDList.add(medicineID.getLong(i));
             }             
             
            // setting the values of DTO from the request
             objectDto.setDiseaseName(diseaseName);
             objectDto.setMedicineID(medicineIDList);
             objectDto.setSymptomID(symptomIDList);
             
            this.objectService.insertDisease(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
            
        }catch(Exception e){
            System.out.println("DiseaseCreateController.run.exception " + e.toString());
         //   objectDto.addError(error);
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }    
        
        jsonObject.put("errorList", objectDto.getErrorList());
        //response.setContentType();
        response.getWriter().write(jsonObject.toString());
        System.out.println("DiseaseCreateController.run " + "end");
        return null;
    }
}

//Slim3
/*
 // Getting all the information sent from the request.
 String diseaseName = request.getParameter("diseaseName");
 String symptomID = request.getParameter("symptomID");
 String medicineID = request.getParameter("medicineID");             

 // splitting the sideDishIds into array.            
 String[] symptomIDArray = symptomID.split(",");
 List<Long> symptomIDList = new ArrayList<Long>();
 String[] medicineIDArray = medicineID.split(",");
 List<Long> medicineIDList = new ArrayList<Long>();            
                         
 converting the ids of the sideDishes from string into Long
 then inserted into the 'sideDishIdList' array

 for (int i = 0; i < symptomIDArray.length; i++){
     symptomIDList.add(Long.parseLong(symptomIDArray[i]));
 }
 for (int i = 0; i < medicineIDArray.length; i++){
     medicineIDList.add(Long.parseLong(medicineIDArray[i]));
 }
 */
