package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.MedicineDto;
import wilcare.service.MedicineService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class MedicineDeleteController extends Controller {
    MedicineService objectService = new MedicineService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        MedicineDto objectDto = new MedicineDto();
        System.out.println("MedicineDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());
                       
             Long id = jsonObject.getLong("id");           
             objectDto.setId(id);    
        }catch(Exception e){
            System.out.println("MedicineDeleteController.run.exception " + e.toString());
        }
        System.out.println("MedicineDeleteController.run " + "end");
        return null;
    }
}
//Slim3
// Getting all the information sent from the request.
//String id = request.getParameter("id");                   
//objectDto.setId(Long.parseLong(id)); 
