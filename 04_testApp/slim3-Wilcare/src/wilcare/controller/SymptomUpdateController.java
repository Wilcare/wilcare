package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.SymptomDto;
import wilcare.service.SymptomService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class SymptomUpdateController extends Controller {
    SymptomService objectService = new SymptomService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        SymptomDto objectDto = new  SymptomDto();
        System.out.println("SymptomUpdateController.run " + "start");
        try{
            Validators validator = new Validators(this.request); 
            validator.add("symptomName", validator.required());
            validator.add("symptomDescription", validator.required());
          
            if(validator.validate()){
            // Getting all the information sent from the request.
            String symptomName = jsonObject.getString("symptomName");
            String symptomDescription = jsonObject.getString("symptomDescription");
     
            // setting the values of DTO from the request
             objectDto.setSymptomDescription(symptomDescription);
             objectDto.setSymptomName(symptomName);    
             
//            objectDto.setId(Long.parseLong("470"));
//            objectDto.setSymptomDescription("test");
//            objectDto.setSymptomName("test11122");
             
            this.objectService.updateSymptom(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("SymptomUpdateController.run.exception " + e.toString());
            //   objectDto.addError(error);
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
            jsonObject.put("errorList", objectDto.getErrorList());
            //response.setContentType();
            response.getWriter().write(jsonObject.toString());
        }
        System.out.println("SymptomUpdateController.run " + "end");
        return null;
    }
}
//Slim3
/*
String symptomName = request.getParameter("symptomName");
String symptomDescription =  request.getParameter("symptomDescription");
*/
