package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.MedicineDto;
import wilcare.service.MedicineService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class MedicineUpdateController extends Controller {
    MedicineService objectService = new MedicineService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        MedicineDto objectDto = new MedicineDto();
        System.out.println("MedicinceUpdateController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());
           
            String medicineName = jsonObject.getString("medicineName");
            String medicineDescription = jsonObject.getString("medicineDescription");
            Double price = jsonObject.getDouble("price");
            
            objectDto.setMedicineName(medicineName);
            objectDto.setMedicineDescription(medicineDescription);
            objectDto.setPrice(price);
            
            this.objectService.updateMedicine(objectDto);
            
        }catch(Exception e){
            System.out.println("MedicinceUpdateController.run.exception " + e.toString());
        }
        System.out.println("MedicinceUpdateController.run " + "end");
        return null;
    }
}
//Slim3
/*
// Getting all the information sent from the request.
 String medicineName = request.getParameter("medicineName");
 String medicineDescription =  request.getParameter("medicineDescription");
 String price = request.getParameter("price");

// setting the values of DTO from the request
 objectDto.setMedicineName(medicineName);
 objectDto.setMedicineDescription(medicineDescription);
 objectDto.setPrice(Double.parseDouble(price));
 */