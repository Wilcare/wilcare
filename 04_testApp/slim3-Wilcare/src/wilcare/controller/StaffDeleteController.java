package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.dto.StaffDto;
import wilcare.service.StaffService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class StaffDeleteController extends Controller {
    StaffService objectService = new StaffService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        StaffDto objectDto = new  StaffDto();        
        System.out.println("StaffDeleteController.run " + "start");
        try{
           jsonObject = new JSONObject(new RequestMap(this.request));           
           Long id = jsonObject.getLong("id");
           System.out.println(id);
           objectDto.setId(id); 
           this.objectService.deleteStaff(objectDto);
            
        }catch(Exception e){
            System.out.println("StaffDeleteController.run.exception " + e.toString());
        }
        System.out.println("StaffDeleteController.run " + "end");
        return null;
    }
}
//Slim3
//String id = request.getParameter("id");                   
//objectDto.setId(Long.parseLong(id)); 