package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class IndexController extends Controller {
    /**
     * Controller that will accessed when 'localhost:8888' is entered.
     * @author wilcare
     * @version 0.01
     * Version History
     * dd/mm/yy
     * [02/09/2016] 0.01 � Wilson Justine G. Sison  � Initial codes.
     */
    
    
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("IndexController.run " + "start, end");
        return forward("/html/Index.html");
      
    }
}
