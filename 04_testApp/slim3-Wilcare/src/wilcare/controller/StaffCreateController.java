package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.dto.StaffDto;
import wilcare.service.StaffService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class StaffCreateController extends Controller {
    StaffService objectService = new StaffService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        StaffDto objectDto = new  StaffDto();
        
        System.out.println("StaffCreateController.run " + "start");
        try{
            Validators validator = new Validators(this.request); 
            validator.add("username", validator.required());
            validator.add("password", validator.required());
            validator.add("firstName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("middleName", validator.required());
            validator.add("staffType", validator.required());
            validator.add("staffStatus", validator.required());
            
            if(validator.validate()){
            jsonObject = new JSONObject(new RequestMap(this.request));
            
            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");
            String firstName =jsonObject.getString("firstName");
            String lastName = jsonObject.getString("lastName");
            String middleName = jsonObject.getString("middleName");
            String staffType = jsonObject.getString("staffType");
            Boolean staffStatus = jsonObject.getBoolean("staffStatus");
            
            objectDto.setFirstName(firstName);
            objectDto.setMiddleName(middleName);
            objectDto.setLastName(lastName);
            objectDto.setUsername(username);
            objectDto.setPassword(password);
            objectDto.setStaffStatus(staffStatus);
            objectDto.setStaffType(staffType);
            
            if(objectDto.getStaffType().equals("admin")){
                
                
            }else{
                
                
            }
            
            this.objectService.insertStaff(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("StaffCreateController.run.exception " + e.toString());
            //   objectDto.addError(error);
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        //response.setContentType();
        response.getWriter().write(jsonObject.toString());
        System.out.println("StaffCreateController.run " + "end");
        return null;
    }
}
/*Slim3
// Getting all the information sent from the request.
  String username = request.getParameter("username");
  String password = request.getParameter("password");
  String firstName = request.getParameter("firstName");
  String lastName = request.getParameter("lastName");
  String middleName = request.getParameter("middleName");
  String staffType = request.getParameter("staffType");
  String staffStatus = request.getParameter("staffStatus");

// setting the values of DTO from the request
 objectDto.setFirstName(firstName);
 objectDto.setMiddleName(middleName);
 objectDto.setLastName(lastName);
 objectDto.setUsername(username);
 objectDto.setPassword(password);
 objectDto.setStaffStatus(Boolean.parseBoolean(staffStatus));
 objectDto.setStaffType(staffType);
 
 if(objectDto.getStaffType().equals("admin")){
     
     
 }else{
     
     
 }
*/