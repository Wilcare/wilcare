package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.StaffListDto;
import wilcare.service.StaffService;

public class StaffListController extends Controller {
    StaffService objectService = new StaffService();
    JSONObject jsonObject = new JSONObject();
    
    @Override
    public Navigation run() throws Exception {
        StaffListDto ListDto = objectService.getStaffList();
        jsonObject.put("staffList",ListDto.getstaffListS());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
