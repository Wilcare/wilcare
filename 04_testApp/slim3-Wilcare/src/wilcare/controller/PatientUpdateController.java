package wilcare.controller;


import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.PatientDto;
import wilcare.service.PatientService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class PatientUpdateController extends Controller {
    PatientService objectService = new PatientService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        PatientDto objectDto = new PatientDto();
      //  DateFormat df = new SimpleDateFormat("mm/dd/yyyy"); 
        System.out.println("PatientUpdateController.run " + "start");
        try{            
            
            Validators validator = new Validators(this.request); 
     
            validator.add("firstName", validator.required());
            validator.add("middleName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("birthday", validator.required());
            validator.add("sex", validator.required());
            validator.add("admissionDate", validator.required());
            validator.add("dischargeDate", validator.required());
            validator.add("status", validator.required());
            validator.add("totalBill", validator.required());
            jsonObject = new JSONObject(this.request.getReader().readLine());
            System.out.println(jsonObject.getString("firstName"));
           
            if(validator.validate()){
  
                     
            
            JSONArray  symptomID = jsonObject.getJSONArray("symptomID");
            JSONArray  medicineID = jsonObject.getJSONArray("medicineID");
            JSONArray  doctorID = jsonObject.getJSONArray("doctorID");
            JSONArray  diseaseID = jsonObject.getJSONArray("doctorID");
            
            String firstName = jsonObject.getString("firstName");
            String middleName = jsonObject.getString("middleName");
            String lastName = jsonObject.getString("lastName");
            String birthday = jsonObject.getString("birthday");
            String sex = jsonObject.getString("sex");
            String admissionDate = jsonObject.getString("admissionDate");
            String dischargeDate = jsonObject.getString("dischargeDate");
            Boolean status = jsonObject.getBoolean("status");
            Double totalBill = jsonObject.getDouble("totalBill");
            
            List<Long> symptomIDList = new ArrayList<Long>();
            List<Long> medicineIDList = new ArrayList<Long>();     
            List<Long> doctorIDList = new ArrayList<Long>(); 
            List<Long> diseaseIDList = new ArrayList<Long>(); 
            
            for (int i = 0; i < symptomID.length(); i++){
                symptomIDList.add(symptomID.getLong(i));
            }
            for (int i = 0; i < medicineID.length(); i++){
                medicineIDList.add(medicineID.getLong(i));
            }             
            for (int i = 0; i < doctorID.length(); i++){
                doctorIDList.add(doctorID.getLong(i));
            }
            for (int i = 0; i < diseaseID.length(); i++){
                diseaseIDList.add(diseaseID.getLong(i));
            }       
            
            objectDto.setDiseaseID(diseaseIDList);
            objectDto.setDoctorID(doctorIDList);
            objectDto.setSymptomID(symptomIDList);
            objectDto.setMedicineID(diseaseIDList);
            objectDto.setFirstName(firstName);
            objectDto.setMiddleName(middleName);
            objectDto.setLastName(lastName);
            objectDto.setBirthday(birthday);
            objectDto.setSex(sex);
            objectDto.setAdmissionDate(admissionDate);
            objectDto.setDischargeDate(dischargeDate);
            objectDto.setStatus(status);
            objectDto.setTotalBill(totalBill);
            
         
            this.objectService.updatePatient(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("PatientUpdateController.run.exception " + e.toString());
            //   objectDto.addError(error);
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        //response.setContentType();
        response.getWriter().write(jsonObject.toString());
        System.out.println("PatientUpdateController.run " + "end");
     
        return null;
    }
}
//Slim3
/*
// Getting all the information sent from the request.
 String diseaseID = request.getParameter("diseaseID");
 String symptomID = request.getParameter("symptomID");
 String doctorID = request.getParameter("doctorID");
 String firstName = request.getParameter("firstName");
 String middleName = request.getParameter("middleName");
 String lastName = request.getParameter("lastName");
 String birthday = request.getParameter("birthday");
 String sex = request.getParameter("sex");
 String admissionDate = request.getParameter("admissionDate");
 String dischargeDate = request.getParameter("dischargeDate");
 String status = request.getParameter("status");
 String totalBill = request.getParameter("totalBill");
 
 // splitting the sideDishIds into array.
 String[] diseaseIDArray = diseaseID.split(",");
 List<Long> diseaseIDList = new ArrayList<Long>();
 String[] symptomIDArray = symptomID.split(",");
 List<Long> symptomIDList = new ArrayList<Long>();
 String[] doctorIDArray = doctorID.split(",");
 List<Long>doctorIDList = new ArrayList<Long>();
 
 
 converting the ids of the sideDishes from string into Long
 then inserted into the 'sideDishIdList' array       
 for (int i = 0; i < diseaseIDArray.length; i++){
     diseaseIDList.add(Long.parseLong(diseaseIDArray[i]));
 }
 for (int i = 0; i < symptomIDArray.length; i++){
     symptomIDList.add(Long.parseLong(symptomIDArray[i]));
 }
 for (int i = 0; i < doctorIDArray.length; i++){
     doctorIDList.add(Long.parseLong(doctorIDArray[i]));
 }
 
//not sure if this will work
Date birthdDate = null;
Date adDate = null;
Date disDate = null;
try {
    birthdDate = (Date) df.parse(birthday);
    adDate = (Date) df.parse(admissionDate);
    disDate = (Date) df.parse(dischargeDate);
} catch (Exception e) {
    e.printStackTrace();
}
  
 
// setting the values of DTO from the request
 objectDto.setDiseaseID(diseaseIDList);
 objectDto.setDoctorID(doctorIDList);
 objectDto.setSymptomID(symptomIDList);
 objectDto.setFirstName(firstName);
 objectDto.setMiddleName(middleName);
 objectDto.setLastName(lastName);
 objectDto.setBirthday(birthdDate);
 objectDto.setSex(sex);
 objectDto.setAdmissionDate(adDate);
 objectDto.setDischargeDate(disDate);
 objectDto.setStatus(Boolean.parseBoolean(status));
 objectDto.setTotalBill(Double.parseDouble(totalBill));
 */