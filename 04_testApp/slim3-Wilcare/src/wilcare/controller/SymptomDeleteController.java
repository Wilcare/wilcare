package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.SymptomDto;
import wilcare.service.SymptomService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class SymptomDeleteController extends Controller {
    SymptomService objectService = new SymptomService();
    JSONObject jsonObject;
    @Override
    public Navigation run() throws Exception {
        SymptomDto objectDto = new  SymptomDto();
        System.out.println("SymptomDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());
          
            Long id = jsonObject.getLong("id");           
            objectDto.setId(id); 
            this.objectService.deleteSymptom(objectDto);
            
        }catch(Exception e){
            System.out.println("SymptomDeleteController.run.exception " + e.toString());
        }
//        objectDto.setId(Long.parseLong("472"));
//        objectDto.setSymptomName("test111");
//        this.objectService.deleteSymptom(objectDto);
        System.out.println("SymptomDeleteController.run " + "end");
        return null;
    }
}
//Slim3
//String id = request.getParameter("id");                   
//objectDto.setId(Long.parseLong(id)); 