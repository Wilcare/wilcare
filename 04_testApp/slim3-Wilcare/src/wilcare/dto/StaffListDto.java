package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class StaffListDto {
 private List<StaffDto> staffListDto;
    
    public StaffListDto() {
        staffListDto = new LinkedList<StaffDto>();
    }
    
    public void addStaffDto(StaffDto staffDto) {
        staffListDto.add(staffDto);
    }
    
    public List<StaffDto> getstaffListS() {
        return staffListDto;
    }
    
    public void setSymptomList(List<StaffDto> staffDto) {
        staffListDto = staffDto;
    }
}
