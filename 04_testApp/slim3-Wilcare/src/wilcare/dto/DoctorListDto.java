package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class DoctorListDto {
  private List<DoctorDto> doctorListDto;
    
    public DoctorListDto() {
        doctorListDto = new LinkedList<DoctorDto>();
    }
    
    public void addDoctorDto(DoctorDto doctorDto) {
        doctorListDto.add(doctorDto);
    }
    
    public List<DoctorDto> getDoctorList() {
        return doctorListDto;
    }
    
    public void setDoctorList(List<DoctorDto> doctorDto) {
        doctorListDto = doctorDto;
    }
}
