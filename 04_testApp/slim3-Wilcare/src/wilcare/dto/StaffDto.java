package wilcare.dto;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class StaffDto extends BaseDto {
    
  
    private  Long id;
    private  String username;
    private  String password;
    private  String firstName;
    private  String lastName;
    private  String middleName;
    private  String staffType;
    private  boolean staffStatus;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getStaffType() {
        return staffType;
    }
    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }
    public boolean getStaffStatus() {
        return staffStatus;
    }
    public void setStaffStatus(boolean staffStatus) {
        this.staffStatus = staffStatus;
    }


}
