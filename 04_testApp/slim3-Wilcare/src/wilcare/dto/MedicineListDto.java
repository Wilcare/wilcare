package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class MedicineListDto {
  private List<MedicineDto> medicineListDto;
    
    public MedicineListDto() {
        medicineListDto = new LinkedList<MedicineDto>();
    }
    
    public void addMedicineDto(MedicineDto medicineDto) {
        medicineListDto.add(medicineDto);
    }
    
    public List<MedicineDto> getMedicineList() {
        return medicineListDto;
    }
    
    public void setMedicineList(List<MedicineDto> medicineDto) {
        medicineListDto = medicineDto;
    }
}
