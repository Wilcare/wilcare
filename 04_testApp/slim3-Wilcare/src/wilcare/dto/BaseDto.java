package wilcare.dto;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class BaseDto {

    private List<String> errorList = new ArrayList<String>();
    
    public void addError(String error) {
        this.getErrorList().add(error);
    }
    public void clearErrors() {
        this.getErrorList().clear();
    }
   
    public List<String> getErrorList() {
        return errorList;
    }
    public void setErrorList(List<String> errorList) {
        this.errorList = errorList;
    }
    
    
}
