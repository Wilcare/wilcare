package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class SymptomListDto extends BaseDto{
    private List<SymptomDto> symptomListDto;
    
    public SymptomListDto() {
        symptomListDto = new LinkedList<SymptomDto>();
    }
    
    public void addSymptomDto(SymptomDto symptomDto) {
        symptomListDto.add(symptomDto);
    }
    
    public List<SymptomDto> getSymptomList() {
        return symptomListDto;
    }
    
    public void setSymptomList(List<SymptomDto> symptomDto) {
        symptomListDto = symptomDto;
    }
}
