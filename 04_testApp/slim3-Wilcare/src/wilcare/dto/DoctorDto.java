package wilcare.dto;

import java.util.List;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/06/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class DoctorDto {
    
   private Long  id;
   private Long  staffID;
   private List<Long> patientID;
   private double price;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public Long getStaffID() {
        return staffID;
    }
    public void setStaffID(Long staffID) {
        this.staffID = staffID;
    }
    public List<Long> getPatientID() {
        return patientID;
    }
    public void setPatientID(List<Long> patientID) {
        this.patientID = patientID;
    }
    public double getPrice() {
        return price;
    }
    public void setPrice(double price) {
        this.price = price;
    }
       
   

}
