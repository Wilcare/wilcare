package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import wilcare.meta.StaffModelMeta;
import wilcare.model.StaffModel;

/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class StaffDao extends DaoBase<StaffModel>{
    
    public StaffModel getStaffByName(StaffModel inputObject){
        StaffModel objectModel = new StaffModel();
        
        System.out.println("StaffDao.getStaffByName " + "start");
        
        StaffModelMeta meta = StaffModelMeta.get();
        objectModel = Datastore.query(meta)
                .filter(meta.lastName.equal(inputObject.getLastName()),
                        meta.firstName.equal(inputObject.getFirstName()),
                        meta.middleName.equal(inputObject.getMiddleName())
                       ).asSingle();
        
        System.out.println("StaffDao.getStaffByName " + "end");
        return objectModel;
        
    }
    
    public StaffModel getStaffByNameId(StaffModel inputObject){
        StaffModel objectModel = new StaffModel();
        
        System.out.println("StaffDao.getStaffByNameId " + "start");
        
        StaffModelMeta meta = StaffModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.lastName.equal(inputObject.getLastName())
                               ,meta.id.equal(inputObject.getId())
                                ).asSingle();
        
        System.out.println("StaffDao.getStaffByNameId " + "end");
        return objectModel;
        
    }
       
    public StaffModel getStaffById(StaffModel inputObject){
        
        StaffModel objectModel = new StaffModel();
        
        System.out.println("StaffDao.getStaffById " + "start");
        
        StaffModelMeta meta = StaffModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId())).asSingle();
                             
        
        System.out.println("StaffDao.getStaffById " + "end");
        return objectModel;
        
    }
       
    
    public void insertStaff(StaffModel inputObject) {
        System.out.println("StaffDao.insertPatient " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
        Key parentKey = KeyFactory.createKey("StaffModel", inputObject.getLastName());
        Key key = Datastore.allocateId(parentKey, "StaffModel");
        
        //passing the key in Model
        inputObject.setKey(key);
        inputObject.setId(key.getId());     
        Datastore.put(inputObject);
        transaction.commit();
        
        System.out.println("StaffDao.insertStaff " + "end");
    }   
    
    public void updateStaff(StaffModel inputOjbect) {
        System.out.println("StaffDao.updateStaff " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("StaffDao.updateStaff " + "end");
    }   
    
    public void deleteStaff(StaffModel inputOjbect) {
        System.out.println("StaffDao.deleteStaff " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        
        System.out.println("StaffDao.deleteStaff " + "end");
    }   
    
    public List<StaffModel> getStaff() {
        
        return Datastore.query(StaffModel.class).asList();
    }

}
