package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


import wilcare.meta.DiseaseModelMeta;

import wilcare.model.DiseaseModel;

/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */

public class DiseaseDao extends DaoBase<DiseaseModel>{
    
   
 public DiseaseModel getDiseaseByName(DiseaseModel inputObject){
     DiseaseModel objectModel = new DiseaseModel();
     
     System.out.println("DiseaseDao.getDieaseByName " + "start");
     
     DiseaseModelMeta meta = DiseaseModelMeta.get();
     objectModel = Datastore.query(meta).filter(meta.diseaseName.equal(inputObject.getDiseaseName())).asSingle();
     
     System.out.println("DiseaseDao.getDieaseByName " + "end");
     return objectModel;
     
 }
 
 public DiseaseModel getDiseaseByNameId(DiseaseModel inputObject){
     DiseaseModel objectModel = new DiseaseModel();
     
     System.out.println("DiseaseDao.getDiseaseByNameId " + "start");
     
     DiseaseModelMeta meta = DiseaseModelMeta.get();
     objectModel = Datastore.query(meta)
                     .filter(meta.diseaseName.equal(inputObject.getDiseaseName())
                            ,meta.id.equal(inputObject.getId())
                             ).asSingle();
     
     System.out.println("DiseaseDao.getDiseaseByNameId " + "end");
     return objectModel;
     
 }
    
 public DiseaseModel getDiseaseById(DiseaseModel inputObject){
     DiseaseModel objectModel = new DiseaseModel();
     
     System.out.println("DiseaseDao.getDiseaseById " + "start");
     
     DiseaseModelMeta meta = DiseaseModelMeta.get();
     objectModel = Datastore.query(meta)
                     .filter(meta.id.equal(inputObject.getId())).asSingle();
                          
     
     System.out.println("DiseaseDao.getDiseaseById " + "end");
     return objectModel;
     
 }
    
 
 public void insertDisease(DiseaseModel inputOjbect) {
     System.out.println("DiseaseDao.insertDisease " + "start");
   
     Transaction transaction = Datastore.beginTransaction();
     //creates key
     Key parentKey = KeyFactory.createKey("DiseaseModel", inputOjbect.getDiseaseName());
     Key key = Datastore.allocateId(parentKey, "DiseaseModel");
     
     //passing the key in DiseaseModel
     inputOjbect.setKey(key);
     inputOjbect.setId(key.getId());     
     Datastore.put(inputOjbect);
     transaction.commit();
     
     System.out.println("DiseaseDao.insertDisease " + "end");
 }   
   
 public void updateDisease(DiseaseModel inputOjbect) {
     System.out.println("DiseaseDao.updateDisease " + "start");
   
     Transaction transaction = Datastore.beginTransaction();     
     Datastore.put(inputOjbect);     
     transaction.commit();
     
     System.out.println("DiseaseDao.updateDisease " + "end");
 }   
 
 public void deleteDisease(DiseaseModel inputOjbect) {
     System.out.println("DiseaseDao.deleteDisease " + "start");
     
     Transaction transaction = Datastore.beginTransaction();
     Datastore.delete(inputOjbect.getKey());     
     transaction.commit();
     System.out.println("DiseaseDao.deleteDisease " + "end");
 }   
 public List<DiseaseModel> getDisease() {        
     return Datastore.query(DiseaseModel.class).asList();
 }
}
