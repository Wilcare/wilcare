package wilcare.service;

import java.util.List;

import wilcare.dao.SymptomDao;
import wilcare.dto.SymptomDto;
import wilcare.dto.SymptomListDto;
import wilcare.model.SymptomModel;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/04/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class SymptomService {
    SymptomDao objectDao =  new SymptomDao();
    
    public SymptomDto insertSymptom(SymptomDto inputDto) {
        SymptomModel objectModel = new SymptomModel(); 
        System.out.println("SymptomService.insertSymptom " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setSymptomName(inputDto.getSymptomName());
        objectModel.setSymptomDescription(inputDto.getSymptomDescription());
        
        try{
            SymptomModel resultModel = this.objectDao.getSymptomByName(objectModel);
            if(resultModel!=null){
                System.out.println(" Symptom Name already exist");                
            }else{
                try{
                    this.objectDao.insertSymptom(objectModel);
                    System.out.println("-------Adding Succesful-------");
                }catch(Exception e){
                    System.out.println("Exception in adding item: " + e.toString());
                }                
            }
            
        }catch(Exception e){
            System.out.println("Exception in adding item: " + e.toString());            
        }
        System.out.println("SymptomService.insertSymptom " + "end");
        return inputDto;
    }
    
    public SymptomDto updateSymptom(SymptomDto inputDto) {
        SymptomModel objectModel = new SymptomModel(); 
        System.out.println("SymptomService.updateSymptom " + "start");
        try{
            objectModel.setId(inputDto.getId());
            objectModel.setSymptomName(inputDto.getSymptomName());
            objectModel.setSymptomDescription(inputDto.getSymptomDescription());
            
            SymptomModel objectModel2 = this.objectDao.getSymptomById(objectModel);
            if(objectModel2!=null){
                objectModel.setKey(objectModel2.getKey());
                this.objectDao.updateSymptom(objectModel);
                System.out.println("Updated Symptom Information");
            } else {               
                System.out.println("There is no item with the same id.");
            }                        
        }catch(Exception e){
            System.out.println("Exception in updating item: " + e.toString());            
        }
        System.out.println("SymptomService.updateSymptom " + "end");
        return inputDto;
    }
    
    public SymptomDto deleteSymptom(SymptomDto inputDto) {
        SymptomModel objectModel = new SymptomModel(); 
        System.out.println("SymptomService.deleteSymptom " + "start");
        try{
            objectModel.setId(inputDto.getId());
            objectModel.setSymptomName(inputDto.getSymptomName());
            objectModel.setSymptomDescription(inputDto.getSymptomDescription());
            
            SymptomModel resultModel = this.objectDao.getSymptomByNameId(objectModel);
            if(resultModel != null){
                this.objectDao.deleteSymptom(resultModel);         
                System.out.println("Deleted Symptom" + resultModel.getKey());               
            }else {
               System.out.println("No Symptom was Found!");
              }       
            
            
        }catch(Exception e){
            System.out.println("Exception in adding item: " + e.toString());            
        }
        System.out.println("SymptomService.deleteSymptom " + "end");
        return inputDto;
    }
    
    public SymptomModel storeDtoToModel(SymptomDto inputDto) {
        System.out.println("SymptomService.storeDtoToModel " + "start");
        
        SymptomModel objectModel = new SymptomModel(); 
        objectModel.setId(inputDto.getId());
        objectModel.setSymptomName(inputDto.getSymptomName());
        objectModel.setSymptomDescription(inputDto.getSymptomDescription());
        
        
        System.out.println("SymptomService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public SymptomListDto  getSymptomList() {
        SymptomListDto symptomListDto = new SymptomListDto();
        List<SymptomModel> symptomList = objectDao.getSymptoms();
        
        for(SymptomModel s : symptomList) {
            SymptomDto symptomDto = new SymptomDto();
            symptomDto.setId(s.getId());
            symptomDto.setSymptomDescription(s.getSymptomDescription());
            symptomDto.setSymptomName(s.getSymptomName());
            
            symptomListDto.addSymptomDto(symptomDto);
        }
        
        return symptomListDto; 
    }
}
