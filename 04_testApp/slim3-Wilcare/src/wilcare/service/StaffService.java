package wilcare.service;

import java.util.List;

import wilcare.dao.StaffDao;
import wilcare.dto.StaffDto;
import wilcare.dto.StaffListDto;
import wilcare.model.StaffModel;

/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/04/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class StaffService {
    StaffDao objectDao =  new StaffDao();
    
    public StaffDto insertStaff(StaffDto inputDto) {
        StaffModel objectModel = new StaffModel(); 
        System.out.println("StaffService.insertStaff " + "start");
        
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword());
        objectModel.setStaffStatus(inputDto.getStaffStatus());
        objectModel.setStaffType(inputDto.getStaffType());
        
        try{
            StaffModel resultModel = this.objectDao.getStaffByName(objectModel);
            if(resultModel!=null){
                System.out.println(" Staff Name already exist");                
            }else{
                try{
                    this.objectDao.insertStaff(objectModel);
                    System.out.println("-------Adding Succesful-------");
                }catch(Exception e){
                    System.out.println("Exception in adding item: " + e.toString());
                }                
            }
            
        }catch(Exception e){
            System.out.println("Exception in Adding item: " + e.toString());
        }
        System.out.println("StaffService.insertStaff " + "end");
        return inputDto;
    }
    
    public StaffDto updateStaff(StaffDto inputDto) {
        StaffModel objectModel = new StaffModel(); 
        System.out.println("StaffService.updateStaff " + "start");
        
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
        objectModel.setUsername(inputDto.getUsername());
        objectModel.setPassword(inputDto.getPassword());
        objectModel.setStaffStatus(inputDto.getStaffStatus());
        objectModel.setStaffType(inputDto.getStaffType());
        try{
            StaffModel objectModel2 = this.objectDao.getStaffById(objectModel);
            if(objectModel2!=null){
                objectModel.setKey(objectModel2.getKey());
                this.objectDao.updateStaff(objectModel);
                System.out.println("Updated Staff Information");
            } else {               
                System.out.println("There is no item with the same id.");
            }
        }catch(Exception e){
            System.out.println("Exception in Updating item: " + e.toString());
        }
        System.out.println("StaffService.updateStaff " + "end");
        return inputDto;
    }
    
    public StaffDto deleteStaff(StaffDto inputDto) {
        StaffModel objectModel = new StaffModel(); 
        System.out.println("StaffService.deleteStaff " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setFirstName(inputDto.getFirstName());
        objectModel.setMiddleName(inputDto.getMiddleName());
        objectModel.setLastName(inputDto.getLastName());
         
        try{
            StaffModel resultModel = this.objectDao.getStaffById(objectModel);
            if(resultModel != null){
                this.objectDao.deleteStaff(resultModel);               
                System.out.println("Deleted Staff");               
            }else {
               System.out.println("No Staff was Found!");
              }       
            
        }catch(Exception e){
            System.out.println("Exception in Deleting item: " + e.toString());
        }
        System.out.println("StaffService.deleteStaff " + "end");
        return inputDto;
    }
    
    public StaffModel storeDtoToModel(StaffDto inputObject) {
        System.out.println("StaffService.storeDtoToModel " + "start");
        
        StaffModel objectModel = new StaffModel(); 
        objectModel.setId(inputObject.getId());
        objectModel.setFirstName(inputObject.getFirstName());
        objectModel.setMiddleName(inputObject.getMiddleName());
        objectModel.setLastName(inputObject.getLastName());
        objectModel.setUsername(inputObject.getUsername());
        objectModel.setPassword(inputObject.getPassword());
        objectModel.setStaffStatus(inputObject.getStaffStatus());
        objectModel.setStaffType(inputObject.getStaffType());
        
        System.out.println("StaffService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public StaffListDto  getStaffList() {
        StaffListDto listDto = new StaffListDto();
        List<StaffModel> modelList = objectDao.getStaff();
        
        for(StaffModel s : modelList) {
            StaffDto objectDto = new StaffDto();
            objectDto.setId(s.getId());
            objectDto.setFirstName(s.getFirstName());
            objectDto.setMiddleName(s.getMiddleName());
            objectDto.setLastName(s.getLastName());
            objectDto.setUsername(s.getUsername());
            objectDto.setPassword(s.getPassword());
            objectDto.setStaffStatus(s.isStaffStatus());
            objectDto.setStaffType(s.getStaffType());            
            listDto.addStaffDto(objectDto);
        }
        
        return listDto; 
    }
}
