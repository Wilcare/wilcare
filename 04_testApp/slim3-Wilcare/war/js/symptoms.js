app.controller('symptomsCtrl', function($scope, $http, $httpParamSerializer) {
    
    $scope.addSymptoms = "Add Symptom";

    $scope.symptom = {
    	id: "",
    	symptomName: "",
    	symptomDescription: ""
    }

    $scope.editSymptom = {
    	id: "",
    	symptomName: "symptomName",
    	symptomDescription: "symptomDescription"
    }

    $scope.symptoms = [];

    $scope.displayToModal = function(symptom) {
    	$scope.editSymptom.id = symptom.id;
    	$scope.editSymptom.symptomName = symptom.symptomName;
    	$scope.editSymptom.symptomDescription = symptom.symptomDescription;
    }

    $scope.symptom.submit = function() {
    	if ($scope.addSymptoms === "Add Symptom") {
    		$scope.addSymptom();
    		$scope.clear();
    	}
    	else {
    		$scope.addSymptoms = "Add Symptom";
    		$scope.updateSymptom();
    	}
    }

    $scope.getStaffs = function() {
    	const url = '/StaffList';
    	$http.get(url)
    		.then(
    				response => {
    					$scope.staffs = response.data.staffList;
    					console.log('success' + $scope.staffList);
    				},
    				error => {
    					console.log('something went wrong in retrieving data');
    				}
    		)
    }
});