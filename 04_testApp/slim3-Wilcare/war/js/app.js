var app = angular.module('wilcareApp', ['ngRoute'])

app.config(['$routeProvider', function($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: '/html/MainScreen/Home.html'
        })
        .when('/Login', {
            templateUrl: '/html/MainScreen/Login.html',
            controller: 'loginCtrl'
        })
        .when('/Doctor', {
            templateUrl: '/html/DoctorScreen/Doctor.html',
            controller: 'doctorCtrl'
        })
        .when('/Admin', {
            templateUrl: '/html/AdminScreen/Admin.html',
            controller: 'adminCtrl'
        })
        .when('/List', {
            templateUrl: '/html/DoctorScreen/List.html',
            controller: 'listCtrl'
        })
        .when('/ListAdmin', {
            templateUrl: '/html/AdminScreen/List.html',
            controller: 'listAdminCtrl'
        })
}]);
