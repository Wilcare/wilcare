app.controller('doctorCtrl', function($scope, $http, $httpParamSerializer) {
    $scope.patient = {
    		id: "",
    		firstName: "firstName", 
    		middleName: "middleName",
    		lastName: "lastName",
    		birthday: "birthday",
    		sex: "sex",
    		admissionDate: "admissionDate",
    		dischargeDate: "dischargeDate",
    		status: false,
    		totalBill: 500    		


    }
    
    $scope.editPatient = {
    		id: "",
    		diseaseID: [],
    		symptomID: [],
    		doctorID: [],
    		medicineID:[],   	
    		firstName: "firstName", 
    		middleName: "middleName",
    		lastName: "lastName",
    		birthday: "birthday",
    		sex: "sex",
    		admissionDate: "admissionDate",
    		dischargeDate: "dischargeDate",
    		status: false,
    		totalBill: 500    		
    }
    
	$scope.patients = [];
	
    $scope.mode = "Add Patient"
    
    $scope.displayToModal = function(patient) {  	
    	$scope.editPatient.id = patient.id;
    	$scope.editPatient.diseaseID = patient.diseaseID;
    	$scope.editPatient.symptomID = patient.symptomID;
    	$scope.editPatient.doctorID = patient.doctorID;
    	$scope.editPatient.medicineID = patient.medicineID;  	
    	$scope.editPatient.firstName = patient.firstName;
    	$scope.editPatient.middleName = patient.middleName;
    	$scope.editPatient.lastName = patient.lastName;
    	$scope.editPatient.birthday = patient.birthday;
    	$scope.editPatient.sex = patient.sex;
    	$scope.editPatient.admissionDate = patient.admissionDate;
    	$scope.editPatient.dischargeDate = patient.dischargeDate;
    	$scope.editPatient.status = patient.status ;
    	$scope.editPatient.totalBill = patient.totalBill; 		
    }
    	
    $scope.submit = function() {
    	//insert
    	if($scope.mode === "Add Patient") {
    		$scope.addPatient();
    		$scope.clear();
    	}
    	else {
    		//update
    		$scope.mode = "Add Patient";
    	    $scope.updatePatient();
    	}
    }	
    
    //list
    $scope.getPatients = function() {
    	const url = '/PatientList';
    	$http.get(url)
    		.then(
    				response => {
    					$scope.patients = response.data.patientList;
    					console.log('success' + $scope.patientList);
    				},
    				error => {
    					console.log('something went wrong in retrieving data');
    				}
    		)
    }
    
    //delete
    $scope.deletePatient = function() {
    	var index = $scope.patients.indexOf($scope.editPatient);
    	var url = '/PatientDelete';
		var headers = {
				headers: {'Content-Type':'application/json'}
	    };
    	 $http.post(url, $scope.editPatient, headers)
      	.then(
      			response => {
      				console.log('delete successful');
      				$scope.patients.splice(index, 1);
      			},
      			error => {
      				console.log('something went wrong in deleting data');
      			}
      	)
     
    }
    
    //kwaon ang data gkan sa table para ma edit gamit sa updatePatient()
    
    $scope.editPatientInfo = function(patient) {
    	var data = {
    			id: patient.id,
    			fullName: patient.fullName
    	}
		$scope.patient = data;
    	$scope.mode = "Done editing";
    }
    
    //update
    $scope.updatePatient = function() {
    	var url = '/PatientUpdate';
    	var patient =  new  $scope.EditPatient($scope.editPatient.id, $scope.editPatient.diseaseID, $scope.editPatient.symptomID,
    			$scope.editPatient.doctorID, $scope.editPatient.medicineID, $scope.editPatient.firstName, $scope.editPatient.middleName, $scope.editPatient.lastName, 
    			$scope.editPatient.birthday, $scope.editPatient.sex, $scope.editPatient.admissionDate, $scope.editPatient.dischargeDate, $scope.editPatient.status,
    			$scope.editPatient.totalBill);
    	
		var headers = {
				headers: {'Content-Type':'application/json'}
	    };
		
		 $http.post(url, patient ,headers)
	      	.then(
	      			response => {
	      				console.log('update successful');
	      			    $scope.getPatients();
	      			    $scope.clear();
	      			},
	      			error => {
	      				console.log('something went wrong in deleting data');
	      			}
	      	)
	      	console.log(patient);
    }
    
    //insert
    $scope.addPatient = function() {
    	const url = '/PatientCreate';
        var patient = new $scope.Patient($scope.patient.id,  $scope.patient.firstName, $scope.patient.middleName, $scope.patient.lastName,
        		                        $scope.patient.birthday, $scope.patient.sex, $scope.patient.admissionDate, $scope.patient.dischargeDate,
        		                        $scope.patient.status, $scope.patient.totalBill);
        		                     
       
        var headers = {
        		headers: {'Content-Type':'application/x-www-form-urlencoded'}
        };
        
        $http.post(url, $httpParamSerializer(patient), headers)
        	.then(
        			response => {
        				$scope.getPatients();
        				console.log('add successful');
        			},
        			error => {
        				console.log('something went wrong in adding data');
        			}
        	)
        
        console.log(patient);
    }
    
    //create object
    $scope.EditPatient = function(id, diseaseID, symptomID, doctorID, medicineID, firstName, middleName, lastName, birthday, sex, admissionDate, dischargeDate, status, totalBill ) {
    
    	this.id = id;
    	this.diseaseID = diseaseID;  	
    	this.symptomID = symptomID
    	this.doctorID = doctorID;
    	this.medicineID	= medicineID;
    	this.firstName = firstName;
    	this.middleName = middleName;
    	this.lastName = lastName;
    	this.birthday = birthday;
    	this.sex = sex;
    	this.admissionDate  = admissionDate;
    	this.dischargeDate = dischargeDate;
    	this.status = status;
    	this.totalBill = totalBill;
    }
    
    $scope.Patient = function(id, firstName, middleName, lastName, birthday, sex, admissionDate, dischargeDate, status, totalBill ) {
        
    	this.id = id;
    	this.firstName = firstName;
    	this.middleName = middleName;
    	this.lastName = lastName;
    	this.birthday = birthday;
    	this.sex = sex;
    	this.admissionDate  = admissionDate;
    	this.dischargeDate = dischargeDate;
    	this.status = status;
    	this.totalBill = totalBill;
    }
    
    $scope.clear = function() {
    	for(p in $scope.patient) {
    		$scope.patient[p] = "";
    	}
    
    	console.log('clear');
    }
    
    //display patients
    $scope.getPatients();
})