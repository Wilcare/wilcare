app.controller('adminCtrl', function($scope, $http, $httpParamSerializer, $routeParams) {

    $scope.addStaffs = "Add Staff";
    $scope.addSymptoms = "Add Symptom";
    $scope.addDiseases = "Add Diseases";
    $scope.addMedicines = "Add Medicines";

    $scope.staff = {
    		id: "",
            username: "",
            password: "",
    		firstName: "", 
    		middleName: "",
    		lastName: "",
            staffType: "",
    		staffStatus: false
    }
    	
    $scope.submit = function() {
    	//insert
    	if($scope.addStaffs === "Add Staff") {
    		$scope.addStaff();
    		$scope.clear();
    	}
    	else {
    		//update
    		$scope.addStaffs = "Add Staff";
    	    $scope.updateStaff();
    	}
    }	
    
    //create object
    $scope.Staff = function(id, username, password, firstName, middleName, lastName, staffType, staffStatus) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.staffType = staffType;
        this.staffStatus = staffStatus;
    }
    
});

app.controller('listAdminCtrl', function($scope, $http, $httpParamSerializer) {
	
	$scope.staffs = [];
	
	$scope.deleteStaff = function() {
    	var url = '/StaffDelete';
		var headers = {
				headers: {'Content-Type':'application/x-www-form-urlencoded'}
	    };
    	 $http.post(url, $httpParamSerializer($scope.editStaff), headers)
      	.then(
      			response => {
      				console.log('delete successful');
      				$scope.staffs.splice($scope.editStaff.index, 1);
      			},
      			error => {
      				console.log('something went wrong in deleting data');
      			}
      	)
     
    }
    
    $scope.clear = function() {
    	for(s in $scope.staff) {
    		$scope.staff[s] = "";
    	}
    
    	console.log('clear');
    }
	
    $scope.editStaff = {
    		id: "",
            username: "username",
            password: "password",
    		firstName: "firstName",
    		middleName: "middleName",
    		lastName: "lastName",
    		staffType: "staffType",
    		staffStatus: false,
    		index: ""
    }
	
    //list
    $scope.getStaffs = function() {
    	const url = '/StaffList';
    	$http.get(url)
    		.then(
    				response => {
    					$scope.staffs = response.data.staffList;
    					console.log('success' +  	$scope.staffs[0].id);
    				},
    				error => {
    					console.log('something went wrong in retrieving data');
    				}
    		)
    }
	
    $scope.displayToModal = function(staff) {  	
    	console.log("success");
    	$scope.editStaff.id = staff.id;
        $scope.editStaff.username = staff.username;
        $scope.editStaff.password = staff.password;
    	$scope.editStaff.firstName = staff.firstName;
    	$scope.editStaff.middleName = staff.middleName;
    	$scope.editStaff.lastName = staff.lastName;
        $scope.editStaff.staffType = staff.staffType;
    	$scope.editStaff.staffStatus = staff.staffStatus;
    	$scope.editStaff.index = $scope.staffs.indexOf(staff);
    }
    
    $scope.editStaffInfo = function(staff) {
    	var data = {
    			id: staff.id,
    			fullName: staff.fullName
    	}
		$scope.staff = data;
    	$scope.addStaffs = "Done editing";
    }
    
    //update
    $scope.updateStaff = function() {
    	var url = '/StaffUpdate';
		var headers = {
				headers: {'Content-Type':'application/x-www-form-urlencoded'}
	    };
		
		 $http.post(url, $httpParamSerializer($scope.editStaff), headers)
	      	.then(
	      			response => {
	      				console.log('update successful');
	      			    $scope.getStaffs();
	      			    $scope.clear();
	      			},
	      			error => {
	      				console.log('something went wrong in deleting data');
	      			}
	      	)
    }
    
    //insert
    $scope.addStaff = function() {
    	const url = '/StaffCreate';
        var staff = new $scope.Staff (
            $scope.staff.id, $scope.staff.username, $scope.staff.password, $scope.staff.firstName, $scope.staff.middleName, $scope.staff.lastName,
            $scope.staff.staffType, $scope.staff.staffStatus
        );
        var headers = {
        		headers: {'Content-Type':'application/x-www-form-urlencoded'}
        };
        
        $http.post(url, $httpParamSerializer(staff), headers)
        	.then(
        			response => {
        				console.log('add successful');
        			},
        			error => {
        				console.log('something went wrong in adding data');
        			}
        	)
        console.log(staff);
    }
    
    $scope.getStaffs();
});
