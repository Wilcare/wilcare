package wilcare.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class StaffServiceTest extends AppEngineTestCase {

    private StaffService service = new StaffService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
