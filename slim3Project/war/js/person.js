// sets the ng-app to be declared.
var app = angular.module("personApp", []);
app.controller("personController", function($scope, $http, $httpParamSerializer) {
	$scope.addUpdate = "ADD";
	
	$scope.personList = [];
	
	$scope.simpleRetrieve = function(){
		$http.get("/SimpleRetrieve")
			.then(function(response) {
				// if the response is success
				// Getting the response data.
				console.log("simpleRetrieve");
				console.log(response.data);
			}, function() {
				alert("An error has occured");
			});
	}
	
	$scope.simpleRetrieve1 = function() {
		var retrievePromise = $http.get("/SimpleRetrieve");

		retrievePromise.success(function(data, status, headers, config) {
			console.log("simpleRetrieve1");
			console.log(data);
		});
		retrievePromise.error(function(data, status, headers, config) {
			alert("An error has occured");
		});
	}
	$scope.simpleRetrieve();
	$scope.simpleRetrieve1();
	
	
	$scope.loadPerson = function(personId) {
		// get the person with that id then fill in the input fields.
		for (var index=0; index<$scope.personList.length; index++) {
			if ($scope.personList[index].id == personId) {
				$scope.idField = $scope.personList[index].id;
				$scope.firstName = $scope.personList[index].firstName;
			    $scope.lastName = $scope.personList[index].lastName;
			    $scope.address = $scope.personList[index].address;
			    $scope.age = $scope.personList[index].age;
			    $scope.addUpdate = "UPDATE";
			    return;
			}
		}
		
	}
	
	$scope.storePerson = function() {
		if ($scope.addUpdate == "ADD") {
			$scope.createPerson();
		} else {
			$scope.updatePerson();
		}
	}
	
	$scope.createPerson = function(){
		var jsonData = {
				fName: $scope.firstName,
				lName: $scope.lastName,
				address: $scope.address,
				age: $scope.age
		};
		
		//$http.post("/CreatePerson", jsonData)
		$http.post("/CreatePerson", $httpParamSerializer(jsonData),
				{// configuring the request not a JSON type.
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			)
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					//There were no errors.
					$scope.clearAllFields();
					alert("Successfully Added Item");
					// updating the table
					$scope.listItems();
				} else {
					// display the error messages.
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i];
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	
	$scope.updatePerson = function() {
		var jsonData = {
				id: $scope.idField,
				fName: $scope.firstName,
				lName: $scope.lastName,
				address: $scope.address,
				age: $scope.age
		};
		
		$http.post("/UpdatePerson", jsonData)
			.then(function(response) {
				
				if (response.data.errorList.length == 0) {
					//There were no errors.
					$scope.clearAllFields();
					alert("Successfully Updated Item");
					$scope.listItems();
					
				} else {
					// display the error messages.
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i];
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	
	$scope.deletePerson = function(personId){
		$scope.clearAllFields();
		var jsonData = {
				id: personId
			};
		
		$http.post("/DeletePerson", jsonData)
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					//There were no errors.
					alert("Successfully Deleted Item");
					$scope.listItems();
				} else {
					// display the error messages.
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i];
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	
	
	$scope.listItems = function() {
		$http.get("/ListPerson")
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					//There were no errors.
					
					// passing the json data from the response to the personList
					$scope.personList = response.data.personList;
				} else {
					// display the error messages.
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i];
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}
	
	$scope.clearAllFields = function() {
		$scope.idField = "";
		$scope.firstName = "";
	    $scope.lastName = "";
	    $scope.address = "";
	    $scope.age = "";
	    $scope.addUpdate = "ADD";
	}
	
	$scope.listItems();
});