package population.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import population.meta.PersonModelMeta;
import population.model.PersonModel;

public class PersonDao {
    
    public List<PersonModel> getPersonList(){
        List<PersonModel> personModelList = new ArrayList<PersonModel>();
        PersonModelMeta meta = PersonModelMeta.get();
        personModelList = Datastore.query(meta)
                .asList();
        return personModelList;
    }
    
    
    public PersonModel getPersonById(PersonModel inputPerson){
        PersonModel personModel = new PersonModel();
        PersonModelMeta meta = PersonModelMeta.get();
        personModel = Datastore.query(meta)
                .filter(meta.id.equal(inputPerson.getId()))
                .asSingle();
        return personModel;
    }
    
    private void generateKeyAndId(PersonModel inputPerson) {
        Key parentKey = KeyFactory.createKey("Person", inputPerson.getFirstName());
        Key key = Datastore.allocateId(parentKey, "PersonModel");

        inputPerson.setKey(key);
        inputPerson.setId(key.getId());
    }
    
    public void insertPerson(PersonModel inputPerson) {
        Transaction transaction = Datastore.beginTransaction();

        this.generateKeyAndId(inputPerson);

        Datastore.put(inputPerson);
        transaction.commit();
    }
    
    public void updatePerson(PersonModel inputPerson) {
        Transaction transaction = Datastore.beginTransaction();
        Datastore.put(inputPerson);
        transaction.commit();
    }
    
    public void deletePerson(PersonModel inputPerson) {
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputPerson.getKey());
        transaction.commit();
    } 
    
}
