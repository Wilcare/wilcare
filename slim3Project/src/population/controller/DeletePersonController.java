package population.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import population.common.GlobalConstants;
import population.dto.PersonDto;
import population.service.PersonService;

public class DeletePersonController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        System.out.println("DeletePersonController.run " + "start");
        PersonService personService = new PersonService();
        PersonDto personDto = new PersonDto();
        JSONObject json = null;

        try {
            json = new JSONObject(this.request.getReader().readLine());
            Long id = json.getLong("id");

         
            // add in to the datastore
            personDto.setId(id);

            personDto = personService.deletePerson(personDto);
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            personDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", personDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("DeletePersonController.run " + "end");
        return null;
    }
}
