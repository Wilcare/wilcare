package population.controller;

import java.util.Map;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validator;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import population.common.GlobalConstants;
import population.dto.PersonDto;
import population.service.PersonService;
import population.utils.JSONValidators;

public class CreatePersonController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        PersonService personService = new PersonService();
        PersonDto personDto = new PersonDto();
        JSONObject json = null;

        try{
            Validators validator = new Validators(this.request);
            
            validator.add("fName", validator.required());
            validator.add("lName", validator.required());
            validator.add("address", validator.required());
            validator.add("age", validator.required());
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
                
                // add in to the datastore
                personDto.setFirstName(json.getString("fName"));
                personDto.setLastName(json.getString("lName"));
                personDto.setAddress(json.getString("address"));
                personDto.setAge(json.getInt("age"));  

                personDto = personService.insertPerson(personDto);
                               
            }else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    personDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(personDto.getErrorList().size() > 0) {
              json.put("errors", personDto.getErrorList());
          }
            
           
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            personDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", personDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("CreatePersonController.run " + "end");
        return null;
    }
}
