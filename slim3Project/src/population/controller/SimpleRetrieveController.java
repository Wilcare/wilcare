package population.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import population.common.GlobalConstants;
import population.dto.PersonListDto;

public class SimpleRetrieveController extends Controller {

    @Override
    public Navigation run() throws Exception {
        
        JSONObject json = new JSONObject();
      
        // adding properties to the JSONObject
        json.put("car", "honda");
        json.put("fruit", "banana");
        json.put("place", "cebu");
        json.put("object", "table");
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("ListPersonController.run " + "end");
        return null;
    }
}
