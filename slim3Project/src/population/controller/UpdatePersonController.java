package population.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import population.common.GlobalConstants;
import population.dto.PersonDto;
import population.service.PersonService;
import population.utils.JSONValidators;

public class UpdatePersonController extends Controller {

    @Override
    protected Navigation run() throws Exception {
        System.out.println("UpdatePersonController.run " + "start");
        PersonService personService = new PersonService();
        PersonDto personDto = new PersonDto();
        JSONObject json = null;

        try {
            json = new JSONObject(this.request.getReader().readLine());
            JSONValidators validator = new JSONValidators(json);
            
            validator.add("id", validator.required());
            validator.add("fName", validator.required());
            validator.add("lName", validator.required());
            validator.add("address", validator.required());
            validator.add("age", validator.required());
            
            if(validator.validate()) {
             // add in to the datastore
                personDto.setId(json.getLong("id"));
                personDto.setFirstName(json.getString("fName"));
                personDto.setLastName(json.getString("lName"));
                personDto.setAddress(json.getString("address"));
                personDto.setAge(json.getInt("age"));  
                
                personDto = personService.updatePerson(personDto);
             } else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    personDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
             }
             if(personDto.getErrorList().size() > 0) {
                 json.put("errors", personDto.getErrorList());
             }
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            personDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", personDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("UpdatePersonController.run " + "end");
        return null;
    }
}
