/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package population.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the DTO for the list of todo entity.
 * @author Lehmar Cabrillos
 * @version 0.01
 * Version History
 * [03/11/2016] 0.01 � Lehmar Cabrillos  � Initial codes.
 */
public class PersonListDto extends BaseDto {

    /**
     * List of person dtos.
     */
    private List<PersonDto> persons = new ArrayList<PersonDto>();

    /**
     * Retrieve person list.
     * @return person list.
     */
    public List<PersonDto> getEntries() {
        return persons;
    }

    /**
     * Sets person list.
     * @param persons the person list to set.
     */
    public void setEntries(List<PersonDto> persons) {
        this.persons = persons;
    }
}