package population.service;

import java.util.List;

import population.common.GlobalConstants;
import population.dao.PersonDao;
import population.dto.PersonDto;
import population.dto.PersonListDto;
import population.model.PersonModel;

public class PersonService {
    PersonDao personDao = new PersonDao();
    
    public PersonDto insertPerson(PersonDto inputPerson) {
        PersonModel personModel = new PersonModel();

        // adding the details to the model
        personModel.setId(inputPerson.getId());
        personModel.setFirstName(inputPerson.getFirstName());
        personModel.setLastName(inputPerson.getLastName());
        personModel.setAddress(inputPerson.getAddress());
        personModel.setAge(inputPerson.getAge());
        
            try {
                    personDao.insertPerson(personModel);
                    System.out.println("added person successfully");
            } catch (Exception e) {
                    System.out.println("Cannot add person");
                    inputPerson.addError("Cannot add person");
            }
 
        return inputPerson;
     }  
    
    
    public PersonDto updatePerson(PersonDto inputPerson) {
       
        PersonModel personModel = new PersonModel();

        // adding the details to the model
        personModel.setId(inputPerson.getId());
        personModel.setFirstName(inputPerson.getFirstName());
        personModel.setLastName(inputPerson.getLastName());
        personModel.setAddress(inputPerson.getAddress());
        personModel.setAge(inputPerson.getAge());
        
            try {
                PersonModel resultModel = personDao.getPersonById(personModel);
                if (null != resultModel) {
                    personModel.setKey(resultModel.getKey());
                    personDao.updatePerson(personModel);
                    System.out.println("added person successfully");
                }
                   
            } catch (Exception e) {
                    System.out.println("Cannot add person");
                    inputPerson.addError("Cannot add person");
            }
 
        return inputPerson;
     } 
    
    public PersonDto deletePerson(PersonDto inputPerson) {
       
        PersonModel personModel = new PersonModel();

        // adding the details to the model
        personModel.setId(inputPerson.getId());
        
        try {
            PersonModel resultModel = personDao.getPersonById(personModel);
            if (null != resultModel) {
                personModel.setKey(resultModel.getKey());
                personDao.deletePerson(personModel);
                System.out.println("added person successfully");
            }     
        } catch (Exception e) {
            System.out.println("Cannot add person");
            inputPerson.addError("Cannot add person");
        }
 
        return inputPerson;
     }
    
    public PersonListDto getTodoList() {
        System.out.println("PersonListDto.getTodoList " + "start");
        // initializing the dto to hold the list of todos.
        PersonListDto personListDto =  new PersonListDto();
        
        try {
            // get the list of todos with the given status. 
            List<PersonModel> personList = personDao.getPersonList();
            if (personList != null) {
                // convert each todoModel from the todoList into TodoDto
                for (PersonModel resultModel : personList) {
                    PersonDto personDto = new PersonDto();
                    
                    personDto.setId(resultModel.getId());
                    personDto.setFirstName(resultModel.getFirstName());
                    personDto.setLastName(resultModel.getLastName());
                    personDto.setAddress(resultModel.getAddress());
                    personDto.setAge(resultModel.getAge());
                    
                    // adding the dto to the list
                    personListDto.getEntries().add(personDto);
                }
            }
        }catch(Exception e) {
            personListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        System.out.println("PersonListDto.getTodoList " + "end");
        return personListDto;
    }
}
