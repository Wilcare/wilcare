package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class DiseaseListDto {
private List<DiseaseDto>  diseaseListDto;
    
    public DiseaseListDto() {
        diseaseListDto = new LinkedList<DiseaseDto>();
    }
    
    public void addDiseaseDto(DiseaseDto diseaseDto) {
        diseaseListDto.add(diseaseDto);
    }
    
    public List<DiseaseDto> getDiseaseList() {
        return diseaseListDto;
    }
    
    public void setDiseaseList(List<DiseaseDto> diseaseDto) {
        diseaseListDto = diseaseDto;
    }
}
