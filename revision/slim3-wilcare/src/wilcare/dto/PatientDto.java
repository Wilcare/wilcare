package wilcare.dto;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */

import java.util.List;

public class PatientDto extends BaseDto {
        
   
   private Long id;
   private List<Long> diseaseID;
   private List<Long> symptomID;
   private List<Long> doctorID;
   private List<Long> medicineID;
   private String firstName;
   private String middleName;
   private String lastName;
   private String birthday;
   private String sex;
   private String admissionDate;
   private String dischargeDate;
   private boolean status;
   private String bloodType;
   private String condition;
   private double totalBill;
 
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public List<Long> getDiseaseID() {
        return diseaseID;
    }
    public void setDiseaseID(List<Long> diseaseID) {
        this.diseaseID = diseaseID;
    }
    public List<Long> getSymptomID() {
        return symptomID;
    }
    public void setSymptomID(List<Long> symptomID) {
        this.symptomID = symptomID;
    }
    public List<Long> getDoctorID() {
        return doctorID;
    }
    public void setDoctorID(List<Long> doctorID) {
        this.doctorID = doctorID;
    }
    
  
    public List<Long> getMedicineID() {
        return medicineID;
    }
    public void setMedicineID(List<Long> medicineID) {
        this.medicineID = medicineID;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getBirthday() {
        return birthday;
    }
    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    public String getAdmissionDate() {
        return admissionDate;
    }
    public void setAdmissionDate(String admissionDate) {
        this.admissionDate = admissionDate;
    }
    public String getDischargeDate() {
        return dischargeDate;
    }
    public void setDischargeDate(String dischargeDate) {
        this.dischargeDate = dischargeDate;
    }
    public boolean isStatus() {
        return status;
    }
    public void setStatus(boolean status) {
        this.status = status;
    }
    public double getTotalBill() {
        return totalBill;
    }
    public void setTotalBill(double totalBill) {
        this.totalBill = totalBill;
    }
    public String getBloodType() {
        return bloodType;
    }
    public void setBloodType(String bloodType) {
        this.bloodType = bloodType;
    }
    public String getCondition() {
        return condition;
    }
    public void setCondition(String condition) {
        this.condition = condition;
    }
       

}
