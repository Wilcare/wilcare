package wilcare.dto;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class SymptomDto extends BaseDto {
    
    
   private Long id;
   private String symptomName;
   private String symptomDescription;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getSymptomName() {
        return symptomName;
    }
    public void setSymptomName(String symptomName) {
        this.symptomName = symptomName;
    }
    public String getSymptomDescription() {
        return symptomDescription;
    }
    public void setSymptomDescription(String symptomDescription) {
        this.symptomDescription = symptomDescription;
    }
       

}
