package wilcare.dto;

import java.util.LinkedList;
import java.util.List;

public class AdminListDto {
 private List<AdminDto> adminListDto;
    
    public AdminListDto() {
        adminListDto = new LinkedList<AdminDto>();
    }
    
    public void addAdminDto(AdminDto adminDto) {
        adminListDto.add(adminDto);
    }
    
    public List<AdminDto> getAdminList() {
        return adminListDto;
    }
    
    public void setAdminList(List<AdminDto> doctorDto) {
        adminListDto = doctorDto;
    }
}
