package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import wilcare.meta.SymptomModelMeta;
import wilcare.model.SymptomModel;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class SymptomDao extends DaoBase<SymptomModel>{

    public SymptomModel getSymptomByName(SymptomModel inputObject){
        SymptomModel objectModel = new SymptomModel();
        
        System.out.println("SymptomDao.getSymptomByName " + "start");
        
        SymptomModelMeta meta = SymptomModelMeta.get();
        objectModel = Datastore.query(meta)
                     .filter(meta.symptomName.equal(inputObject.getSymptomName()))
                     .asSingle();
        
        System.out.println("SymptomDao.getSymptomByName " + "end");
        return objectModel;
        
    }
    
    public SymptomModel getSymptomByNameId(SymptomModel inputObject){
        SymptomModel objectModel = new SymptomModel();
        
        System.out.println("SymptomDao.getSymptomByNameId " + "start");
        
        SymptomModelMeta meta = SymptomModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.symptomName.equal(inputObject.getSymptomName())
                               ,meta.id.equal(inputObject.getId())
                                ).asSingle();
        System.out.println(Datastore.query(meta).asList().get(0).getId());
        System.out.println("SymptomDao.getSymptomByNameId " + "end");
        return objectModel;
        
    }
       
    public SymptomModel getSymptomById(SymptomModel inputObject){
        
        SymptomModel objectModel = new SymptomModel();
        
        System.out.println("SymptomDao.getSymptomById " + "start");
        
        SymptomModelMeta meta = SymptomModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId())).asSingle();
                             
        System.out.println( "wilson");
        System.out.println("SymptomDao.getSymptomById " + "end");
        return objectModel;
        
    }
       
    
    public void insertSymptom(SymptomModel inputObject) {
        System.out.println("SymptomDao.insertSymptom " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
        Key parentKey = KeyFactory.createKey("SymptomModel", inputObject.getSymptomName());
        Key key = Datastore.allocateId(parentKey, "SymptomModel");
        
        //passing the key in Model
        inputObject.setKey(key);
        inputObject.setId(key.getId());     
        Datastore.put(inputObject);
        transaction.commit();
        
        System.out.println("SymptomDao.insertSymptom " + "end");
    }   
    
    public void updateSymptom(SymptomModel inputOjbect) {
        System.out.println("SymptomDao.updateSymptom " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("SymptomDao.updateSymptom " + "end");
    }   
    
    public void deleteSymptom(SymptomModel inputOjbect) {
        System.out.println("SymptomDao.deleteSymptom " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        
        System.out.println("SymptomDao.deleteSymptom " + "end");
    }   
    
    public List<SymptomModel> getSymptoms() {
      
        return Datastore.query(SymptomModel.class).asList();
    }
    
    
}
