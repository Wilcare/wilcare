package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import wilcare.meta.AdminModelMeta;
import wilcare.model.AdminModel;


public class AdminDao extends DaoBase<AdminModel>{

    public AdminModel getAdminById(AdminModel inputObject){
        AdminModel objectModel = new AdminModel();
        
        System.out.println("AdminDao.getAdminById " + "start");        
        AdminModelMeta meta = AdminModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId())).asSingle();                             
        
        System.out.println("AdminDao.getAdminById " + "end");
        return objectModel;        
    }
       
    
    public void insertAdmin(AdminModel inputOjbect) {
        System.out.println("AdminDao.insertAdmin " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
        Key parentKey = KeyFactory.createKey("AdminModel", inputOjbect.getLastName());
        Key key = Datastore.allocateId(parentKey, "AdminModel");
        
        //passing the key in DoctorModel
        inputOjbect.setKey(key);
        inputOjbect.setId(key.getId());     
        Datastore.put(inputOjbect);
        transaction.commit();
        
        System.out.println("AdminDao.insertAdmin " + "end");
        
    }   
      
    public void updateAdmin(AdminModel inputOjbect) {
        System.out.println("AdminDao.updateAdmin " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("AdminDao.updateAdmin " + "end");
    }   
    
    public void deleteAdmin(AdminModel inputOjbect) {
        System.out.println("AdminDao.deleteAdmin " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        System.out.println("AdminDao.deleteAdmin " + "end");
    }   
    public List<AdminModel> getAdmin() {        
        return Datastore.query(AdminModel.class).asList();
    }    
    
    
}
