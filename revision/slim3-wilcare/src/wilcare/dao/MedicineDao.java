package wilcare.dao;

import java.util.List;

import org.slim3.datastore.DaoBase;
import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;


import wilcare.model.MedicineModel;
import wilcare.meta.MedicineModelMeta;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/03/2016] 1.00 Wilson Justine G. Sison - initial codes
 */


public class MedicineDao extends DaoBase<MedicineDao>{
    
    public MedicineModel getMedicineByName(MedicineModel inputObject){
        MedicineModel objectModel = new MedicineModel();
        
        System.out.println("MedicineDao.getMedicineByName " + "start");
        
        MedicineModelMeta meta = MedicineModelMeta.get();
        objectModel = Datastore.query(meta).filter(meta.medicineName.equal(inputObject.getMedicineName())).asSingle();
        
        System.out.println("MedicineDao.getDieaseByName " + "end");
        return objectModel;
        
    }
    
    public MedicineModel getMedicineByNameId(MedicineModel inputObject){
        MedicineModel objectModel = new MedicineModel();
        
        System.out.println("MedicineDao.getMedicineByNameId " + "start");
        
        MedicineModelMeta meta = MedicineModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.medicineName.equal(inputObject.getMedicineName())
                               ,meta.id.equal(inputObject.getId())
                                ).asSingle();
        
        System.out.println("MedicineDao.getMedicineByNameId " + "end");
        return objectModel;
        
    }
       
    public MedicineModel getMedicineById(MedicineModel inputObject){
        MedicineModel objectModel = new MedicineModel();
        
        System.out.println("MedicineDao.getMedicineById " + "start");
        
        MedicineModelMeta meta = MedicineModelMeta.get();
        objectModel = Datastore.query(meta)
                        .filter(meta.id.equal(inputObject.getId())).asSingle();
                             
        
        System.out.println("MedicineDao.getMedicineById " + "end");
        return objectModel;
        
    }
       
    
    public void insertMedicine(MedicineModel inputObject) {
        System.out.println("MedicineDao.insertMedicine " + "start");
      
        Transaction transaction = Datastore.beginTransaction();
        //creates key
         Key parentKey = KeyFactory.createKey("DiseaseModel", inputObject.getMedicineName());
         Key key = Datastore.allocateId(parentKey, "DiseaseModel");
         
         //passing the key in Model
         inputObject.setKey(key);
         inputObject.setId(key.getId());
         Datastore.put(inputObject);
         transaction.commit();
        
        System.out.println("MedicineDao.insertMedicine " + "end");
    }   
    
    public void updateMedicine(MedicineModel inputOjbect) {
        System.out.println("MedicineDao.updateMedicine " + "start");
      
        Transaction transaction = Datastore.beginTransaction();     
        Datastore.put(inputOjbect);     
        transaction.commit();
        
        System.out.println("MedicineDao.updateMedicine " + "end");
    }   
    
    public void deleteMedicine(MedicineModel inputOjbect) {
        System.out.println("MedicineDao.deleteMedicine " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOjbect.getKey());     
        transaction.commit();
        System.out.println("MedicineDao.deleteMedicine " + "end");
    }   
    public List<MedicineModel> getMedicine() {
        
        return Datastore.query(MedicineModel.class).asList();
    }
}
