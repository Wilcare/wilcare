package wilcare.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-19 10:23:31")
/** */
public final class AdminModelMeta extends org.slim3.datastore.ModelMeta<wilcare.model.AdminModel> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> firstName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "firstName", "firstName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> lastName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "lastName", "lastName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> middleName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "middleName", "middleName");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> password = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "password", "password");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Boolean> status = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Boolean>(this, "status", "status", boolean.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> type = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "type", "type");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel> username = new org.slim3.datastore.StringAttributeMeta<wilcare.model.AdminModel>(this, "username", "username");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.AdminModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final AdminModelMeta slim3_singleton = new AdminModelMeta();

    /**
     * @return the singleton
     */
    public static AdminModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public AdminModelMeta() {
        super("AdminModel", wilcare.model.AdminModel.class);
    }

    @Override
    public wilcare.model.AdminModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        wilcare.model.AdminModel model = new wilcare.model.AdminModel();
        model.setFirstName((java.lang.String) entity.getProperty("firstName"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setLastName((java.lang.String) entity.getProperty("lastName"));
        model.setMiddleName((java.lang.String) entity.getProperty("middleName"));
        model.setPassword((java.lang.String) entity.getProperty("password"));
        model.setStatus(booleanToPrimitiveBoolean((java.lang.Boolean) entity.getProperty("status")));
        model.setType((java.lang.String) entity.getProperty("type"));
        model.setUsername((java.lang.String) entity.getProperty("username"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("firstName", m.getFirstName());
        entity.setProperty("id", m.getId());
        entity.setProperty("lastName", m.getLastName());
        entity.setProperty("middleName", m.getMiddleName());
        entity.setProperty("password", m.getPassword());
        entity.setProperty("status", m.isStatus());
        entity.setProperty("type", m.getType());
        entity.setProperty("username", m.getUsername());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        wilcare.model.AdminModel m = (wilcare.model.AdminModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getFirstName() != null){
            writer.setNextPropertyName("firstName");
            encoder0.encode(writer, m.getFirstName());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getLastName() != null){
            writer.setNextPropertyName("lastName");
            encoder0.encode(writer, m.getLastName());
        }
        if(m.getMiddleName() != null){
            writer.setNextPropertyName("middleName");
            encoder0.encode(writer, m.getMiddleName());
        }
        if(m.getPassword() != null){
            writer.setNextPropertyName("password");
            encoder0.encode(writer, m.getPassword());
        }
        writer.setNextPropertyName("status");
        encoder0.encode(writer, m.isStatus());
        if(m.getType() != null){
            writer.setNextPropertyName("type");
            encoder0.encode(writer, m.getType());
        }
        if(m.getUsername() != null){
            writer.setNextPropertyName("username");
            encoder0.encode(writer, m.getUsername());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected wilcare.model.AdminModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        wilcare.model.AdminModel m = new wilcare.model.AdminModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("firstName");
        m.setFirstName(decoder0.decode(reader, m.getFirstName()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("lastName");
        m.setLastName(decoder0.decode(reader, m.getLastName()));
        reader = rootReader.newObjectReader("middleName");
        m.setMiddleName(decoder0.decode(reader, m.getMiddleName()));
        reader = rootReader.newObjectReader("password");
        m.setPassword(decoder0.decode(reader, m.getPassword()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.isStatus()));
        reader = rootReader.newObjectReader("type");
        m.setType(decoder0.decode(reader, m.getType()));
        reader = rootReader.newObjectReader("username");
        m.setUsername(decoder0.decode(reader, m.getUsername()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}