package wilcare.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-19 10:23:31")
/** */
public final class SymptomModelMeta extends org.slim3.datastore.ModelMeta<wilcare.model.SymptomModel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.SymptomModel> symptomDescription = new org.slim3.datastore.StringAttributeMeta<wilcare.model.SymptomModel>(this, "symptomDescription", "symptomDescription");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.SymptomModel> symptomName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.SymptomModel>(this, "symptomName", "symptomName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.SymptomModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final SymptomModelMeta slim3_singleton = new SymptomModelMeta();

    /**
     * @return the singleton
     */
    public static SymptomModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public SymptomModelMeta() {
        super("SymptomModel", wilcare.model.SymptomModel.class);
    }

    @Override
    public wilcare.model.SymptomModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        wilcare.model.SymptomModel model = new wilcare.model.SymptomModel();
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setSymptomDescription((java.lang.String) entity.getProperty("symptomDescription"));
        model.setSymptomName((java.lang.String) entity.getProperty("symptomName"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("id", m.getId());
        entity.setProperty("symptomDescription", m.getSymptomDescription());
        entity.setProperty("symptomName", m.getSymptomName());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        wilcare.model.SymptomModel m = (wilcare.model.SymptomModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getSymptomDescription() != null){
            writer.setNextPropertyName("symptomDescription");
            encoder0.encode(writer, m.getSymptomDescription());
        }
        if(m.getSymptomName() != null){
            writer.setNextPropertyName("symptomName");
            encoder0.encode(writer, m.getSymptomName());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected wilcare.model.SymptomModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        wilcare.model.SymptomModel m = new wilcare.model.SymptomModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("symptomDescription");
        m.setSymptomDescription(decoder0.decode(reader, m.getSymptomDescription()));
        reader = rootReader.newObjectReader("symptomName");
        m.setSymptomName(decoder0.decode(reader, m.getSymptomName()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}