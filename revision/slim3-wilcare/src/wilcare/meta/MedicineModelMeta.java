package wilcare.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2016-10-19 10:23:31")
/** */
public final class MedicineModelMeta extends org.slim3.datastore.ModelMeta<wilcare.model.MedicineModel> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.MedicineModel> medicineDescription = new org.slim3.datastore.StringAttributeMeta<wilcare.model.MedicineModel>(this, "medicineDescription", "medicineDescription");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<wilcare.model.MedicineModel> medicineName = new org.slim3.datastore.StringAttributeMeta<wilcare.model.MedicineModel>(this, "medicineName", "medicineName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Double> price = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Double>(this, "price", "price", double.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<wilcare.model.MedicineModel, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final MedicineModelMeta slim3_singleton = new MedicineModelMeta();

    /**
     * @return the singleton
     */
    public static MedicineModelMeta get() {
       return slim3_singleton;
    }

    /** */
    public MedicineModelMeta() {
        super("MedicineModel", wilcare.model.MedicineModel.class);
    }

    @Override
    public wilcare.model.MedicineModel entityToModel(com.google.appengine.api.datastore.Entity entity) {
        wilcare.model.MedicineModel model = new wilcare.model.MedicineModel();
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setMedicineDescription((java.lang.String) entity.getProperty("medicineDescription"));
        model.setMedicineName((java.lang.String) entity.getProperty("medicineName"));
        model.setPrice(doubleToPrimitiveDouble((java.lang.Double) entity.getProperty("price")));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("id", m.getId());
        entity.setProperty("medicineDescription", m.getMedicineDescription());
        entity.setProperty("medicineName", m.getMedicineName());
        entity.setProperty("price", m.getPrice());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        wilcare.model.MedicineModel m = (wilcare.model.MedicineModel) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMedicineDescription() != null){
            writer.setNextPropertyName("medicineDescription");
            encoder0.encode(writer, m.getMedicineDescription());
        }
        if(m.getMedicineName() != null){
            writer.setNextPropertyName("medicineName");
            encoder0.encode(writer, m.getMedicineName());
        }
        writer.setNextPropertyName("price");
        encoder0.encode(writer, m.getPrice());
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected wilcare.model.MedicineModel jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        wilcare.model.MedicineModel m = new wilcare.model.MedicineModel();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("medicineDescription");
        m.setMedicineDescription(decoder0.decode(reader, m.getMedicineDescription()));
        reader = rootReader.newObjectReader("medicineName");
        m.setMedicineName(decoder0.decode(reader, m.getMedicineName()));
        reader = rootReader.newObjectReader("price");
        m.setPrice(decoder0.decode(reader, m.getPrice()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}