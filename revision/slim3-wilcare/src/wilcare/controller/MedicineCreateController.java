package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.dto.MedicineDto;
import wilcare.service.MedicineService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */

public class MedicineCreateController extends Controller {
    MedicineService objectService = new MedicineService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        MedicineDto objectDto = new MedicineDto();
        System.out.println("MedicineCreateController.run " + "start");
        try{
            Validators validator = new Validators(this.request); 
            validator.add("medicineName", validator.required());
            validator.add("medicineDescription", validator.required());
            validator.add("price", validator.required());
           
            if(validator.validate()){
                jsonObject = new JSONObject(new RequestMap(this.request));
           
            String medicineName = jsonObject.getString("medicineName");
            String medicineDescription = jsonObject.getString("medicineDescription");
            Double price = jsonObject.getDouble("price");
            
            
            objectDto.setMedicineName(medicineName);
            objectDto.setMedicineDescription(medicineDescription);
            objectDto.setPrice(price);
        
            this.objectService.insertMedicine(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                   
              }
          }
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("MedicineCreateController.run.exception " + e.toString());
            //   objectDto.addError(error);
            if(jsonObject == null ){
                jsonObject = new JSONObject();
                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        //response.setContentType();
        response.getWriter().write(jsonObject.toString());
        System.out.println("MedicineCreateController.run " + "end");
        return null;
    }
}
//Slim3
/*
// Getting all the information sent from the request.
 String medicineName = request.getParameter("medicineName");
 String medicineDescription =  request.getParameter("medicineDescription");
 String price = request.getParameter("price");

// setting the values of DTO from the request
 objectDto.setMedicineName(medicineName);
 objectDto.setMedicineDescription(medicineDescription);
 objectDto.setPrice(Double.parseDouble(price));
 */