package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.DoctorListDto;
import wilcare.service.DoctorService;

public class DoctorListController extends Controller {
    DoctorService objectService = new  DoctorService();
    JSONObject jsonObject = new JSONObject();
    @Override
    public Navigation run() throws Exception {
        
        DoctorListDto ListDto = objectService.getDoctorList();
        jsonObject.put("doctorList",ListDto.getDoctorList());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
