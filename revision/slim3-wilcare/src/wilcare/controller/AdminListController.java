package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.AdminListDto;
import wilcare.service.AdminService;

public class AdminListController extends Controller {
    AdminService objectService = new  AdminService();
    JSONObject jsonObject = new JSONObject();
    @Override
    
    public Navigation run() throws Exception {
        
        AdminListDto ListDto = objectService.getAdminList();        
        jsonObject.put("adminList",ListDto.getAdminList());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
