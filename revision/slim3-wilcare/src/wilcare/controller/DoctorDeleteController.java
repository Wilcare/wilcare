package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.DoctorDto;
import wilcare.service.DoctorService;

public class DoctorDeleteController extends Controller {
   DoctorService objectService = new DoctorService();
    @Override
    public Navigation run() throws Exception {
        
        DoctorDto objectDto = new DoctorDto();
        JSONObject jsonObject;
        
        System.out.println("DoctorDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());           
            Long id = jsonObject.getLong("id");           
            objectDto.setId(id); 
             this.objectService.deleteDoctor(objectDto);            
        }catch(Exception e){
            System.out.println("DoctorDeleteController.run.exception " + e.toString());
        }
        System.out.println("DoctorDeleteController.run " + "end");
        
        return null;
    }
}
