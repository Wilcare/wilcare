package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.PatientListDto;
import wilcare.service.PatientService;

public class PatientListController extends Controller {

    PatientService objectService = new PatientService();
    JSONObject jsonObject = new JSONObject();
    
    @Override
    public Navigation run() throws Exception {
        PatientListDto ListDto = objectService.getPatientList();
        jsonObject.put("patientList",ListDto.getPatientList());
        
        response.setContentType("application/json");
        response.getWriter().write(jsonObject.toString());
        
        return null;
    }
}
