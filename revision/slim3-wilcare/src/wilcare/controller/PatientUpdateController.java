package wilcare.controller;


import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONArray;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.common.GlobalConstants;
import wilcare.dto.PatientDto;
import wilcare.service.PatientService;
import wilcare.utils.JSONValidators;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class PatientUpdateController extends Controller {
    PatientService objectService = new PatientService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        PatientDto objectDto = new PatientDto();
    
        System.out.println("PatientUpdateController.run " + "start");
        try{                        
            
            JSONValidators validator = new JSONValidators(this.request);             
            validator.add("symptomID", validator.required());
            validator.add("medicineID", validator.required());
            validator.add("doctorID", validator.required());
            validator.add("diseaseID", validator.required());
            validator.add("firstName", validator.required());
            validator.add("middleName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("birthday", validator.required());
            validator.add("sex", validator.required());
            validator.add("admissionDate", validator.required());
            validator.add("dischargeDate", validator.required());
            validator.add("status", validator.required());
            validator.add("totalBill", validator.required());
            
           
            if(validator.validate()){    
            jsonObject = new JSONObject(this.request.getReader().readLine());
            
            JSONArray  symptomID = jsonObject.getJSONArray("symptomID");
            JSONArray  medicineID = jsonObject.getJSONArray("medicineID");
            JSONArray  doctorID = jsonObject.getJSONArray("doctorID");
            JSONArray  diseaseID = jsonObject.getJSONArray("doctorID");
            
            String firstName = jsonObject.getString("firstName");
            String middleName = jsonObject.getString("middleName");
            String lastName = jsonObject.getString("lastName");
            String birthday = jsonObject.getString("birthday");
            String sex = jsonObject.getString("sex");
            String admissionDate = jsonObject.getString("admissionDate");
            String dischargeDate = jsonObject.getString("dischargeDate");
            Boolean status = jsonObject.getBoolean("status");
            Double totalBill = jsonObject.getDouble("totalBill");
            
            List<Long> symptomIDList = new ArrayList<Long>();
            List<Long> medicineIDList = new ArrayList<Long>();     
            List<Long> doctorIDList = new ArrayList<Long>(); 
            List<Long> diseaseIDList = new ArrayList<Long>(); 
            
            for (int i = 0; i < symptomID.length(); i++){
                symptomIDList.add(symptomID.getLong(i));
            }
            for (int i = 0; i < medicineID.length(); i++){
                medicineIDList.add(medicineID.getLong(i));
            }             
            for (int i = 0; i < doctorID.length(); i++){
                doctorIDList.add(doctorID.getLong(i));
            }
            for (int i = 0; i < diseaseID.length(); i++){
                diseaseIDList.add(diseaseID.getLong(i));
            }       
            
            objectDto.setDiseaseID(diseaseIDList);
            objectDto.setDoctorID(doctorIDList);
            objectDto.setSymptomID(symptomIDList);
            objectDto.setMedicineID(diseaseIDList);
            objectDto.setFirstName(firstName);
            objectDto.setMiddleName(middleName);
            objectDto.setLastName(lastName);
            objectDto.setBirthday(birthday);
            objectDto.setSex(sex);
            objectDto.setAdmissionDate(admissionDate);
            objectDto.setDischargeDate(dischargeDate);
            objectDto.setStatus(status);
            objectDto.setTotalBill(totalBill);
            
         
            this.objectService.updatePatient(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("PatientUpdateController.run.exception " + e.toString());
            objectDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        response.getWriter().write(jsonObject.toString());
        System.out.println("PatientUpdateController.run " + "end");
     
        return null;
    }
}
