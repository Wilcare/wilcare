package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import wilcare.dto.AdminDto;
import wilcare.service.AdminService;
/**
 * @author Wilcare
 * @version 1.00
 * Version History
 *      [09/05/2016] 1.00 Wilson Justine G. Sison - initial codes
 */
public class AdminDeleteController extends Controller {
   AdminService objectService = new AdminService();
    @Override
    public Navigation run() throws Exception {
        
        AdminDto objectDto = new AdminDto();
        JSONObject jsonObject;
        
        System.out.println("AdminDeleteController.run " + "start");
        try{
            jsonObject = new JSONObject(this.request.getReader().readLine());           
            Long id = jsonObject.getLong("id");           
            objectDto.setId(id); 
             this.objectService.deleteAdmin(objectDto);       
        }catch(Exception e){
            System.out.println("AdminDeleteController.run.exception " + e.toString());
        }
        System.out.println("AdminDeleteController.run " + "end");
        
        return null;
    }
}
