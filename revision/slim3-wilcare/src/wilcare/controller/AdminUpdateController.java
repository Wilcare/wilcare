package wilcare.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import wilcare.common.GlobalConstants;
import wilcare.dto.AdminDto;
import wilcare.service.AdminService;

public class AdminUpdateController extends Controller {

    AdminService  adminService = new  AdminService();
    JSONObject jsonObject = null;
    @Override
    public Navigation run() throws Exception {
        AdminDto objectDto = new AdminDto();
        System.out.println("AdminCreateController.run " + "start");
        
        try{
            Validators validator = new Validators(this.request); 
            validator.add("username", validator.required());
            validator.add("password", validator.required());
            validator.add("firstName", validator.required());
            validator.add("lastName", validator.required());
            validator.add("middleName", validator.required());  
            validator.add("type", validator.required());
            validator.add("status", validator.required());
            
            if(validator.validate()){
            jsonObject = new JSONObject(new RequestMap(this.request));
            
            String username = jsonObject.getString("username");
            String password = jsonObject.getString("password");
            String firstName =jsonObject.getString("firstName");
            String lastName = jsonObject.getString("lastName");
            String middleName = jsonObject.getString("middleName"); 
            String type = jsonObject.getString("type");  
            Boolean status = jsonObject.getBoolean("status");
            
            objectDto.setFirstName(firstName);
            objectDto.setMiddleName(middleName);
            objectDto.setLastName(lastName);
            objectDto.setUsername(username);
            objectDto.setPassword(password);
            objectDto.setType(type);
            objectDto.setStatus(status);      
                    
            this.adminService.updateAdmin(objectDto);
            }
            else{
                for(int i = 0; i < validator.getErrors().size(); i ++){
                    objectDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));                    
                }                
            }
            
            if(objectDto.getErrorList().size() > 0){
                jsonObject.put("errors",objectDto.getErrorList());                
            }
        }catch(Exception e){
            System.out.println("AdminCreateController.run.exception " + e.toString());
            objectDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            if(jsonObject == null ){
                jsonObject = new JSONObject();                
            }
        }
        jsonObject.put("errorList", objectDto.getErrorList());
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        response.getWriter().write(jsonObject.toString());
     
        
        System.out.println("AdminCreateController.run " + "end");
        return null;
    }
}
