package wilcare.service;

import java.util.List;

import wilcare.dao.MedicineDao;
import wilcare.dto.MedicineDto;
import wilcare.dto.MedicineListDto;
import wilcare.model.MedicineModel;

public class MedicineService {
    
    MedicineDao objectDao =  new MedicineDao();
    
    public MedicineDto insertMedicine(MedicineDto inputDto) {               
        MedicineModel objectModel = new MedicineModel(); 
        
        System.out.println("MedicineService.insertMedicine " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setMedicineName(inputDto.getMedicineName());
        objectModel.setMedicineDescription(inputDto.getMedicineDescription());
        objectModel.setPrice(inputDto.getPrice());       
        
        try{
            MedicineModel resultModel = this.objectDao.getMedicineByName(objectModel);
            if(resultModel != null){                
                System.out.println(" Medicine Name already exist");
            }else{
                try{
                    this.objectDao.insertMedicine(objectModel);
                    System.out.println("-------Adding Succesful-------");
                }catch(Exception e){
                    System.out.println("Exception in adding item: " + e.toString());
                }                
            }
            
        }catch(Exception e){
            System.out.println("Exception in Adding item: " + e.toString());
        }
        
        System.out.println("MedicineService.insertMedicine " + "end");
        return inputDto;
    }
    public MedicineDto updateMedicine(MedicineDto inputDto) {
        MedicineModel objectModel = new MedicineModel(); 
        System.out.println("MedicineService.updateMedicine " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setMedicineName(inputDto.getMedicineName());
        objectModel.setMedicineDescription(inputDto.getMedicineDescription());
        objectModel.setPrice(inputDto.getPrice());       
        try{
            MedicineModel objectModel2 = this.objectDao.getMedicineById(objectModel);
            if(objectModel2!=null){
                objectModel.setKey(objectModel2.getKey());
                this.objectDao.updateMedicine(objectModel);
                System.out.println("Updated Medicine Information");
            } else {               
                System.out.println("There is no item with the same id.");
            }
        }catch(Exception e){
            System.out.println("Exception in Updating Medicine Information: " + e.toString());   
        }
    
        
        System.out.println("MedicineService.updateMedicine " + "end");
        
        
        return inputDto;
    }
    public MedicineDto deleteMedicine(MedicineDto inputDto) {
        MedicineModel objectModel = new MedicineModel(); 
        
        System.out.println("MedicineService.deleteMedicine " + "start");
        objectModel.setId(inputDto.getId());
        objectModel.setMedicineName(inputDto.getMedicineName());
        objectModel.setMedicineDescription(inputDto.getMedicineDescription());
        objectModel.setPrice(inputDto.getPrice());       
        try{
            MedicineModel resultModel = this.objectDao.getMedicineByNameId(objectModel);
            if(resultModel != null){
                this.objectDao.deleteMedicine(resultModel);                
                System.out.println("Deleted Medicine");               
            }else {
               System.out.println("No Medicine was Found!");
        }       
        }catch(Exception e){
            System.out.println("Exception in deleting  item: " + e.toString());            
        }                
        
        System.out.println("MedicineService.deleteMedicine " + "end");
        
        return inputDto;
    }
    
    public MedicineModel storeDtoToModel(MedicineDto inputDto) {
        System.out.println("MedicineService.storeDtoToModel " + "start");
        
        MedicineModel objectModel = new MedicineModel(); 
        objectModel.setId(inputDto.getId());
        objectModel.setMedicineName(inputDto.getMedicineName());
        objectModel.setMedicineDescription(inputDto.getMedicineDescription());
        objectModel.setPrice(inputDto.getPrice());   
        
        System.out.println("MedicineService.storeDtoToModel " + "end");
        return objectModel;
    }
    
    public MedicineListDto  getMedicineList() {
        MedicineListDto listDto = new MedicineListDto();
        List<MedicineModel> modelList = objectDao.getMedicine();
        
        for(MedicineModel s : modelList) {
            MedicineDto objectDto = new MedicineDto();
            objectDto.setId(s.getId());
            objectDto.setMedicineName(s.getMedicineName());
            objectDto.setMedicineDescription(s.getMedicineDescription());
            objectDto.setPrice(s.getPrice());   
            listDto.addMedicineDto(objectDto);
        }
        
        return listDto; 
    }
}
