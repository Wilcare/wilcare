wilcare.config(["$routeProvider", function($routeProvider) {
	$routeProvider
	.when("/", {
		resolve: {
			'check': function($location, $localStorage) {
				if($localStorage != undefined) {
					if($localStorage.user == "doctor")
						$location.path("/doctor")
				}
			}
		},
		templateUrl: "../adminScreen/adminRecords.html"})
	.when("/records", {
		templateUrl: "../adminScreen/adminRecords.html"})
	.when("/addRecord", {
		templateUrl: "../adminScreen/adminAddRecord.html"})
	.when("/medicines", {
		templateUrl: "../adminScreen/adminMedicines.html"})
		.when("/addMedicine", {
		templateUrl: "../adminScreen/adminAddMedicine.html"})
	.when("/diseases", {
		templateUrl: "../adminScreen/adminDiseases.html"})
	.when("/addDisease", {
		templateUrl: "../adminScreen/adminAddDisease.html"})
	.when("/symptoms", {
		templateUrl: "../adminScreen/adminSymptoms.html"})
	.when("/addSymptom", {
		templateUrl: "../adminScreen/adminAddSymptom.html"})
	.when("/logout", {
		templateUrl: "../mainScreen/login.html"})
}]);

wilcare.controller('adminController', function($scope, $http, $httpParamSerializer) {
	console.log("adminController start");
	$scope.adminMode  = "Add";
	$scope.doctorMode  = "Add";
	$scope.medicineMode  = "Add";
	$scope.symptomMode  = "Add";		
	
	$scope.admin = {
	    	id: "",
	    	username:"",
	    	password:"",
	    	firstName: "", 
	    	middleName: "",
	    	lastName: "",
	   		type:"",
	    	status:true  		
	    }
	
	$scope.doctor = {
			id: "",
			username:"",
			password:"",
			firstName: "", 
			middleName: "",
			lastName: "",
			type:"doctor",	
			price:0.0,
			status:true  		
	}
	
	$scope.editDoctor = {
			id: "",
			patientID: [],
			username:"",
			password:"",
			firstName: "", 
			middleName: "",
			lastName: "",
			type:"doctor",
			price:0.0,
			status:true  		
	}
	
	
	$scope.medicine = {
			id: "",
			medicineName:"",
			medicineDescription:"",
			price:0.0
	}
	
	$scope.symptom = {
			id: "",
			symptomName:"",
			symptomDescription:""	
	}
	
	$scope.adminList = [];
	$scope.doctorList = [];
	$scope.medicineList = [];
	$scope.symptomList = [];
	
	$scope.Doctor = function(id,username,password, firstName, middleName, lastName, type, price, status ) {
		this.id = id;
		this.username = username;
		this.password = password;
	    this.firstName = firstName;
	    this.middleName = middleName;
	    this.lastName = lastName;
	    this.type = type;
	    this.price = price;
	    this.status = status;
	}
	
	$scope.EditDoctor = function(id,patientID,username,password, firstName, middleName, lastName, type, price, status ) {
		this.id = id;
		this.patientID = patientID;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.type = type;
		this.price = price;
		this.status = status;  
	}
	
	$scope.Admin = function( id, username, 	password, firstName,  middleName, lastName,  type, status ){
		this.id = id;
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName
		this.type = type;
		this.status = status;	
	}	
	
	$scope.Medicine = function( id, medicineName, medicineDescription, price ){
		this.id = id;
		this.medicineName = medicineName;
		this.medicineDescription = medicineDescription;
		this.price = price;
	}
	
	$scope.Symptom = function(	id, symptomName, symptomDescription){
		this.id = id;
		this.symptomName = symptomName;
		this.symptomDescription = symptomDescription;
	}
	
	$scope.submitDoctor = function() {	
		if($scope.doctorMode === "Add") {
			$scope.addDoctor();
			$scope.clearDoctor();
		}
		else {
		  $scope.doctorMode = "Update"
		  $scope.updateDoctor();
		}
	}	
	
	$scope.submitAdmin = function() {	
		if($scope.adminMode === "Add") {
			$scope.addAdmin();
			$scope.clear();
		}
		else {
		  $scope.adminMode = "Update"
		  $scope.updateAdmin();
		}
	}	
	
	$scope.submitMedicine = function() {	
		if($scope.medicineMode === "Add") {
			$scope.addMedicine();
			$scope.clear();
		}
		else {
		  $scope.medicineMode = "Update"
		  $scope.updateMedicine();
		}
	}	
	
	$scope.submitSymptom = function() {	
		if($scope.symptomMode === "Add") {
			$scope.addSymptom();
			$scope.clear();
		}
		else {
		  $scope.symptomMode = "Update"
		  $scope.updateSymptom();
		}
	}	
	
	
	$scope.addDoctor = function() {
		const url = '/DoctorCreate';
		
		var doctor = new $scope.Doctor(
				$scope.doctor.id, 
				$scope.doctor.username, 
				$scope.doctor.password,
				$scope.doctor.firstName, 
			    $scope.doctor.middleName,
			    $scope.doctor.lastName,
			    $scope.doctor.type,
			    $scope.doctor.price,
			    $scope.doctor.status );	
		var headers = {
				 headers: {'Content-Type':'application/x-www-form-urlencoded'}  
	    };
		
		 $http.post(url, $httpParamSerializer(doctor), headers)
			.then(
					response => {
						$scope.getDoctorList();
						$scope.clearDoctor();
					},
					error => {
						console.log('something went wrong in adding data');
					}
			)
			console.log(doctor);
		
	}
	
	//$scope.deleteDoctor = function() {
	//	var index = $scope.patients.indexOf($scope.editPatient);
	//	var url = '/DoctorDelete';
	//	var headers = {
	//			headers: {'Content-Type':'application/json'}
	//    };
	//	 $http.post(url, $scope.editPatient, headers)
	//  	.then(
	//  			response => {
	//  				console.log('delete successful');
	//  				$scope.patients.splice(index, 1);
	//  			},
	//  			error => {
	//  				console.log('something went wrong in deleting data');
	//  			}
	//  	) 
	//}
	
	$scope.addAdmin = function() {
		const url = '/AdminCreate';
		
		var admin = new $scope.Admin($scope.admin.id, $scope.admin.username, $scope.admin.password, $scope.admin.firstName, 
									$scope.admin.middleName, $scope.admin.lastName, $scope.admin.type,  $scope.doctor.status 
									 );	
		
		var headers = {
			 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
	    };
		
		  $http.post(url, $httpParamSerializer(admin), headers)
			.then(
					response => {
						$scope.getAdminList();
						$scope.clear();
					},
					error => {
						console.log('something went wrong in adding data');
					}
			)
			console.log(admin);
	}
	
	$scope.deleteAdmin = function() {
		
		var index = $scope.adminList.indexOf($scope.admin);
		var url = '/AdminDelete';
		var headers = {
				headers: {'Content-Type':'application/x-www-form-urlencoded'}
	    };
		 $http.post(url, $httpParamSerializer($scope.admin), headers)
	  	.then(
	  			response => {
	  				console.log('delete successful');
	  				$scope.adminList.splice(index, 1);
	  			},
	  			error => {
	  				console.log('something went wrong in deleting data');
	  			}
	  	) 
	}
	
	$scope.addMedicine = function() {
		const url = '/MedicineCreate';
		
		var	medicine = new $scope.Medicine($scope.medicine.id, $scope.medicine.medicineName,$scope.medicine.medicineDescription, 
											$scope.medicine.price);	
		
		var headers = {
			 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
	    };
		
		  $http.post(url, $httpParamSerializer(medicine), headers)
			.then(
					response => {
						$scope.getMedicineList();
						$scope.clear();
					},
					error => {
						console.log('something went wrong in adding data');
					}
			)
			console.log(medicine);
	}
	
			
	$scope.deleteMedicine = function() {
		
		var index = $scope.medicineList.indexOf($scope.medicine);
		var url = '/MedicineDelete';
		var headers = {
				headers: {'Content-Type':'application/x-www-form-urlencoded'}
	    };
		 $http.post(url, $httpParamSerializer($scope.medicine), headers)
	  	.then(
	  			response => {
	  				console.log('delete successful');
	  				$scope.medicineList.splice(index, 1);
	  			},
	  			error => {
	  				console.log('something went wrong in deleting data');
	  			}
	  	) 
	}
	
	
	$scope.addSymptom = function() {
		const url = '/SymptomCreate';
		
		var	symptom = new $scope.Medicine($scope.symptom.id, $scope.symptom.symptomName, $scope.symptom.symptomDescription);									
		
		var headers = {
			 headers: {'Content-Type':'application/x-www-form-urlencoded'}           
	    };
		
		  $http.post(url, $httpParamSerializer(symptom), headers)
			.then(
					response => {
						$scope.getSymptomList();
						$scope.clear();
					},
					error => {
						console.log('something went wrong in adding data');
					}
			)
			console.log(symptom);
	}
	
   $scope.deleteSymptom = function() {			
		var index = $scope.symptomList.indexOf($scope.symptom);
		var url = '/SymptomDelete';
		var headers = {
				headers: {'Content-Type':'application/x-www-form-urlencoded'}
	    };
		 $http.post(url, $httpParamSerializer($scope.symptom), headers)
	  	.then(
	  			response => {
	  				console.log('delete successful');
	  				$scope.symptomList.splice(index, 1);
	  			},
	  			error => {
	  				console.log('something went wrong in deleting data');
	  			}
	  	) 
	}
	
	
	
	$scope.getDoctorList = function() {
		const url = '/DoctorList';
		$http.get(url)
			.then(
					response => {
						$scope.doctorList = response.data.doctorList;
						console.log('success' + $scope.doctorList);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
	$scope.getAdminList = function() {
		const url = '/AdminList';
		$http.get(url)
			.then(
					response => {
						$scope.adminList = response.data.adminList;
						console.log('success' + $scope.adminList);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
	$scope.getMedicineList = function() {
		const url = '/MedicineList';
		$http.get(url)
			.then(
					response => {
						$scope.medicineList = response.data.medicineList;
						console.log('success' + $scope.medicineList);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
	$scope.getSymptomList = function() {
		const url = '/SymptomList';
		$http.get(url)
			.then(
					response => {
						$scope.symptomList = response.data.symptomList;
						console.log('success' + $scope.symptomList);
					},
					error => {
						console.log('something went wrong in retrieving data');
					}
			)
	}
	
 	$scope.clearDoctor = function() {
    	for(p in $scope.doctor) {
    		$scope.doctor[p] = "";
    	}		    	
		username:"";
		password:"";
		firstName: "";
		middleName: "";
		lastName: "";
		type:"doctor";	
		price:0.0;
		status:true;  	
    }
});   


