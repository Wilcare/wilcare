wilcare.config(["$routeProvider", function($routeProvider) {
	$routeProvider
	.when("/", {
		resolve: {
			'check': function($location, $localStorage) {
				if($localStorage != undefined) {
					if($localStorage.user == "doctor")
						$location.path("/doctor")
				}
			}
		},
		templateUrl: "../doctorScreen/doctorRecords.html",
		controller: "loginController"})
	.when("/doctor", {
		templateUrl: "../doctorScreen/doctor.html"})
	.when("/records", {
		templateUrl: "../doctorScreen/doctorRecords.html"})
	.when("/medicines", {
		templateUrl: "../doctorScreen/doctorMedicines.html"})
	.when("/diseases", {
		templateUrl: "../doctorScreen/doctorDiseases.html"})
	.when("/addPatient", {
		templateUrl: "../doctorScreen/doctorAddPatient.html"})
	.when("/logout", {
		templateUrl: "../Index.html"})
}]);

wilcare.controller('doctorController', function($scope, $http, $httpParamSerializer, $timeout) {
	console.log("wilcareDoctor.wilcareDoctorController " + "start");
	
	$scope.monthArray = [
		{optionValue: 0 , numValue: "00", displayValue: "Month"},
		{optionValue: 1 , numValue: "01", displayValue: "January"},
		{optionValue: 2 , numValue: "02", displayValue: "February"},
		{optionValue: 3 , numValue: "03", displayValue: "March"},
		{optionValue: 4 , numValue: "04", displayValue: "April"},
		{optionValue: 5 , numValue: "05", displayValue: "May"},
		{optionValue: 6 , numValue: "06", displayValue: "June"},
		{optionValue: 7 , numValue: "07", displayValue: "July"},
		{optionValue: 8 , numValue: "08", displayValue: "August"},
		{optionValue: 9 , numValue: "09", displayValue: "September"},
		{optionValue: 10, numValue: "10", displayValue: "October"},
		{optionValue: 11, numValue: "11", displayValue: "November"},
		{optionValue: 12, numValue: "12", displayValue: "December"}
	];

	$scope.sexArray = [
		{optionValue: 0, displayValue: "Male"},
		{optionValue: 1, displayValue: "Female"},
	];

	$scope.statusArray = [
		{optionValue: 0, displayValue: "false"},
		{optionValue: 1, displayValue: "true"},
	];
	

	$scope.patient = {
    		id: "",
    		firstName: "", 
    		middleName: "",
    		lastName: "",
    		sex: "",
    		birthday: "",
    		admissionDate: "",
    		dischargeDate: "not set",
    		status: true,
    		totalBill: 0.0   		
    }
    
    $scope.editPatient = {
		id: "",
		diseaseID: [],
		symptomID: [],
		doctorID: [],
		medicineID:[],   	
		firstName: "", 
		middleName: "",
		lastName: "",
		birthday: "",
		sex: "",
		admissionDate: "",
		dischargeDate: "not set",
		status: true,
		totalBill: 0.0    		
    }

	$scope.patients = [];
    $scope.mode = "Add Patient";
    
    $scope.selectedRecord = $scope.patient;

	$scope.initAddPatient = function(){
		$scope.birthMonth = $scope.dischargeMonth = $scope.monthArray[0];
		$scope.patient.sex = $scope.sexArray[0];
	}
	
	$scope.submit = function() {
    	//insert
    	if($scope.mode === "Add Patient") {
    		$scope.addPatient();
    		$scope.clear();
    	}
    	//update
    	else {
    		$scope.mode = "Add Patient";
    	    $scope.updatePatient();
    	}
    }
	
	$scope.addPatient = function() {
    	const url = '/PatientCreate';
    	$scope.patient.birthday = $scope.birthMonth + " " + $scope.birthDay + ", " + $scope.birthYear;
        var patient = new $scope.Patient($scope.patient.id,  $scope.patient.firstName, $scope.patient.middleName, $scope.patient.lastName,
        		                        $scope.patient.birthday, $scope.patient.sex, $scope.patient.admissionDate, $scope.patient.dischargeDate,
        		                        $scope.patient.status, $scope.patient.totalBill);
        var headers = {
        		headers: {'Content-Type':'application/x-www-form-urlencoded'}           
        };
        $http.post(url, $httpParamSerializer(patient), headers)
        	.then(
        			response => {
        				$scope.getPatients();
        				$scope.clear();
        			},
        			error => {
        				console.log('something went wrong in adding data');
        			}
        	)
        console.log(patient);
    }
	
	$scope.updatePatient = function(){
		const url = '/PatientUpdate';
		var headers = {
		     headers: {'Content-Type':'application/json'}  
		};
		$http.post(url, $scope.editPatient, headers)
			.then(
				response => {
					$scope.getPatients();
					$scope.clear();
				},
				error => {
					console.log('something went wrong in adding data');
				}
			)
		console.log("UPDATED PATIENT");
	}
	
	$scope.Patient = function(id, firstName, middleName, lastName, birthday, sex, admissionDate, dischargeDate, status, totalBill ) {
    	this.id = id;
    	this.firstName = firstName;
    	this.middleName = middleName;
    	this.lastName = lastName;
    	this.birthday = birthday;
    	this.sex = sex;
    	this.admissionDate  = admissionDate;
    	this.dischargeDate = dischargeDate;
    	this.status = status;
    	this.totalBill = totalBill;
    }
	
	$scope.setAdmissionDate = function() {
		var today = new Date();
		$scope.admissionDay = today.getDate();
		var month = today.getMonth() + 1; //January is 0!
		$scope.admissionYear = today.getFullYear();
		switch(month) {
		case 1: $scope.admissionMonth = "January";
			break;
		case 2: $scope.admissionMonth = "February";
		break;
		case 3: $scope.admissionMonth = "March";
		break;
		case 4: $scope.admissionMonth = "April";
		break;
		case 5: $scope.admissionMonth = "May";
		break;
		case 6: $scope.admissionMonth = "June";
		break;
		case 7: $scope.admissionMonth = "July";
		break;
		case 8: $scope.admissionMonth = "August";
		break;
		case 9: $scope.admissionMonth = "September";
		break;
		case 10: $scope.admissionMonth = "October";
		break;
		case 11: $scope.admissionMonth = "November";
		break;
		default: $scope.admissionMonth = "December";
		break;	
		}
		$scope.patient.admissionDate = $scope.admissionMonth + " " + $scope.admissionDay + ", " + $scope.admissionYear;
	}

	$scope.getPatients = function() {
    	const url = '/PatientList';
    	$http.get(url)
    		.then(
				response => {
					$scope.patients = response.data.patientList;
					console.log('success' + $scope.patients);
				},
				error => {
					console.log('something went wrong in retrieving data');
				}
    		)
    }
	
	$scope.filterMedicine = function(){
		
	}
	
	$scope.deletePatient = function(){
		var index = $scope.patients.indexOf($scope.editPatient);
		var url = '/PatientDelete';
		var headers = {
				headers: {'Content-Type':'application/json'}
	    };
    	 $http.post(url, $scope.editPatient, headers)
      	.then(
      			response => {
      				console.log('delete successful');
      				$scope.patients.splice(index, 1);
      			},
      			error => {
      				console.log('something went wrong in deleting data');
      			}
      	)
	}
	
	$scope.clear = function() {
    	for(p in $scope.patient) {
    		$scope.patient[p] = "";
    	}
    	$scope.initAddPatient();
    	$scope.birthMonth = $scope.monthArray[0];
    	$scope.birthDay = "";
    	$scope.birthYear = "";
    	$scope.dischargeDate = "not set";
    	$scope.status = true;
    	$scope.totalBill = 0.0;
    }
	
	$scope.selectRecord = function(patient){
		$scope.setEditPatient(patient);
		$scope.displayPatientRecord(patient);
	}
	
	$scope.displayPatientRecord = function(patient) {
		var modalWrapper = angular.element(document.querySelector('#modalWrapper'));
		var modalContainer = angular.element(document.querySelector('#modalContainer'));
		var close = angular.element(document.querySelector('#close'));
		modalWrapper.addClass("showModalWrapper");
		modalContainer.addClass("showModalContainer");
		close.on('click', function() {
			modalContainer.removeClass("showModalContainer");
			modalWrapper.removeClass("showModalWrapper");
		})
	}
	
	$scope.setEditPatient = function(patient) {
		$scope.editPatient.id = patient.id;
    	$scope.editPatient.diseaseID = patient.diseaseID;
    	$scope.editPatient.symptomID = patient.symptomID;
    	$scope.editPatient.doctorID = patient.doctorID;
    	$scope.editPatient.medicineID = patient.medicineID;  	
    	$scope.editPatient.firstName = patient.firstName;
    	$scope.editPatient.middleName = patient.middleName;
    	$scope.editPatient.lastName = patient.lastName;
    	$scope.editPatient.birthday = patient.birthday;
    	$scope.editPatient.sex = patient.sex;
    	$scope.editPatient.admissionDate = patient.admissionDate;
    	$scope.editPatient.dischargeDate = patient.dischargeDate;
    	$scope.editPatient.status = patient.status ;
    	$scope.editPatient.totalBill = patient.totalBill;
    }
	
	$scope.initAddPatient();
	$scope.getPatients();
	$scope.setAdmissionDate();
});
