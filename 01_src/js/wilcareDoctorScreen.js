/** ------------------------------------------------------------------------------
WilCare - HEALTH CARE APPLICATION
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Christopher Justine Distrito
* @version 0.02
* Version History
* [07/26/2016] 0.01 – Christopher Justine Distrito – initial codes
* [07/27/2016] 0.02 – Christopher Justine Distrito – modified changContent function
**/

/**
* The screen that is currently displayed.
**/
var displayedScreen = "screenRecords";

/*
* This function is called to change the displayed div,
* label and image of the div#body depending on the selected
* 'displayScreen'.
* @param displayScreen - 'id' of the div to be displayed
*/
function changeContent(displayScreen) {
	if (displayedScreen != displayScreen) {

		document.getElementById("records").style.visibility = "hidden";
		document.getElementById("medicines").style.visibility = "hidden";
		document.getElementById("functionsViewingInfo").style.visibility = "hidden";
		document.getElementById("pages").style.visibility = "visible";
		
		switch (displayScreen) {
			/* Displays records screen.*/
			case "screenRecords":
				document.getElementById("addPatient").style.visibility = "hidden";
				document.getElementById("records").style.visibility = "visible";
				document.getElementById("record").style.backgroundColor = "rgb(106,214,255)";
				document.getElementById("record").style.color = "white";
				document.getElementById("medicine").style.backgroundColor = "white";
				document.getElementById("medicine").style.color = "rgb(216,216,216)";
				/* Sets maximum number of pages. */
				document.getElementById("numberOfPages").innerHTML = "of 540";
				/* Hides record, medicine and their tables. */
				document.getElementById("functions").style.visibility = "visible";
				document.getElementById("entryInformation").style.visibility = "hidden";
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				break;
			/* Displays medicines screen.*/
			case "screenMedicines":
				document.getElementById("addPatient").style.visibility = "hidden";
				document.getElementById("medicines").style.visibility = "visible";
				document.getElementById("record").style.backgroundColor = "white";
				document.getElementById("record").style.color = "rgb(216,216,216)";
				document.getElementById("medicine").style.backgroundColor = "rgb(106,214,255)";
				document.getElementById("medicine").style.color = "white";
				/* Sets maximum number of pages. */
				document.getElementById("numberOfPages").innerHTML = "of 300";
				/* Hides record or medicine entry information. */
				document.getElementById("functions").style.visibility = "visible";
				document.getElementById("entryInformation").style.visibility = "hidden";
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				break;
			/* Displays information of record entry 1.*/
			case "screenRecordEntry1":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows record tables. */
				document.getElementById("patientTable").style.visibility = "visible";
				document.getElementById("patientDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("patientName").innerHTML = "Name: Vincent Joseph Oplas";
				document.getElementById("patientAddress").innerHTML = "Address: Somewhere Down The Road";
				document.getElementById("patientCity").innerHTML = "City: Cebu City";
				document.getElementById("patientState").innerHTML = "State: Cebu";
				document.getElementById("patientZip").innerHTML = "Zip: 6000";
				document.getElementById("patientBirthdate").innerHTML = "Birth Date: March 30, 1996";
				document.getElementById("patientSex").innerHTML = "Sex: Male";
				document.getElementById("patientAdmissionDate").innerHTML = "Admission Date: 07/24/2016";
				document.getElementById("patientDischargeDate").innerHTML = "Discharge Date: ---";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("patientDisease").innerHTML = "Disease: POEMS Syndrome";
				document.getElementById("patientMedicine").innerHTML = "Medicine: Anti-POEMS";
				document.getElementById("patientMedicineQuantity").innerHTML = "Qty: 13";
				/* Hides medicine tables. */
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenRecords")};
				break;
			/* Displays information of record entry 2.*/
			case "screenRecordEntry2":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows record tables. */
				document.getElementById("patientTable").style.visibility = "visible";
				document.getElementById("patientDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("patientName").innerHTML = "Name: Chance Adrian Angam";
				document.getElementById("patientAddress").innerHTML = "Address: Do&ntildea Maria Vill. 1 Punta Princesa";
				document.getElementById("patientCity").innerHTML = "City: Cebu City";
				document.getElementById("patientState").innerHTML = "State: Cebu";
				document.getElementById("patientZip").innerHTML = "Zip: 6000";
				document.getElementById("patientBirthdate").innerHTML = "Birth Date: March 31, 1996";
				document.getElementById("patientSex").innerHTML = "Sex: Male";
				document.getElementById("patientAdmissionDate").innerHTML = "Admission Date: 03/31/2016";
				document.getElementById("patientDischargeDate").innerHTML = "Discharge Date: 04/01/2016";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("patientDisease").innerHTML = "Disease: Type 1 Diabetes";
				document.getElementById("patientMedicine").innerHTML = "Medicine: Anti-Diabetes";
				document.getElementById("patientMedicineQuantity").innerHTML = "Qty: 10";
				/* Hides medicine tables. */
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenRecords")};
				break;
			/* Displays information of record entry 3.*/
			case "screenRecordEntry3":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows record tables. */
				document.getElementById("patientTable").style.visibility = "visible";
				document.getElementById("patientDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("patientName").innerHTML = "Name: Jessie John Gocotano";
				document.getElementById("patientAddress").innerHTML = "Address: Very near CIT-U.";
				document.getElementById("patientCity").innerHTML = "City: Cebu City";
				document.getElementById("patientState").innerHTML = "State: Cebu";
				document.getElementById("patientZip").innerHTML = "Zip: 6000";
				document.getElementById("patientBirthdate").innerHTML = "Birth Date: December 21, 1997";
				document.getElementById("patientSex").innerHTML = "Sex: Male";
				document.getElementById("patientAdmissionDate").innerHTML = "Admission Date: 01/25/2016";
				document.getElementById("patientDischargeDate").innerHTML = "Discharge Date: 12/25/2016";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("patientDisease").innerHTML = "Disease: Restless Legs Syndrome";
				document.getElementById("patientMedicine").innerHTML = "Medicine: Leg Exercises";
				document.getElementById("patientMedicineQuantity").innerHTML = "Qty: 100";
				/* Hides medicine tables. */
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenRecords")};
				break;
			/* Displays information of record entry 4.*/
			case "screenRecordEntry4":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows record tables. */
				document.getElementById("patientTable").style.visibility = "visible";
				document.getElementById("patientDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("patientName").innerHTML = "Name: Christopher Justine Distrito";
				document.getElementById("patientAddress").innerHTML = "Address: Very far from CIT-U.";
				document.getElementById("patientCity").innerHTML = "City: Cebu City";
				document.getElementById("patientState").innerHTML = "State: Cebu";
				document.getElementById("patientZip").innerHTML = "Zip: 6000";
				document.getElementById("patientBirthdate").innerHTML = "Birth Date: January 1, 1996";
				document.getElementById("patientSex").innerHTML = "Sex: Male";
				document.getElementById("patientAdmissionDate").innerHTML = "Admission Date: 04/20/2016";
				document.getElementById("patientDischargeDate").innerHTML = "Discharge Date: 08/08/2016";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("patientDisease").innerHTML = "Disease: Herpes Gestationis";
				document.getElementById("patientMedicine").innerHTML = "Medicine: Single Pill";
				document.getElementById("patientMedicineQuantity").innerHTML = "Qty: 1";
				/* Hides medicine tables. */
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenRecords")};
				break;
			/* Displays information of medicine entry 1.*/
			case "screenMedicineEntry1":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows medicine tables. */
				document.getElementById("medicineTable").style.visibility = "visible";
				document.getElementById("medicineDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("medicineName").innerHTML = "Name: Bisolvon";
				document.getElementById("medicineGenericName").innerHTML = "Generic Name: Bromhexine";
				document.getElementById("medicinePrice").innerHTML = "Price: P700.00";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("medicineDisease").innerHTML = "Diseases: Cough";
				/* Hides record tables. */
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenMedicines")};
				break;
			/* Displays information of medicine entry 2.*/
			case "screenMedicineEntry2":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows medicine tables. */
				document.getElementById("medicineTable").style.visibility = "visible";
				document.getElementById("medicineDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("medicineName").innerHTML = "Name: DayQuil";
				document.getElementById("medicineGenericName").innerHTML = "Generic Name: Acetaminophen, Dextromethorphan, Pseudoephedrine";
				document.getElementById("medicinePrice").innerHTML = "Price: P896.00";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("medicineDisease").innerHTML = "Diseases: Flu, Cold";
				/* Hides record tables. */
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenMedicines")};
				break;
			/* Displays information of medicine entry 3.*/
			case "screenMedicineEntry3":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows medicine tables. */
				document.getElementById("medicineTable").style.visibility = "visible";
				document.getElementById("medicineDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("medicineName").innerHTML = "Name: Metformin";
				document.getElementById("medicineGenericName").innerHTML = "Generic Name: Glucophage.";
				document.getElementById("medicinePrice").innerHTML = "Price: P850.50";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("medicineDisease").innerHTML = "Diseases: Diabetes";
				/* Hides record tables. */
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenMedicines")};
				break;
			/* Displays information of medicine entry 4.*/
			case "screenMedicineEntry4":
				document.getElementById("functionsViewingInfo").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "visible";
				document.getElementById("addPatient").style.visibility = "hidden";
				/* Shows medicine tables. */
				document.getElementById("medicineTable").style.visibility = "visible";
				document.getElementById("medicineDiseases").style.visibility = "visible";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				/* Sets patient information. */
				document.getElementById("medicineName").innerHTML = "Name: Gynodel";
				document.getElementById("medicineGenericName").innerHTML = "Generic Name: Dopamin Agonisti.";
				document.getElementById("medicinePrice").innerHTML = "Price: P900.00";
				/* Sets patient's diseases and prescriptions. */
				document.getElementById("medicineDisease").innerHTML = "Diseases: Restless Legs Syndrome";
				/* Hides record tables. */
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				/* Back button goes back to records list. */
				document.getElementById("back").onclick = function(){changeContent("screenMedicines")};
				break;
			/* Screen for adding patient.*/
			case "add":
				document.getElementById("addPatient").style.visibility = "visible";
				document.getElementById("functions").style.visibility = "visible";
				document.getElementById("functionsViewingInfo").style.visibility = "hidden";
				document.getElementById("entryInformation").style.visibility = "hidden";
				document.getElementById("medicineTable").style.visibility = "hidden";
				document.getElementById("medicineDiseases").style.visibility = "hidden";
				document.getElementById("patientTable").style.visibility = "hidden";
				document.getElementById("patientDiseases").style.visibility = "hidden";
				/* Hides pages. */
				document.getElementById("pages").style.visibility = "hidden";
				break;
			default:
					changeContent("screenRecords");
				break;
		}
		
		displayedScreen = displayScreen;
	}
}