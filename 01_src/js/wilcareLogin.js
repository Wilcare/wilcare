/** ------------------------------------------------------------------------------
 *WilCare - HEALTHCARE APPLICATION
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Wilcare
* @version 0.01
* Version History
* [
* [07/11/2016] 0.01 Wilson Justine G. Sison - initial codes
* [07/25/2016] 0.02 Chance Adrian M. Angam - revised all codes
**/

/**
* These are the global variables
**/

function login(){
	var username = document.getElementById("user").value;
	var password = document.getElementById("pass").value;
	var id = getUserID(username);
	if(id != -1){
		if(isPasswordCorrect(id,password)){
			window.location = "doctorScreen.html";
		}
		else{
			alert("Password is incorrect");
		}
	}
	else{
		alert("Username does not exist");
	}
}

function isPasswordCorrect(id,password){
	var valid = false;
	if(password == doctors[id].password)
		valid = true;
	return valid;
}

function getUserID(username){
	var id = -1;
	for(var i = 0; i < doctors.length; i++){
		if(username == doctors[i].username){
			id = i;
			break;
		}
	}
	return id;
}