/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
This file contains the functionalities and global variables for the application.
* @author Wilcare
* @version 0.02
* Version History
* [07/20/2016] 0.01 – Christopher Justine Distrito - initial codes
* [07/26/2016] 0.02 – Wilson Justine G. Sison - modified all functions and added new codes 
**/

/**
* This function is used to submit and finalize user information.
**/

/**
* NOTE: Unfinish pani..........
**/

function registerUser() {
	console.log("Registration" + " start");
	var wfirstName = document.getElementById("fNameField").value;
	var wlastName = document.getElementById("lNameField").value;
	var wbirthMonth =  String(document.getElementById("month").value);
	var wbT = document.getElementById("blood");
	var wbloodType = wbT.options[wbT.selectedIndex].text;
	var wbirthDay = document.getElementById("birthD").value;
	var wbirthYear = document.getElementById("birthY").value;
	
	var zbM = document.getElementById("month");
    var zbirthMonth = zbM.options[zbM.selectedIndex].text;
	
  
	 
	 if(validateFields(wfirstName,wlastName,wbirthDay,wbirthMonth,wbirthYear)){
		 
		 var newPatient = { 
				id: patients.length, 				
				firstName: wfirstName,
				lastName: wlastName,
				birthDate: zbirthMonth + " " + wbirthDay + " " + wbirthYear,
				sex:null,
				bloodtype:wbloodType,
				admissionDate:null,
				dischargeDate:null,
				disease:null,
				condition:null,
				totalBill:null
				
			};		 		 
		
			patients.push(newPatient);
			alert("Added item to 'patients' was successful.");
			
			for (var i = 0; i < patients.length; i++) {
				console.log(patients[i]);
			}
	 }	 				
		
	console.log("Registration" + " end");
}


function validateFields(firstName,lastName,birthDay, birthMonth,birthYear){
	var check = false;
	
	if(checkBirthday(birthDay,birthMonth,birthYear) && checkName(firstName,lastName)){
		check = true;
	}
	
	return check;
	
}

function checkName(firstName,lastName){
	var check=true;		
    if(firstName == "" || lastName == ""){
	  window.alert("All fields must not be empty.");
	
	  check = false;
	}		
		
	if(firstName.length > 254 || lastName.length > 254){
	  window.alert("Invalid String Length");
	  check = false;			
	}
	return check;
}

function checkBirthday(day,month,year){
	
	
	var check=true;		
	var currentTime = new Date();
	
	 if(day == "" || day == ""){
	  window.alert("All fields must not be empty.");
	
	  check = false;
	 }		
	
	if(isNaN(day) || isNaN(year)){
		window.alert("Invalid Birthdate");
		check =false;		
	}
	if(year > currentTime.getFullYear() || day > new Date(currentTime.getFullYear() , month.valueOf(month), 0).getDate()){
		window.alert("Invalid Birthdate");
		check =false;		
	}
	
	return check;
}


function AdmissionDate(day,month,year){
	var check=true;		
	var currentTime = new Date();
	
	 //e set ang fields sa current dates......
	
	return check;
}
function DischargeDate(day,month,year){
	
	var check=true;		
	var currentTime = new Date();
	
	 if(day == "" || day == ""){
	  window.alert("All fields must not be empty.");
	
	  check = false;
	 }		
	
	if(isNaN(day) || isNaN(year)){
		window.alert("Invalid date");
		check =false;		
	}
	if(year > currentTime.getFullYear() || day > new Date(currentTime.getFullYear() , month.valueOf(month), 0).getDate()){
		window.alert("Invalid date");
		check =false;		
	}
	
	return check;
}

function checkDisease(disease){
	var check=true;		   
		
	if(disease.length > 50){
	  window.alert("Invalid Length");
	  check = false;			
	}
	return check;
	
}
function checkMedicine(medicine){
	var check=true;		   
		
	if(medicine.length > 30){
	  window.alert("Invalid Length");
	  check = false;			
	}
	return check;
	
}

function checkZip(zip){
	var check=true;		   
		
	if(zip.length > 50){
	  window.alert("Invalid Length");
	  check = false;			
	}
	return check;
	
}

function checkState(state){
	var check=true;		   
		
	if(state.length > 50){
	  window.alert("Invalid Length");
	  check = false;			
	}
	return check;
	
}

function checkCity(city){
	var check=true;		   
		
	if(city.length > 50){
	  window.alert("Invalid Length");
	  check = false;			
	}
	return check;
	
}





//references

/*
		// Return today's date and time
		var currentTime = new Date()

		// returns the month (from 0 to 11)
		var month = currentTime.getMonth() + 1

		// returns the day of the month (from 1 to 31)
		var day = currentTime.getDate()

		// returns the year (four digits)
		var year = currentTime.getFullYear()

		// write output MM/dd/yyyy
		document.write(month + "/" + day + "/" + year)
		
		function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}
		  alert(new Date(currentTime.getFullYear() , birthMonth.valueOf(birthMonth), 0).getDate());
		  
		  	//var bM = document.getElementById("month");
//	var birthMonth = bM.options[bM.selectedIndex].text;
			
	*/

