/** ------------------------------------------------------------------------------
 *WilCare - HEALTHCARE APPLICATION
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Wilcare
* @version 0.01
* Version History
* [07/20/2016] 0.01 – Chance Adrian M. Angam - initial codes
  [07/26/2016] 0.02 – Wilson Justine G. Sison - added codes
  [08/10/2016] 0.03 – Chance Adrian M. Angam - updated codes
**/

/**
* This function is used to submit and finalize user information.
**/

var patients = [
	{id:0, name: "Vincent John Oplas", birthDate: "December 21,1996", sex: "Male",bloodtype: " ", admissionDate: "" ,dischargeDate: "", disease: [""], symptom: [""], disease: "" ,condition: "", totalBill: ""},
	{id:1, name: "Chance Adrian Angam", birthDate: "December 21,1996", sex: "Male",bloodtype: " ", admissionDate: "" ,dischargeDate: "", disease: [""], symptom: [""], disease: "" ,condition: "", totalBill: ""},
	{id:2, name: "Jessie John Gocotano", birthDate: "December 21,1996", sex: "Male",bloodtype: " ", admissionDate: "" ,dischargeDate: "", disease: [""], symptom: [""], disease: "" ,condition: "", totalBill: ""},
	{id:3, name: "Christopher Justine Distrito", birthDate: "December 21,1996", sex: "Male",bloodtype: " ", admissionDate: "" ,dischargeDate: "", disease: [""], symptom: [""], disease: "" ,condition: "", totalBill: ""},
	
];

var doctors = [
	{id:0, username: "wilson" , password: "123" ,name: "Wilson Justine Sison"},
];

var admins = [
	{id:0, username: "tophee", password: "123"},
];

var diseases = [
	{id:0, name: "POEMS Syndrome"},
	{id:1, name: "Type I Diabetes"},
	{id:2, name: "Restless legs Syndrome"},
	{id:3, name: "Herpes Gestationis"},
];

var medicine = [
	{id:0, name: "Bisolvon",  price: "700.00"},
	{id:1, name: "DayQuil",   price: "896.00"},
	{id:2, name: "Metformin", price: "850.50"},
	{id:3, name: "Gynodel",   price: "900.00"},
];
