/** ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- **/
 
/**
* This file contains the functionalities and global variables for the application.
* @author Wilcare
* @version 0.01
* Version History
* [07/27/2016] 0.01 – Wilson Justine G. Sison - initial codes
**/


 function listMedicalRecords(){}
 function filterMedicine(){}
 function update(){}
 
 